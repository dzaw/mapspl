-- create tables


CREATE TABLE Countries (
  IDCountry int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  CountryCode varchar(2) NOT NULL UNIQUE,
  CountryName varchar(100) NOT NULL UNIQUE
);

CREATE TABLE Cities (
  IDCity int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  CityName varchar(100) NOT NULL UNIQUE,
  CountryID int NOT NULL,
  FOREIGN KEY (CountryID) REFERENCES Countries(IDCountry)
);

CREATE TABLE Addresses (
  IDAddress int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  Address text NOT NULL,
  Code int,
  CityID int NOT NULL,
  FOREIGN KEY (CityID) REFERENCES Cities(IDCity)
);

CREATE TABLE Coordinates (
  IDCoords int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  Lat float NOT NULL,
  Lon float NOT NULL,
  AddressID int NOT NULL,
  FOREIGN KEY (AddressID) REFERENCES Addresses(IDAddress)
);

CREATE TABLE LinkCategories (
  IDLinkCat int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  LinkCatName varchar(24) NOT NULL UNIQUE
);

CREATE TABLE Links (
  IDLinks int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  URL text NOT NULL,
  -- POIID int NOT NULL,
  LinkCatID int NOT NULL,
  FOREIGN KEY (LinkCatID) REFERENCES LinkCategories(IDLinkCat)
);

CREATE TABLE Categories (
  IDCtgry int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  CatName text NOT NULL
);

CREATE TABLE Subcategories (
  IDSubctgry int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  SubcatName varchar(50) NOT NULL UNIQUE,
  CategoryID int NOT NULL,
  FOREIGN KEY (CategoryID) REFERENCES Categories(IDCtgry)
);

CREATE TABLE Types (
  IDType int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  TypeName varchar(12) NOT NULL UNIQUE
);

CREATE TABLE Fidelities (
  IDFidelity int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  FidelityName text NOT NULL
);

CREATE TABLE Updaters (
  IDUpdater int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  UpdaterName text NOT NULL
);

CREATE TABLE POIStatuses (
  IDPOIStatus int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  POIStatusName varchar(12) NOT NULL DEFAULT 'existent'
);

CREATE TABLE POIPhotos (
  IDPhoto int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  PhotoLink text NOT NULL
  -- , POIID int NOT NULL
);

CREATE TABLE POIVideos (
  IDVideo int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  VideoLink text NOT NULL
  -- , POIID int NOT NULL
);

CREATE TABLE Tags (
  IDTag int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  TagName text
  -- , POIID int NOT NULL
);

CREATE TABLE POIs (
  IDPOI int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  Name varchar(100) NOT NULL UNIQUE,
  CategoryID int NOT NULL,
  SubcategoryID int,
  CoordinatesID int NOT NULL,
  LinkID int,
  TypeID int NOT NULL,
  Logo text NOT NULL,
  ProfPhoto text,
  TagsID int,
  Description text,
  PhotoID int,
  VideoID int,
  WWW text,
  Phone text,
  Mail varchar(320),
  FidelityID int NOT NULL,
  UpdateDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UpdaterID int NOT NULL,
  POIStatusID int NOT NULL,
  CheckDate date,
  FOREIGN KEY (CategoryID) REFERENCES Categories(IDCtgry),
  FOREIGN KEY (SubcategoryID) REFERENCES Subcategories(IDSubctgry),
  FOREIGN KEY (CoordinatesID) REFERENCES Coordinates(IDCoords),
  FOREIGN KEY (LinkID) REFERENCES Links(IDLinks),
  FOREIGN KEY (TypeID) REFERENCES Types(IDType),
  FOREIGN KEY (FidelityID) REFERENCES Fidelities(IDFidelity),
  FOREIGN KEY (UpdaterID) REFERENCES Updaters(IDUpdater),
  FOREIGN KEY (POIStatusID) REFERENCES POIStatuses(IDPOIStatus),
  FOREIGN KEY (PhotoID) REFERENCES POIPhotos(IDPhoto),
  FOREIGN KEY (VideoID) REFERENCES POIVideos(IDVideo),
  FOREIGN KEY (TagsID) REFERENCES Tags(IDTag)
);

/*
-- alter table
ALTER TABLE Links ADD FOREIGN KEY (POIID) REFERENCES POIs(IDPOI);
ALTER TABLE POIPhotos ADD FOREIGN KEY (POIID) REFERENCES POIs(IDPOI);
ALTER TABLE POIVideos ADD FOREIGN KEY (POIID) REFERENCES POIs(IDPOI);
ALTER TABLE Tags ADD FOREIGN KEY (POIID) REFERENCES POIs(IDPOI);
*/
