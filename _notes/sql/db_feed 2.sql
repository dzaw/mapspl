-- Countries
INSERT INTO Countries VALUES (null, 'AF', 'Afghanistan');
INSERT INTO Countries VALUES (null, 'AL', 'Albania');
INSERT INTO Countries VALUES (null, 'DZ', 'Algeria');
INSERT INTO Countries VALUES (null, 'DS', 'American Samoa');
INSERT INTO Countries VALUES (null, 'AD', 'Andorra');
INSERT INTO Countries VALUES (null, 'AO', 'Angola');
INSERT INTO Countries VALUES (null, 'AI', 'Anguilla');
INSERT INTO Countries VALUES (null, 'AQ', 'Antarctica');
INSERT INTO Countries VALUES (null, 'AG', 'Antigua and Barbuda');
INSERT INTO Countries VALUES (null, 'AR', 'Argentina');
INSERT INTO Countries VALUES (null, 'AM', 'Armenia');
INSERT INTO Countries VALUES (null, 'AW', 'Aruba');
INSERT INTO Countries VALUES (null, 'AU', 'Australia');
INSERT INTO Countries VALUES (null, 'AT', 'Austria');
INSERT INTO Countries VALUES (null, 'AZ', 'Azerbaijan');
INSERT INTO Countries VALUES (null, 'BS', 'Bahamas');
INSERT INTO Countries VALUES (null, 'BH', 'Bahrain');
INSERT INTO Countries VALUES (null, 'BD', 'Bangladesh');
INSERT INTO Countries VALUES (null, 'BB', 'Barbados');
INSERT INTO Countries VALUES (null, 'BY', 'Belarus');
INSERT INTO Countries VALUES (null, 'BE', 'Belgium');
INSERT INTO Countries VALUES (null, 'BZ', 'Belize');
INSERT INTO Countries VALUES (null, 'BJ', 'Benin');
INSERT INTO Countries VALUES (null, 'BM', 'Bermuda');
INSERT INTO Countries VALUES (null, 'BT', 'Bhutan');
INSERT INTO Countries VALUES (null, 'BO', 'Bolivia');
INSERT INTO Countries VALUES (null, 'BA', 'Bosnia and Herzegovina');
INSERT INTO Countries VALUES (null, 'BW', 'Botswana');
INSERT INTO Countries VALUES (null, 'BV', 'Bouvet Island');
INSERT INTO Countries VALUES (null, 'BR', 'Brazil');
INSERT INTO Countries VALUES (null, 'IO', 'British Indian Ocean Territory');
INSERT INTO Countries VALUES (null, 'BN', 'Brunei Darussalam');
INSERT INTO Countries VALUES (null, 'BG', 'Bulgaria');
INSERT INTO Countries VALUES (null, 'BF', 'Burkina Faso');
INSERT INTO Countries VALUES (null, 'BI', 'Burundi');
INSERT INTO Countries VALUES (null, 'KH', 'Cambodia');
INSERT INTO Countries VALUES (null, 'CM', 'Cameroon');
INSERT INTO Countries VALUES (null, 'CA', 'Canada');
INSERT INTO Countries VALUES (null, 'CV', 'Cape Verde');
INSERT INTO Countries VALUES (null, 'KY', 'Cayman Islands');
INSERT INTO Countries VALUES (null, 'CF', 'Central African Republic');
INSERT INTO Countries VALUES (null, 'TD', 'Chad');
INSERT INTO Countries VALUES (null, 'CL', 'Chile');
INSERT INTO Countries VALUES (null, 'CN', 'China');
INSERT INTO Countries VALUES (null, 'CX', 'Christmas Island');
INSERT INTO Countries VALUES (null, 'CC', 'Cocos (Keeling) Islands');
INSERT INTO Countries VALUES (null, 'CO', 'Colombia');
INSERT INTO Countries VALUES (null, 'KM', 'Comoros');
INSERT INTO Countries VALUES (null, 'CG', 'Congo');
INSERT INTO Countries VALUES (null, 'CK', 'Cook Islands');
INSERT INTO Countries VALUES (null, 'CR', 'Costa Rica');
INSERT INTO Countries VALUES (null, 'HR', 'Croatia (Hrvatska)');
INSERT INTO Countries VALUES (null, 'CU', 'Cuba');
INSERT INTO Countries VALUES (null, 'CY', 'Cyprus');
INSERT INTO Countries VALUES (null, 'CZ', 'Czech Republic');
INSERT INTO Countries VALUES (null, 'DK', 'Denmark');
INSERT INTO Countries VALUES (null, 'DJ', 'Djibouti');
INSERT INTO Countries VALUES (null, 'DM', 'Dominica');
INSERT INTO Countries VALUES (null, 'DO', 'Dominican Republic');
INSERT INTO Countries VALUES (null, 'TP', 'East Timor');
INSERT INTO Countries VALUES (null, 'EC', 'Ecuador');
INSERT INTO Countries VALUES (null, 'EG', 'Egypt');
INSERT INTO Countries VALUES (null, 'SV', 'El Salvador');
INSERT INTO Countries VALUES (null, 'GQ', 'Equatorial Guinea');
INSERT INTO Countries VALUES (null, 'ER', 'Eritrea');
INSERT INTO Countries VALUES (null, 'EE', 'Estonia');
INSERT INTO Countries VALUES (null, 'ET', 'Ethiopia');
INSERT INTO Countries VALUES (null, 'FK', 'Falkland Islands (Malvinas)');
INSERT INTO Countries VALUES (null, 'FO', 'Faroe Islands');
INSERT INTO Countries VALUES (null, 'FJ', 'Fiji');
INSERT INTO Countries VALUES (null, 'FI', 'Finland');
INSERT INTO Countries VALUES (null, 'FR', 'France');
INSERT INTO Countries VALUES (null, 'FX', 'France, Metropolitan');
INSERT INTO Countries VALUES (null, 'GF', 'French Guiana');
INSERT INTO Countries VALUES (null, 'PF', 'French Polynesia');
INSERT INTO Countries VALUES (null, 'TF', 'French Southern Territories');
INSERT INTO Countries VALUES (null, 'GA', 'Gabon');
INSERT INTO Countries VALUES (null, 'GM', 'Gambia');
INSERT INTO Countries VALUES (null, 'GE', 'Georgia');
INSERT INTO Countries VALUES (null, 'DE', 'Germany');
INSERT INTO Countries VALUES (null, 'GH', 'Ghana');
INSERT INTO Countries VALUES (null, 'GI', 'Gibraltar');
INSERT INTO Countries VALUES (null, 'GK', 'Guernsey');
INSERT INTO Countries VALUES (null, 'GR', 'Greece');
INSERT INTO Countries VALUES (null, 'GL', 'Greenland');
INSERT INTO Countries VALUES (null, 'GD', 'Grenada');
INSERT INTO Countries VALUES (null, 'GP', 'Guadeloupe');
INSERT INTO Countries VALUES (null, 'GU', 'Guam');
INSERT INTO Countries VALUES (null, 'GT', 'Guatemala');
INSERT INTO Countries VALUES (null, 'GN', 'Guinea');
INSERT INTO Countries VALUES (null, 'GW', 'Guinea-Bissau');
INSERT INTO Countries VALUES (null, 'GY', 'Guyana');
INSERT INTO Countries VALUES (null, 'HT', 'Haiti');
INSERT INTO Countries VALUES (null, 'HM', 'Heard and Mc Donald Islands');
INSERT INTO Countries VALUES (null, 'HN', 'Honduras');
INSERT INTO Countries VALUES (null, 'HK', 'Hong Kong');
INSERT INTO Countries VALUES (null, 'HU', 'Hungary');
INSERT INTO Countries VALUES (null, 'IS', 'Iceland');
INSERT INTO Countries VALUES (null, 'IN', 'India');
INSERT INTO Countries VALUES (null, 'IM', 'Isle of Man');
INSERT INTO Countries VALUES (null, 'ID', 'Indonesia');
INSERT INTO Countries VALUES (null, 'IR', 'Iran (Islamic Republic of)');
INSERT INTO Countries VALUES (null, 'IQ', 'Iraq');
INSERT INTO Countries VALUES (null, 'IE', 'Ireland');
INSERT INTO Countries VALUES (null, 'IL', 'Israel');
INSERT INTO Countries VALUES (null, 'IT', 'Italy');
INSERT INTO Countries VALUES (null, 'CI', 'Ivory Coast');
INSERT INTO Countries VALUES (null, 'JE', 'Jersey');
INSERT INTO Countries VALUES (null, 'JM', 'Jamaica');
INSERT INTO Countries VALUES (null, 'JP', 'Japan');
INSERT INTO Countries VALUES (null, 'JO', 'Jordan');
INSERT INTO Countries VALUES (null, 'KZ', 'Kazakhstan');
INSERT INTO Countries VALUES (null, 'KE', 'Kenya');
INSERT INTO Countries VALUES (null, 'KI', 'Kiribati');
INSERT INTO Countries VALUES (null, 'KP', 'Korea, Democratic People''s Republic of');
INSERT INTO Countries VALUES (null, 'KR', 'Korea, Republic of');
INSERT INTO Countries VALUES (null, 'XK', 'Kosovo');
INSERT INTO Countries VALUES (null, 'KW', 'Kuwait');
INSERT INTO Countries VALUES (null, 'KG', 'Kyrgyzstan');
INSERT INTO Countries VALUES (null, 'LA', 'Lao People''s Democratic Republic');
INSERT INTO Countries VALUES (null, 'LV', 'Latvia');
INSERT INTO Countries VALUES (null, 'LB', 'Lebanon');
INSERT INTO Countries VALUES (null, 'LS', 'Lesotho');
INSERT INTO Countries VALUES (null, 'LR', 'Liberia');
INSERT INTO Countries VALUES (null, 'LY', 'Libyan Arab Jamahiriya');
INSERT INTO Countries VALUES (null, 'LI', 'Liechtenstein');
INSERT INTO Countries VALUES (null, 'LT', 'Lithuania');
INSERT INTO Countries VALUES (null, 'LU', 'Luxembourg');
INSERT INTO Countries VALUES (null, 'MO', 'Macau');
INSERT INTO Countries VALUES (null, 'MK', 'Macedonia');
INSERT INTO Countries VALUES (null, 'MG', 'Madagascar');
INSERT INTO Countries VALUES (null, 'MW', 'Malawi');
INSERT INTO Countries VALUES (null, 'MY', 'Malaysia');
INSERT INTO Countries VALUES (null, 'MV', 'Maldives');
INSERT INTO Countries VALUES (null, 'ML', 'Mali');
INSERT INTO Countries VALUES (null, 'MT', 'Malta');
INSERT INTO Countries VALUES (null, 'MH', 'Marshall Islands');
INSERT INTO Countries VALUES (null, 'MQ', 'Martinique');
INSERT INTO Countries VALUES (null, 'MR', 'Mauritania');
INSERT INTO Countries VALUES (null, 'MU', 'Mauritius');
INSERT INTO Countries VALUES (null, 'TY', 'Mayotte');
INSERT INTO Countries VALUES (null, 'MX', 'Mexico');
INSERT INTO Countries VALUES (null, 'FM', 'Micronesia, Federated States of');
INSERT INTO Countries VALUES (null, 'MD', 'Moldova, Republic of');
INSERT INTO Countries VALUES (null, 'MC', 'Monaco');
INSERT INTO Countries VALUES (null, 'MN', 'Mongolia');
INSERT INTO Countries VALUES (null, 'ME', 'Montenegro');
INSERT INTO Countries VALUES (null, 'MS', 'Montserrat');
INSERT INTO Countries VALUES (null, 'MA', 'Morocco');
INSERT INTO Countries VALUES (null, 'MZ', 'Mozambique');
INSERT INTO Countries VALUES (null, 'MM', 'Myanmar');
INSERT INTO Countries VALUES (null, 'NA', 'Namibia');
INSERT INTO Countries VALUES (null, 'NR', 'Nauru');
INSERT INTO Countries VALUES (null, 'NP', 'Nepal');
INSERT INTO Countries VALUES (null, 'NL', 'Netherlands');
INSERT INTO Countries VALUES (null, 'AN', 'Netherlands Antilles');
INSERT INTO Countries VALUES (null, 'NC', 'New Caledonia');
INSERT INTO Countries VALUES (null, 'NZ', 'New Zealand');
INSERT INTO Countries VALUES (null, 'NI', 'Nicaragua');
INSERT INTO Countries VALUES (null, 'NE', 'Niger');
INSERT INTO Countries VALUES (null, 'NG', 'Nigeria');
INSERT INTO Countries VALUES (null, 'NU', 'Niue');
INSERT INTO Countries VALUES (null, 'NF', 'Norfolk Island');
INSERT INTO Countries VALUES (null, 'MP', 'Northern Mariana Islands');
INSERT INTO Countries VALUES (null, 'NO', 'Norway');
INSERT INTO Countries VALUES (null, 'OM', 'Oman');
INSERT INTO Countries VALUES (null, 'PK', 'Pakistan');
INSERT INTO Countries VALUES (null, 'PW', 'Palau');
INSERT INTO Countries VALUES (null, 'PS', 'Palestine');
INSERT INTO Countries VALUES (null, 'PA', 'Panama');
INSERT INTO Countries VALUES (null, 'PG', 'Papua New Guinea');
INSERT INTO Countries VALUES (null, 'PY', 'Paraguay');
INSERT INTO Countries VALUES (null, 'PE', 'Peru');
INSERT INTO Countries VALUES (null, 'PH', 'Philippines');
INSERT INTO Countries VALUES (null, 'PN', 'Pitcairn');
INSERT INTO Countries VALUES (null, 'PL', 'Poland');
INSERT INTO Countries VALUES (null, 'PT', 'Portugal');
INSERT INTO Countries VALUES (null, 'PR', 'Puerto Rico');
INSERT INTO Countries VALUES (null, 'QA', 'Qatar');
INSERT INTO Countries VALUES (null, 'RE', 'Reunion');
INSERT INTO Countries VALUES (null, 'RO', 'Romania');
INSERT INTO Countries VALUES (null, 'RU', 'Russian Federation');
INSERT INTO Countries VALUES (null, 'RW', 'Rwanda');
INSERT INTO Countries VALUES (null, 'KN', 'Saint Kitts and Nevis');
INSERT INTO Countries VALUES (null, 'LC', 'Saint Lucia');
INSERT INTO Countries VALUES (null, 'VC', 'Saint Vincent and the Grenadines');
INSERT INTO Countries VALUES (null, 'WS', 'Samoa');
INSERT INTO Countries VALUES (null, 'SM', 'San Marino');
INSERT INTO Countries VALUES (null, 'ST', 'Sao Tome and Principe');
INSERT INTO Countries VALUES (null, 'SA', 'Saudi Arabia');
INSERT INTO Countries VALUES (null, 'SN', 'Senegal');
INSERT INTO Countries VALUES (null, 'RS', 'Serbia');
INSERT INTO Countries VALUES (null, 'SC', 'Seychelles');
INSERT INTO Countries VALUES (null, 'SL', 'Sierra Leone');
INSERT INTO Countries VALUES (null, 'SG', 'Singapore');
INSERT INTO Countries VALUES (null, 'SK', 'Slovakia');
INSERT INTO Countries VALUES (null, 'SI', 'Slovenia');
INSERT INTO Countries VALUES (null, 'SB', 'Solomon Islands');
INSERT INTO Countries VALUES (null, 'SO', 'Somalia');
INSERT INTO Countries VALUES (null, 'ZA', 'South Africa');
INSERT INTO Countries VALUES (null, 'GS', 'South Georgia South Sandwich Islands');
INSERT INTO Countries VALUES (null, 'ES', 'Spain');
INSERT INTO Countries VALUES (null, 'LK', 'Sri Lanka');
INSERT INTO Countries VALUES (null, 'SH', 'St. Helena');
INSERT INTO Countries VALUES (null, 'PM', 'St. Pierre and Miquelon');
INSERT INTO Countries VALUES (null, 'SD', 'Sudan');
INSERT INTO Countries VALUES (null, 'SR', 'Suriname');
INSERT INTO Countries VALUES (null, 'SJ', 'Svalbard and Jan Mayen Islands');
INSERT INTO Countries VALUES (null, 'SZ', 'Swaziland');
INSERT INTO Countries VALUES (null, 'SE', 'Sweden');
INSERT INTO Countries VALUES (null, 'CH', 'Switzerland');
INSERT INTO Countries VALUES (null, 'SY', 'Syrian Arab Republic');
INSERT INTO Countries VALUES (null, 'TW', 'Taiwan');
INSERT INTO Countries VALUES (null, 'TJ', 'Tajikistan');
INSERT INTO Countries VALUES (null, 'TZ', 'Tanzania, United Republic of');
INSERT INTO Countries VALUES (null, 'TH', 'Thailand');
INSERT INTO Countries VALUES (null, 'TG', 'Togo');
INSERT INTO Countries VALUES (null, 'TK', 'Tokelau');
INSERT INTO Countries VALUES (null, 'TO', 'Tonga');
INSERT INTO Countries VALUES (null, 'TT', 'Trinidad and Tobago');
INSERT INTO Countries VALUES (null, 'TN', 'Tunisia');
INSERT INTO Countries VALUES (null, 'TR', 'Turkey');
INSERT INTO Countries VALUES (null, 'TM', 'Turkmenistan');
INSERT INTO Countries VALUES (null, 'TC', 'Turks and Caicos Islands');
INSERT INTO Countries VALUES (null, 'TV', 'Tuvalu');
INSERT INTO Countries VALUES (null, 'UG', 'Uganda');
INSERT INTO Countries VALUES (null, 'UA', 'Ukraine');
INSERT INTO Countries VALUES (null, 'AE', 'United Arab Emirates');
INSERT INTO Countries VALUES (null, 'GB', 'United Kingdom');
INSERT INTO Countries VALUES (null, 'US', 'United States');
INSERT INTO Countries VALUES (null, 'UM', 'United States minor outlying islands');
INSERT INTO Countries VALUES (null, 'UY', 'Uruguay');
INSERT INTO Countries VALUES (null, 'UZ', 'Uzbekistan');
INSERT INTO Countries VALUES (null, 'VU', 'Vanuatu');
INSERT INTO Countries VALUES (null, 'VA', 'Vatican City State');
INSERT INTO Countries VALUES (null, 'VE', 'Venezuela');
INSERT INTO Countries VALUES (null, 'VN', 'Vietnam');
INSERT INTO Countries VALUES (null, 'VG', 'Virgin Islands (British)');
INSERT INTO Countries VALUES (null, 'VI', 'Virgin Islands (U.S.)');
INSERT INTO Countries VALUES (null, 'WF', 'Wallis and Futuna Islands');
INSERT INTO Countries VALUES (null, 'EH', 'Western Sahara');
INSERT INTO Countries VALUES (null, 'YE', 'Yemen');
INSERT INTO Countries VALUES (null, 'ZR', 'Zaire');
INSERT INTO Countries VALUES (null, 'ZM', 'Zambia');
INSERT INTO Countries VALUES (null, 'ZW', 'Zimbabwe');

-- Cities
INSERT INTO Cities VALUES (null, 'Rzym', 106);
INSERT INTO Cities VALUES (null, 'Florencja', 106);
INSERT INTO Cities VALUES (null, 'Mediolan', 106);
INSERT INTO Cities VALUES (null, 'Padova', 106);
INSERT INTO Cities VALUES (null, 'Reggio di Calabria', 106);
INSERT INTO Cities VALUES (null, 'Monte Cassino', 106);

-- Addresses
INSERT INTO Addresses VALUES (null, 'Via Agostino Depretis, 9-25', 20142, 3);
INSERT INTO Addresses VALUES (null, 'Largo Andrea Barbazza 32/33', 00168, 1);
INSERT INTO Addresses VALUES (null, 'Cimitero Militare Polacco di Monte Cassino', null, 6);
INSERT INTO Addresses VALUES (null, 'Via Piemonte 117, I', 00187, 1);
INSERT INTO Addresses VALUES (null, 'Via Trapezzoli Sud, 97', 89134, 5); 
INSERT INTO Addresses VALUES (null, 'Via Giovanni Giolitti, 2', 35129, 4); 
INSERT INTO Addresses VALUES (null, 'Giardino di Boboli, Piazza de Pitti', 50125, 2); 


-- Coordinates
INSERT INTO Coordinates VALUES (null, 41.9201, 12.4117, 2);
INSERT INTO Coordinates VALUES (null, 41.4941, 13.8057, 3);
INSERT INTO Coordinates VALUES (null, 45.4305, 9.15366, 1);
INSERT INTO Coordinates VALUES (null, 41.9097, 12.4934, 4);
INSERT INTO Coordinates VALUES (null, 38.0548, 15.6781, 5);
INSERT INTO Coordinates VALUES (null, 45.4144, 11.9165, 6);
INSERT INTO Coordinates VALUES (null, 43.7631, 11.2505, 7);

-- Categories
INSERT INTO Categories VALUES (null, 'Życie codzienne');
INSERT INTO Categories VALUES (null, 'Instytucje');
INSERT INTO Categories VALUES (null, 'Architektura miast');
INSERT INTO Categories VALUES (null, 'Krajobraz i turystyka');
INSERT INTO Categories VALUES (null, 'Religijne');
INSERT INTO Categories VALUES (null, 'Historyczne');
INSERT INTO Categories VALUES (null, 'Media polonijne');
INSERT INTO Categories VALUES (null, 'Imprezy i wydarzenia');

-- Subcategories
INSERT INTO Subcategories VALUES (null, 'Polskie sklepy', 1);
INSERT INTO Subcategories VALUES (null, 'Polskie restauracje', 1);
INSERT INTO Subcategories VALUES (null, 'Polscy lekarze', 1);
INSERT INTO Subcategories VALUES (null, 'Polscy prawnicy', 1);
INSERT INTO Subcategories VALUES (null, 'Polscy usługodawcy', 1);
INSERT INTO Subcategories VALUES (null, 'Polskie firmy za granicą', 1);

INSERT INTO Subcategories VALUES (null, 'Dyplomatyczne, konsularne', 2);
INSERT INTO Subcategories VALUES (null, 'Kulturalne i handlowe', 2);
INSERT INTO Subcategories VALUES (null, 'Instytucje i organizacje lokalne', 2);
INSERT INTO Subcategories VALUES (null, 'Uczelnie, szkoły', 2);
INSERT INTO Subcategories VALUES (null, 'Ośrodki naukowe, szkoleniowe', 2);

INSERT INTO Subcategories VALUES (null, 'Ulice, skwery, ronda, parki', 3);
INSERT INTO Subcategories VALUES (null, 'Dzielnice, wioski, miasta', 3);
INSERT INTO Subcategories VALUES (null, 'Miasta partnerskie', 3);
INSERT INTO Subcategories VALUES (null, 'Budynki polskich architektów', 3);
INSERT INTO Subcategories VALUES (null, 'Instytucje im. wielkich Polaków', 3);

INSERT INTO Subcategories VALUES (null, 'Szlaki turystyczne', 4);
INSERT INTO Subcategories VALUES (null, 'Miejsca geograficzne', 4);
INSERT INTO Subcategories VALUES (null, 'Polscy odkrywcy i podróżnicy', 4);

-- ToDo 5,6,7,8..

-- Types
INSERT INTO Types VALUES (null, 'commerce');
INSERT INTO Types VALUES (null, 'non-commerce');

-- LinkCategories 
INSERT INTO LinkCategories VALUES (null, 'facebook');
INSERT INTO LinkCategories VALUES (null, 'twitter');
INSERT INTO LinkCategories VALUES (null, 'instagram');
INSERT INTO LinkCategories VALUES (null, 'google plus');
INSERT INTO LinkCategories VALUES (null, 'linkedin');

-- Fidelities
INSERT INTO Fidelities VALUES (null, 'budynek');
INSERT INTO Fidelities VALUES (null, 'ulica');
INSERT INTO Fidelities VALUES (null, 'miasto');
INSERT INTO Fidelities VALUES (null, 'region');
INSERT INTO Fidelities VALUES (null, 'państwo');

-- Updaters
INSERT INTO Updaters VALUES (null, 'updater1');

-- POI Status
INSERT INTO POIStatuses VALUES (null, 'existent');
INSERT INTO POIStatuses VALUES (null, 'non-existent');

-- Tags
INSERT INTO Tags VALUES ();

-- POI
INSERT INTO POIs VALUES (null, 'Ristorante Polacco Bajka', 1, 2, 1, null, 1, 'assets/img/icons/restaurant.png', 'assets/img/items/34.png', null, 'Lorem ipsum..', null, null, 'http://bajka.it', '+39 328 803 0162', 'mail@bajka.it', 1, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Monte Cassino', 6, null, 2, null, 2, 'assets/img/icons/memorialsite.png', 'assets/img/items/32.png', null, 'Lorem ipsum..', null, null, null, null, null, 2, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Centauro di Mitoraj', 3, 15, 3, null, 2, 'assets/img/icons/art.png', 'assets/img/items/33.png', null, 'Lorem ipsum..', null, null, null, null, null, 1, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Związek Polaków we Włoszech', 2, 9, 4, null, 2, 'assets/img/icons/organization.png', 'assets/img/items/36.png', null, 'Lorem ipsum..', null, null, 'http://polonia-wloska.org', '+39 555 444 333', 'http://polonia-wloska.org/kontakt', 1, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Związek Polaków w Kalabrii', 2, 9, 5, null, 2, 'assets/img/icons/organization.png', 'assets/img/items/37.png', null, 'Lorem ipsum..', null, null, 'http://www.associazionepolacchiincalabria.it', '0965-624466', 'assopolca@yahoo.it', 1, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Scuola di Lingua e Cultura Polacca', 2, 10, 6, null, 2, 'assets/img/icons/education.png', 'assets/img/items/38.png', null, 'Lorem ipsum..', null, null, 'http://www.polskaszkola-padwa.pl', '+39 328 803 0162', 'aipp.szkolapolska@gmail.com', 1, null, 1, 1, null);
INSERT INTO POIs VALUES (null, 'Bronze head of Igor Mitoraj', 3, 15, 7, null, 2, 'assets/img/icons/art.png', 'assets/img/items/39.png', null, 'Lorem ipsum..', null, null, null, null, null, 1, null, 1, 1, null);

