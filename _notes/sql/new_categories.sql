SET FOREIGN_KEY_CHECKS=0;

DROP TABLE Categories;
DROP TABLE Subcategories;

CREATE TABLE Categories (
  IDCtgry int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  CatName text NOT NULL
);

CREATE TABLE Subcategories (
  IDSubctgry int NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
  SubcatName varchar(50) NOT NULL UNIQUE,
  CategoryID int NOT NULL,
  FOREIGN KEY (CategoryID) REFERENCES Categories(IDCtgry)
);

INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Handlowe: Sklepy');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Handlowe: Gastronomia i życie nocne');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Handlowe: Usługowe');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Handlowe: Inne polskie firmy');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Instytucje: Oficjalne i państwowe');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Instytucje: Kulturalne i sportowe');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Instytucje: Organizacje polonijne');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Instytucje: Oświatowe');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Ku pamięci: Architektura miast');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Ku pamięci: Wyjątkowe');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Ku pamięci: Krajobraz');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Religijne');
INSERT INTO Categories (IDCtgry, CatName) VALUES (null, 'Historyczne');

INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie delikatesy', 1);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Sklepy z polskimi produktami', 1);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie piekarnie', 1);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne polskie sklepy', 1);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie restauracje', 2);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie kawiarnie', 2);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie puby', 2);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie kluby i dyskoteki', 2);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polscy lekarze', 3);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polscy prawnicy', 3);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polscy tłumacze', 3);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie biura podróży', 3);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne polskie usługi', 3);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, ' Polskie firmy', 4);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Ambasady', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Konsulaty generalne', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Konsulaty', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Agencje konsularne', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Konsulaty honorowe', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Instytuty polskie', 5);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Domy polskie', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Muzea', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Teatry', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Biblioteki', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Siedziby mediów polonijnych', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Szkoły i kluby sportowe', 6);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Kulturalne', 7);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Oświatowe', 7);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Kombatanckie', 7);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Religijne', 7);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Ogólne / inne', 7);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Uczelnie wyższe - fakultety', 8);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie szkoły i z nauką j. polskiego', 8);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Szkoły nauki języka polskiego', 8);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne ośrodki szkoleniowe', 8);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Ulice', 9);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Ronda', 9);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Skwery, parki', 9);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Dzielnice, wioski, miasta', 9);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Instytucje im. Wielkich Polaków', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Budynki polskich architektów', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Tablice pamiątkowe', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Rzeżby, posągi, pomniki', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Miejsca sportowych sukcesów', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne miejsca zw. z polakami', 10);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Szlaki turystyczne', 11);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Góry', 11);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Rzeki', 11);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Kotliny', 11);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne', 11);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie parafie', 12);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie kościoły', 12);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polscy księża i misjonarze', 12);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Nabożeństwa w języku polskim', 12);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Pomniki bohaterów', 13);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Miejsca pamięci', 13);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Groby wielkich polaków', 13);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Polskie cmentarze i kwatery', 13);
INSERT INTO Subcategories (IDSubctgry, SubcatName, CategoryID) VALUES (null, 'Inne historyczne', 13);

SET FOREIGN_KEY_CHECKS=1;