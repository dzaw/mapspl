-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 27 Lis 2018, 09:09
-- Wersja serwera: 10.1.36-MariaDB-cll-lve
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `v55582464_mapspl`
--
CREATE DATABASE IF NOT EXISTS `v55582464_mapspl` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `v55582464_mapspl`;

DELIMITER $$
--
-- Procedury
--
DROP PROCEDURE IF EXISTS `sp_insertcity`$$
CREATE DEFINER=`v55582464_mapusr`@`localhost` PROCEDURE `sp_insertcity` (IN `_IDCity` INT, IN `_CityName` VARCHAR(100), IN `_CountryID` INT)  BEGIN
INSERT INTO mapspl2.Cities VALUES (
_IDCity, 
_CityName, 
_CountryID);

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Addresses`
--

DROP TABLE IF EXISTS `Addresses`;
CREATE TABLE IF NOT EXISTS `Addresses` (
  `IDAddress` int(11) NOT NULL AUTO_INCREMENT,
  `Address` text NOT NULL,
  `Code` int(11) DEFAULT NULL,
  `CityID` int(11) NOT NULL,
  PRIMARY KEY (`IDAddress`),
  KEY `CityID` (`CityID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Addresses`
--

INSERT INTO `Addresses` (`IDAddress`, `Address`, `Code`, `CityID`) VALUES
(1, 'Via Agostino Depretis, 9-25', 20142, 3),
(2, 'Largo Andrea Barbazza 32/33', 168, 1),
(3, 'Cimitero Militare Polacco di Monte Cassino', NULL, 6),
(4, 'Via Piemonte 117, I', 187, 1),
(5, 'Via Trapezzoli Sud, 97', 89134, 5),
(6, 'Via Giovanni Giolitti, 2', 35129, 4),
(7, 'Giardino di Boboli, Piazza de Pitti', 50125, 2),
(21, 'Berlin, Niemcy', 0, 7),
(24, 'Alexanderpl. 9, 10178 Berlin, Niemcy', 10178, 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Categories`
--

DROP TABLE IF EXISTS `Categories`;
CREATE TABLE IF NOT EXISTS `Categories` (
  `IDCtgry` int(11) NOT NULL AUTO_INCREMENT,
  `CatName` text NOT NULL,
  PRIMARY KEY (`IDCtgry`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Categories`
--

INSERT INTO `Categories` (`IDCtgry`, `CatName`) VALUES
(1, 'Handlowe: Sklepy'),
(2, 'Handlowe: Gastronomia i życie nocne'),
(3, 'Handlowe: Usługowe'),
(4, 'Handlowe: Inne polskie firmy'),
(5, 'Instytucje: Oficjalne i państwowe'),
(6, 'Instytucje: Kulturalne i sportowe'),
(7, 'Instytucje: Organizacje polonijne'),
(8, 'Instytucje: Oświatowe'),
(9, 'Ku pamięci: Architektura miast'),
(10, 'Ku pamięci: Wyjątkowe'),
(11, 'Ku pamięci: Krajobraz'),
(12, 'Religijne'),
(13, 'Historyczne'),
(14, 'Życie codzienne'),
(15, 'Instytucje'),
(16, 'Architektura miast'),
(17, 'Krajobraz i turystyka'),
(18, 'Religijne'),
(19, 'Historyczne'),
(20, 'Media polonijne'),
(21, 'Imprezy i wydarzenia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Cities`
--

DROP TABLE IF EXISTS `Cities`;
CREATE TABLE IF NOT EXISTS `Cities` (
  `IDCity` int(11) NOT NULL AUTO_INCREMENT,
  `CityName` varchar(100) NOT NULL,
  `CountryID` int(11) NOT NULL,
  PRIMARY KEY (`IDCity`),
  UNIQUE KEY `CityName` (`CityName`),
  KEY `CountryID` (`CountryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Cities`
--

INSERT INTO `Cities` (`IDCity`, `CityName`, `CountryID`) VALUES
(1, 'Rzym', 186),
(2, 'Florencja', 186),
(3, 'Mediolan', 186),
(4, 'Padova', 186),
(5, 'Reggio di Calabria', 186),
(6, 'Monte Cassino', 186),
(7, 'Berlin', 122);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Coordinates`
--

DROP TABLE IF EXISTS `Coordinates`;
CREATE TABLE IF NOT EXISTS `Coordinates` (
  `IDCoords` int(11) NOT NULL AUTO_INCREMENT,
  `Lat` float NOT NULL,
  `Lon` float NOT NULL,
  `AddressID` int(11) NOT NULL,
  PRIMARY KEY (`IDCoords`),
  KEY `AddressID` (`AddressID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Coordinates`
--

INSERT INTO `Coordinates` (`IDCoords`, `Lat`, `Lon`, `AddressID`) VALUES
(1, 41.9201, 12.4117, 2),
(2, 41.4941, 13.8057, 3),
(3, 45.4305, 9.15366, 1),
(4, 41.9097, 12.4934, 4),
(5, 38.0548, 15.6781, 5),
(6, 45.4144, 11.9165, 6),
(7, 43.7631, 11.2505, 7),
(11, 52.52, 13.405, 7),
(22, 52.52, 13.405, 21),
(25, 52.5221, 13.4124, 24);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Countries`
--

DROP TABLE IF EXISTS `Countries`;
CREATE TABLE IF NOT EXISTS `Countries` (
  `IDCountry` int(11) NOT NULL AUTO_INCREMENT,
  `CountryCode` varchar(2) NOT NULL,
  `CountryName` varchar(100) NOT NULL,
  `CountryNameEN` varchar(100) NOT NULL,
  `CountryNameDE` varchar(100) NOT NULL,
  PRIMARY KEY (`IDCountry`),
  UNIQUE KEY `CountryCode` (`CountryCode`),
  UNIQUE KEY `CountryName` (`CountryName`),
  UNIQUE KEY `CountryNameEN` (`CountryNameEN`),
  UNIQUE KEY `CountryNameDE` (`CountryNameDE`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Countries`
--

INSERT INTO `Countries` (`IDCountry`, `CountryCode`, `CountryName`, `CountryNameEN`, `CountryNameDE`) VALUES
(1, 'af', 'Afganistan', 'Afghanistan', 'Afghanistan'),
(2, 'al', 'Albania', 'Albania', 'Albanien'),
(3, 'dz', 'Algieria', 'Algeria', 'Algerien'),
(4, 'ad', 'Andora', 'Andorra', 'Andorra'),
(5, 'ao', 'Angola', 'Angola', 'Angola'),
(6, 'ag', 'Antigua i Barbuda', 'Antigua and Barbuda', 'Antigua und Barbuda'),
(7, 'sa', 'Arabia Saudyjska', 'Saudi Arabia', 'Saudi-Arabien'),
(8, 'ar', 'Argentyna', 'Argentina', 'Argentinien'),
(9, 'am', 'Armenia', 'Armenia', 'Armenien'),
(10, 'au', 'Australia', 'Australia', 'Australien'),
(11, 'at', 'Austria', 'Austria', 'Österreich'),
(12, 'az', 'Azerbejdżan', 'Azerbaijan', 'Aserbaidschan'),
(13, 'bs', 'Bahamy', 'Bahamas', 'Bahamas'),
(14, 'bh', 'Bahrajn', 'Bahrain', 'Bahrain'),
(15, 'bd', 'Bangladesz', 'Bangladesh', 'Bangladesch'),
(16, 'bb', 'Barbados', 'Barbados', 'Barbados'),
(17, 'be', 'Belgia', 'Belgium', 'Belgien'),
(18, 'bz', 'Belize', 'Belize', 'Belize'),
(19, 'bj', 'Benin', 'Benin', 'Benin'),
(20, 'bt', 'Bhutan', 'Bhutan', 'Bhutan'),
(21, 'by', 'Białoruś', 'Belarus', 'Weißrussland / Belarus'),
(22, 'mm', 'Birma', 'Myanmar', 'Birma'),
(23, 'bo', 'Boliwia', 'Bolivia', 'Bolivien'),
(24, 'ba', 'Bośnia i Hercegowina', 'Bosnia and Herzegovina', 'Bosnien und Herzegowina'),
(25, 'bw', 'Botswana', 'Botswana', 'Botsuana'),
(26, 'br', 'Brazylia', 'Brazil', 'Brasilien'),
(27, 'bn', 'Brunei', 'Brunei Darussalam', 'Brunei'),
(28, 'bg', 'Bułgaria', 'Bulgaria', 'Bulgarien'),
(29, 'bf', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso'),
(30, 'bi', 'Burundi', 'Burundi', 'Burundi'),
(31, 'cl', 'Chile', 'Chile', 'Chile'),
(32, 'cn', 'Chiny', 'China', 'Republik China'),
(33, 'hr', 'Chorwacja', 'Croatia', 'Kroatien'),
(34, 'cy', 'Cypr', 'Cyprus', 'Zypern'),
(35, 'td', 'Czad', 'Chad', 'Tschad'),
(36, 'me', 'Czarnogóra', 'Montenegro', 'Montenegro'),
(37, 'cz', 'Czechy', 'Czech Republic', 'Tschechien'),
(38, 'dk', 'Dania', 'Denmark', 'Dänemark'),
(39, 'cd', 'Demokratyczna Republika Konga', 'Democratic Republic of Congo', 'Kongo, Republik'),
(40, 'dm', 'Dominika', 'Dominica', 'Dominica'),
(41, 'do', 'Dominikana', 'Dominican Republic', 'Dominikanische Republik'),
(42, 'dj', 'Dżibuti', 'Djibouti', 'Dschibuti'),
(43, 'eg', 'Egipt', 'Egypt', 'Ägypten'),
(44, 'ec', 'Ekwador', 'Ecuador', 'Ecuador'),
(45, 'er', 'Erytrea', 'Eritrea', 'Eritrea'),
(46, 'ee', 'Estonia', 'Estonia', 'Estland'),
(47, 'et', 'Etiopia', 'Ethiopia', 'Äthiopien'),
(48, 'fj', 'Fidżi', 'Fiji', 'Fidschi'),
(49, 'ph', 'Filipiny', 'Philippines', 'Philippinen'),
(50, 'fi', 'Finlandia', 'Finland', 'Finnland'),
(51, 'fr', 'Francja', 'France', 'Frankreich'),
(52, 'ga', 'Gabon', 'Gabon', 'Gabun'),
(53, 'gm', 'Gambia', 'Gambia', 'Gambia'),
(54, 'gh', 'Ghana', 'Ghana', 'Ghana'),
(55, 'gr', 'Grecja', 'Greece', 'Griechenland'),
(56, 'gd', 'Grenada', 'Grenada', 'Grenada'),
(57, 'ge', 'Gruzja', 'Georgia', 'Georgien'),
(58, 'gy', 'Gujana', 'Guyana', 'Guyana'),
(59, 'gt', 'Gwatemala', 'Guatemala', 'Guatemala'),
(60, 'gn', 'Gwinea', 'Guinea', 'Guinea'),
(61, 'gw', 'Gwinea Bissau', 'Guinea-Bissau', 'Guinea-Bissau'),
(62, 'gq', 'Gwinea Równikowa', 'Equatorial Guinea', 'Äquatorialguinea'),
(63, 'ht', 'Haiti', 'Haiti', 'Haiti'),
(64, 'es', 'Hiszpania', 'Spain', 'Spanien'),
(65, 'nl', 'Holandia', 'Netherlands', 'Königreich der Niederlande'),
(66, 'hn', 'Honduras', 'Honduras', 'Honduras'),
(67, 'in', 'Indie', 'India', 'Indien'),
(68, 'id', 'Indonezja', 'Indonesia', 'Indonesien'),
(69, 'iq', 'Irak', 'Iraq', 'Irak'),
(70, 'ir', 'Iran', 'Iran', 'Iran'),
(71, 'ie', 'Irlandia', 'Ireland', 'Irland'),
(72, 'im', 'Islandia', 'Isle of Man', 'Island'),
(73, 'il', 'Izrael', 'Israel', 'Israel'),
(74, 'jm', 'Jamajka', 'Jamaica', 'Jamaika'),
(75, 'jp', 'Japonia', 'Japan', 'Japan'),
(76, 'ye', 'Jemen', 'Yemen', 'Jemen'),
(77, 'jo', 'Jordania', 'Jordan', 'Jordanien'),
(78, 'kh', 'Kambodża', 'Cambodia', 'Kambodscha'),
(79, 'cm', 'Kamerun', 'Cameroon', 'Kamerun'),
(80, 'ca', 'Kanada', 'Canada', 'Kanada'),
(81, 'qa', 'Katar', 'Qatar', 'Katar'),
(82, 'kz', 'Kazachstan', 'Kazakhstan', 'Kasachstan'),
(83, 'ke', 'Kenia', 'Kenya', 'Kenia'),
(84, 'kg', 'Kirgistan', 'Kyrgyzstan', 'Kirgisistan'),
(85, 'ki', 'Kiribati', 'Kiribati', 'Kiribati'),
(86, 'co', 'Kolumbia', 'Colombia', 'Kolumbien'),
(87, 'km', 'Komory', 'Comoros', 'Komoren'),
(88, 'cg', 'Kongo', 'Congo', 'Kongo'),
(89, 'kr', 'Korea Południowa', 'Korea, Republic of', 'Korea, Süd'),
(90, 'kp', 'Korea Północna', 'Korea, Democratic People`s Republic of', 'Korea, Nord'),
(91, 'cr', 'Kostaryka', 'Costa Rica', 'Costa Rica'),
(92, 'cu', 'Kuba', 'Cuba', 'Kuba'),
(93, 'kw', 'Kuwejt', 'Kuwait', 'Kuwait'),
(94, 'la', 'Laos', 'Lao People`s Democratic Republic', 'Laos'),
(95, 'ls', 'Lesotho', 'Lesotho', 'Lesotho'),
(96, 'lb', 'Liban', 'Lebanon', 'Libanon'),
(97, 'lr', 'Liberia', 'Liberia', 'Liberia'),
(98, 'ly', 'Libia', 'Libyan Arab Jamahiriya', 'Libyen'),
(99, 'li', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein'),
(100, 'lt', 'Litwa', 'Lithuania', 'Litauen'),
(101, 'lu', 'Luksemburg', 'Luxembourg', 'Luxemburg'),
(102, 'lv', 'Łotwa', 'Latvia', 'Lettland'),
(103, 'mk', 'Macedonia', 'Macedonia', 'Mazedonien'),
(104, 'mg', 'Madagaskar', 'Madagascar', 'Madagaskar'),
(105, 'mw', 'Malawi', 'Malawi', 'Malawi'),
(106, 'mv', 'Malediwy', 'Maldives', 'Malediven'),
(107, 'my', 'Malezja', 'Malaysia', 'Malaysia'),
(108, 'ml', 'Mali', 'Mali', 'Mali'),
(109, 'mt', 'Malta', 'Malta', 'Malta'),
(110, 'ma', 'Maroko', 'Morocco', 'Marokko'),
(111, 'mr', 'Mauretania', 'Mauritania', 'Mauretanien'),
(112, 'mu', 'Mauritius', 'Mauritius', 'Mauritius'),
(113, 'mx', 'Meksyk', 'Mexico', 'Mexiko'),
(114, 'fm', 'Mikronezja', 'Micronesia', 'Mikronesien'),
(115, 'md', 'Mołdawia', 'Moldova', 'Moldawien'),
(116, 'mc', 'Monako', 'Monaco', 'Monaco'),
(117, 'mn', 'Mongolia', 'Mongolia', 'Mongolei'),
(118, 'mz', 'Mozambik', 'Mozambique', 'Mosambik'),
(119, 'na', 'Namibia', 'Namibia', 'Namibia'),
(120, 'nr', 'Nauru', 'Nauru', 'Nauru'),
(121, 'np', 'Nepal', 'Nepal', 'Nepal'),
(122, 'de', 'Niemcy', 'Germany', 'Deutschland'),
(123, 'ne', 'Niger', 'Niger', 'Niger'),
(124, 'ng', 'Nigeria', 'Nigeria', 'Nigeria'),
(125, 'ni', 'Nikaragua', 'Nicaragua', 'Nicaragua'),
(126, 'no', 'Norwegia', 'Norway', 'Norwegen'),
(127, 'nz', 'Nowa Zelandia', 'New Zealand', 'Neuseeland'),
(128, 'om', 'Oman', 'Oman', 'Oman'),
(129, 'pk', 'Pakistan', 'Pakistan', 'Pakistan'),
(130, 'pw', 'Palau', 'Palau', 'Palau'),
(131, 'pa', 'Panama', 'Panama', 'Panama'),
(132, 'pg', 'Papua-Nowa Gwinea', 'Papua New Guinea', 'Papua-Neuguinea'),
(133, 'py', 'Paragwaj', 'Paraguay', 'Paraguay'),
(134, 'pe', 'Peru', 'Peru', 'Peru'),
(135, 'pl', 'Polska', 'Poland', 'Polen'),
(136, 'pt', 'Portugalia', 'Portugal', 'Portugal'),
(137, 'za', 'Republika Południowej Afryki', 'South Africa', 'Südafrika'),
(138, 'cf', 'Republika Środkowoafrykańska', 'Central African Republic', 'Zentralafrikanische Republik'),
(139, 'cv', 'Republika Zielonego Przylądka', 'Cape Verde', 'Kap Verde'),
(140, 'ru', 'Rosja', 'Russian Federation', 'Russland'),
(141, 'ro', 'Rumunia', 'Romania', 'Rumänien'),
(142, 'rw', 'Rwanda', 'Rwanda', 'Ruanda'),
(143, 'kn', 'Saint Kitts i Nevis', 'Saint Kitts and Nevis', 'St. Kitts und Nevis'),
(144, 'lc', 'Saint Lucia', 'Saint Lucia', 'St. Lucia'),
(145, 'vc', 'Saint Vincent i Grenadyny', 'Saint Vincent and the Grenadines', 'St. Vincent und die Grenadinen'),
(146, 'sv', 'Salwador', 'El Salvador', 'El Salvador'),
(147, 'ws', 'Samoa', 'Samoa', 'Samoa'),
(148, 'sm', 'San Marino', 'San Marino', 'San Marino'),
(149, 'sn', 'Senegal', 'Senegal', 'Senegal'),
(150, 'rs', 'Serbia', 'Serbia', 'Serbien'),
(151, 'sc', 'Seszele', 'Seychelles', 'Seychellen'),
(152, 'sl', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone'),
(153, 'sg', 'Singapur', 'Singapore', 'Singapur'),
(154, 'sk', 'Słowacja', 'Slovakia', 'Slowakei'),
(155, 'si', 'Słowenia', 'Slovenia', 'Slowenien'),
(156, 'so', 'Somalia', 'Somalia', 'Somalia'),
(157, 'lk', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka'),
(158, 'us', 'Stany Zjednoczone', 'United States', 'Vereinigte Staaten'),
(159, 'sz', 'Suazi', 'Swaziland', 'Swasiland'),
(160, 'sd', 'Sudan', 'Sudan', 'Sudan'),
(161, 'sr', 'Surinam', 'Suriname', 'Suriname'),
(162, 'sy', 'Syria', 'Syrian Arab Republic', 'Syrien'),
(163, 'ch', 'Szwajcaria', 'Switzerland', 'Schweiz'),
(164, 'se', 'Szwecja', 'Sweden', 'Schweden'),
(165, 'tj', 'Tadżykistan', 'Tajikistan', 'Tadschikistan'),
(166, 'th', 'Tajlandia', 'Thailand', 'Thailand'),
(167, 'tz', 'Tanzania', 'Tanzania', 'Tansania'),
(168, 'tl', 'Timor Wschodni', 'Timor-Leste', 'Osttimor / Timor-Leste'),
(169, 'tg', 'Togo', 'Togo', 'Togo'),
(170, 'to', 'Tonga', 'Tonga', 'Tonga'),
(171, 'tt', 'Trynidad i Tobago', 'Trinidad and Tobago', 'Trinidad und Tobago'),
(172, 'tn', 'Tunezja', 'Tunisia', 'Tunesien'),
(173, 'tr', 'Turcja', 'Turkey', 'Türkei'),
(174, 'tm', 'Turkmenistan', 'Turkmenistan', 'Turkmenistan'),
(175, 'tv', 'Tuvalu', 'Tuvalu', 'Tuvalu'),
(176, 'ug', 'Uganda', 'Uganda', 'Uganda'),
(177, 'ua', 'Ukraina', 'Ukraine', 'Ukraine'),
(178, 'uy', 'Urugwaj', 'Uruguay', 'Uruguay'),
(179, 'uz', 'Uzbekistan', 'Uzbekistan', 'Usbekistan'),
(180, 'vu', 'Vanuatu', 'Vanuatu', 'Vanuatu'),
(181, 'va', 'Watykan', 'Vatican', 'Vatikan'),
(182, 've', 'Wenezuela', 'Venezuela', 'Venezuela'),
(183, 'hu', 'Węgry', 'Hungary', 'Ungarn'),
(184, 'gb', 'Wielka Brytania', 'United Kingdom', 'Großbritannien'),
(185, 'vn', 'Wietnam', 'Viet Nam', 'Vietnam'),
(186, 'it', 'Włochy', 'Italy', 'Italien'),
(187, 'ci', 'Wybrzeże Kości Słoniowej', 'Côte d`Ivoire', 'Elfenbeinküste'),
(188, 'mh', 'Wyspy Marshalla', 'Marshall Islands', 'Marshallinseln'),
(189, 'sb', 'Wyspy Salomona', 'Solomon Islands', 'Salomonen'),
(190, 'st', 'Wyspy Świętego Tomasza i Książęca', 'Sao Tome and Principe', 'Sao Tomé und Príncipe'),
(191, 'zm', 'Zambia', 'Zambia', 'Sambia'),
(192, 'zw', 'Zimbabwe', 'Zimbabwe', 'Zimbabwe'),
(193, 'ae', 'Zjednoczone Emiraty Arabskie', 'United Arab Emirates', 'Vereinigte Arabische Emirate');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Fidelities`
--

DROP TABLE IF EXISTS `Fidelities`;
CREATE TABLE IF NOT EXISTS `Fidelities` (
  `IDFidelity` int(11) NOT NULL AUTO_INCREMENT,
  `FidelityName` text NOT NULL,
  PRIMARY KEY (`IDFidelity`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Fidelities`
--

INSERT INTO `Fidelities` (`IDFidelity`, `FidelityName`) VALUES
(1, 'budynek'),
(2, 'ulica'),
(3, 'miasto'),
(4, 'region'),
(5, 'państwo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `LinkCategories`
--

DROP TABLE IF EXISTS `LinkCategories`;
CREATE TABLE IF NOT EXISTS `LinkCategories` (
  `IDLinkCat` int(11) NOT NULL AUTO_INCREMENT,
  `LinkCatName` varchar(24) NOT NULL,
  PRIMARY KEY (`IDLinkCat`),
  UNIQUE KEY `LinkCatName` (`LinkCatName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `LinkCategories`
--

INSERT INTO `LinkCategories` (`IDLinkCat`, `LinkCatName`) VALUES
(1, 'facebook'),
(2, 'twitter');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Links`
--

DROP TABLE IF EXISTS `Links`;
CREATE TABLE IF NOT EXISTS `Links` (
  `IDLinks` int(11) NOT NULL AUTO_INCREMENT,
  `URL` text NOT NULL,
  `POIID` int(11) NOT NULL,
  `LinkCatID` int(11) NOT NULL,
  PRIMARY KEY (`IDLinks`),
  KEY `LinkCatID` (`LinkCatID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Links`
--

INSERT INTO `Links` (`IDLinks`, `URL`, `POIID`, `LinkCatID`) VALUES
(1, '8', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Photos`
--

DROP TABLE IF EXISTS `Photos`;
CREATE TABLE IF NOT EXISTS `Photos` (
  `IDPhotos` int(11) NOT NULL AUTO_INCREMENT,
  `PhotoPath` varchar(100) NOT NULL,
  `POIID` int(11) NOT NULL,
  PRIMARY KEY (`IDPhotos`),
  KEY `POIID` (`POIID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Photos`
--

INSERT INTO `Photos` (`IDPhotos`, `PhotoPath`, `POIID`) VALUES
(1, '', 8),
(2, '', 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `POIs`
--

DROP TABLE IF EXISTS `POIs`;
CREATE TABLE IF NOT EXISTS `POIs` (
  `IDPOI` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `SubcategoryID` int(11) DEFAULT NULL,
  `CoordinatesID` int(11) NOT NULL,
  `LinkID` int(11) DEFAULT NULL,
  `TypeID` int(11) NOT NULL,
  `Logo` text NOT NULL,
  `ProfPhoto` text,
  `TagsID` int(11) DEFAULT NULL,
  `Description` text,
  `PhotoID` int(11) DEFAULT NULL,
  `VideoID` int(11) DEFAULT NULL,
  `WWW` text,
  `Phone` text,
  `Mail` varchar(320) DEFAULT NULL,
  `FidelityID` int(11) NOT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdaterID` int(11) DEFAULT '1',
  `POIStatusID` int(11) NOT NULL,
  `CheckDate` date DEFAULT NULL,
  PRIMARY KEY (`IDPOI`),
  UNIQUE KEY `Name` (`Name`),
  KEY `CategoryID` (`CategoryID`),
  KEY `SubcategoryID` (`SubcategoryID`),
  KEY `CoordinatesID` (`CoordinatesID`),
  KEY `TypeID` (`TypeID`),
  KEY `FidelityID` (`FidelityID`),
  KEY `UpdaterID` (`UpdaterID`),
  KEY `POIStatusID` (`POIStatusID`),
  KEY `TagsID` (`TagsID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `POIs`
--

INSERT INTO `POIs` (`IDPOI`, `Name`, `CategoryID`, `SubcategoryID`, `CoordinatesID`, `LinkID`, `TypeID`, `Logo`, `ProfPhoto`, `TagsID`, `Description`, `PhotoID`, `VideoID`, `WWW`, `Phone`, `Mail`, `FidelityID`, `UpdateDate`, `UpdaterID`, `POIStatusID`, `CheckDate`) VALUES
(1, 'Ristorante Polacco Bajka', 1, 2, 1, NULL, 1, 'assets/img/icons/restaurant.png', 'assets/img/items/34.png', NULL, 'Lorem ipsum..', NULL, NULL, 'http://bajka.it', '+39 328 803 0162', 'mail@bajka.it', 1, '2018-11-23 18:58:50', 1, 1, NULL),
(2, 'Monte Cassino', 6, NULL, 2, NULL, 2, 'assets/img/icons/memorialsite.png', 'assets/img/items/32.png', NULL, 'Lorem ipsum..', NULL, NULL, NULL, NULL, NULL, 2, '2018-11-23 18:58:50', 1, 1, NULL),
(3, 'Centauro di Mitoraj', 3, 15, 3, NULL, 2, 'assets/img/icons/art.png', 'assets/img/items/33.png', NULL, 'Lorem ipsum..', NULL, NULL, NULL, NULL, NULL, 1, '2018-11-23 18:58:50', 1, 1, NULL),
(4, 'Związek Polaków we Włoszech', 2, 9, 4, NULL, 2, 'assets/img/icons/organization.png', 'assets/img/items/36.png', NULL, 'Lorem ipsum..', NULL, NULL, 'http://polonia-wloska.org', '+39 555 444 333', 'http://polonia-wloska.org/kontakt', 1, '2018-11-23 18:58:50', 1, 1, NULL),
(5, 'Związek Polaków w Kalabrii', 2, 9, 5, NULL, 2, 'assets/img/icons/organization.png', 'assets/img/items/37.png', NULL, 'Lorem ipsum..', NULL, NULL, 'http://www.associazionepolacchiincalabria.it', '0965-624466', 'assopolca@yahoo.it', 1, '2018-11-23 18:58:50', 1, 1, NULL),
(6, 'Scuola di Lingua e Cultura Polacca', 2, 10, 6, NULL, 2, 'assets/img/icons/education.png', 'assets/img/items/38.png', NULL, 'Lorem ipsum..', NULL, NULL, 'http://www.polskaszkola-padwa.pl', '+39 328 803 0162', 'aipp.szkolapolska@gmail.com', 1, '2018-11-23 18:58:50', 1, 1, NULL),
(7, 'Bronze head of Igor Mitoraj', 3, 15, 7, NULL, 2, 'assets/img/icons/art.png', 'assets/img/items/39.png', NULL, 'Lorem ipsum..', NULL, NULL, NULL, NULL, NULL, 1, '2018-11-23 18:58:50', 1, 1, NULL),
(8, 'test', 1, 1, 22, 1, 1, 'assets/img/icons/art.png', '', 1, 'testing', NULL, NULL, 'www.example.com', '123456', 'mail@mail.com', 3, '2018-11-25 23:00:00', 1, 1, '2018-11-26'),
(9, 'test2', 1, 1, 25, NULL, 2, 'assets/img/icons/organization.png', '', 1, 'testing2', NULL, NULL, 'www.example.com', '123456', 'mail@mail.com', 2, '2018-11-27 08:05:02', 19, 1, '2018-11-27');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `POIStatuses`
--

DROP TABLE IF EXISTS `POIStatuses`;
CREATE TABLE IF NOT EXISTS `POIStatuses` (
  `IDPOIStatus` int(11) NOT NULL AUTO_INCREMENT,
  `POIStatusName` varchar(12) NOT NULL DEFAULT 'existent',
  PRIMARY KEY (`IDPOIStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `POIStatuses`
--

INSERT INTO `POIStatuses` (`IDPOIStatus`, `POIStatusName`) VALUES
(1, 'existent'),
(2, 'non-existent');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `POIVideos`
--

DROP TABLE IF EXISTS `POIVideos`;
CREATE TABLE IF NOT EXISTS `POIVideos` (
  `IDVideo` int(11) NOT NULL AUTO_INCREMENT,
  `VideoLink` text NOT NULL,
  PRIMARY KEY (`IDVideo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `StreetView`
--

DROP TABLE IF EXISTS `StreetView`;
CREATE TABLE IF NOT EXISTS `StreetView` (
  `IDSV` int(11) NOT NULL AUTO_INCREMENT,
  `POIID` int(11) NOT NULL,
  `Heading` float NOT NULL,
  `Pitch` float NOT NULL,
  `Zoom` int(11) NOT NULL,
  PRIMARY KEY (`IDSV`),
  KEY `POIID` (`POIID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Subcategories`
--

DROP TABLE IF EXISTS `Subcategories`;
CREATE TABLE IF NOT EXISTS `Subcategories` (
  `IDSubctgry` int(11) NOT NULL AUTO_INCREMENT,
  `SubcatName` varchar(50) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  PRIMARY KEY (`IDSubctgry`),
  UNIQUE KEY `SubcatName` (`SubcatName`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Subcategories`
--

INSERT INTO `Subcategories` (`IDSubctgry`, `SubcatName`, `CategoryID`) VALUES
(1, 'Polskie delikatesy', 1),
(2, 'Sklepy z polskimi produktami', 1),
(3, 'Polskie piekarnie', 1),
(4, 'Inne polskie sklepy', 1),
(5, 'Polskie restauracje', 2),
(6, 'Polskie kawiarnie', 2),
(7, 'Polskie puby', 2),
(8, 'Polskie kluby i dyskoteki', 2),
(9, 'Polscy lekarze', 3),
(10, 'Polscy prawnicy', 3),
(11, 'Polscy tłumacze', 3),
(12, 'Polskie biura podróży', 3),
(13, 'Inne polskie usługi', 3),
(14, ' Polskie firmy', 4),
(15, 'Ambasady', 5),
(16, 'Konsulaty generalne', 5),
(17, 'Konsulaty', 5),
(18, 'Agencje konsularne', 5),
(19, 'Konsulaty honorowe', 5),
(20, 'Instytuty polskie', 5),
(21, 'Domy polskie', 6),
(22, 'Muzea', 6),
(23, 'Teatry', 6),
(24, 'Biblioteki', 6),
(25, 'Siedziby mediów polonijnych', 6),
(26, 'Szkoły i kluby sportowe', 6),
(27, 'Kulturalne', 7),
(28, 'Oświatowe', 7),
(29, 'Kombatanckie', 7),
(30, 'Religijne', 7),
(31, 'Ogólne / inne', 7),
(32, 'Uczelnie wyższe - fakultety', 8),
(33, 'Polskie szkoły i z nauką j. polskiego', 8),
(34, 'Szkoły nauki języka polskiego', 8),
(35, 'Inne ośrodki szkoleniowe', 8),
(36, 'Ulice', 9),
(37, 'Ronda', 9),
(38, 'Skwery, parki', 9),
(39, 'Dzielnice, wioski, miasta', 9),
(40, 'Instytucje im. Wielkich Polaków', 10),
(41, 'Budynki polskich architektów', 10),
(42, 'Tablice pamiątkowe', 10),
(43, 'Rzeżby, posągi, pomniki', 10),
(44, 'Miejsca sportowych sukcesów', 10),
(45, 'Inne miejsca zw. z polakami', 10),
(46, 'Szlaki turystyczne', 11),
(47, 'Góry', 11),
(48, 'Rzeki', 11),
(49, 'Kotliny', 11),
(50, 'Inne', 11),
(51, 'Polskie parafie', 12),
(52, 'Polskie kościoły', 12),
(53, 'Polscy księża i misjonarze', 12),
(54, 'Nabożeństwa w języku polskim', 12),
(55, 'Pomniki bohaterów', 13),
(56, 'Miejsca pamięci', 13),
(57, 'Groby wielkich polaków', 13),
(58, 'Polskie cmentarze i kwatery', 13),
(59, 'Inne historyczne', 13),
(60, 'Polskie sklepy', 1),
(64, 'Polscy usługodawcy', 1),
(65, 'Polskie firmy za granicą', 1),
(66, 'Dyplomatyczne, konsularne', 2),
(67, 'Kulturalne i handlowe', 2),
(68, 'Instytucje i organizacje lokalne', 2),
(69, 'Uczelnie, szkoły', 2),
(70, 'Ośrodki naukowe, szkoleniowe', 2),
(71, 'Ulice, skwery, ronda, parki', 3),
(73, 'Miasta partnerskie', 3),
(77, 'Miejsca geograficzne', 4),
(78, 'Polscy odkrywcy i podróżnicy', 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Tags`
--

DROP TABLE IF EXISTS `Tags`;
CREATE TABLE IF NOT EXISTS `Tags` (
  `IDTag` int(11) NOT NULL AUTO_INCREMENT,
  `TagName` text,
  `POIID` int(11) NOT NULL,
  PRIMARY KEY (`IDTag`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Tags`
--

INSERT INTO `Tags` (`IDTag`, `TagName`, `POIID`) VALUES
(1, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Types`
--

DROP TABLE IF EXISTS `Types`;
CREATE TABLE IF NOT EXISTS `Types` (
  `IDType` int(11) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(12) NOT NULL,
  PRIMARY KEY (`IDType`),
  UNIQUE KEY `TypeName` (`TypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Types`
--

INSERT INTO `Types` (`IDType`, `TypeName`) VALUES
(1, 'commerce'),
(2, 'non-commerce');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Updaters`
--

DROP TABLE IF EXISTS `Updaters`;
CREATE TABLE IF NOT EXISTS `Updaters` (
  `IDUpdater` int(11) NOT NULL AUTO_INCREMENT,
  `UpdaterName` text NOT NULL,
  PRIMARY KEY (`IDUpdater`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Updaters`
--

INSERT INTO `Updaters` (`IDUpdater`, `UpdaterName`) VALUES
(1, 'updater1'),
(5, 'updater1'),
(16, 'updater1'),
(19, 'updater1');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view1`
-- (Zobacz poniżej rzeczywisty widok)
--
DROP VIEW IF EXISTS `view1`;
CREATE TABLE IF NOT EXISTS `view1` (
`id` int(11)
,`title` varchar(100)
,`marker_image` text
,`cat_id` int(11)
,`category_pl` text
,`subcat_id` int(11)
,`subcat_pl` varchar(50)
,`latitude` float
,`longitude` float
,`phone` text
,`email` varchar(320)
,`website` text
,`sidebar_image` text
,`description` text
,`cityid` int(11)
,`location` text
,`city_pl` varchar(100)
,`cntry_id` int(11)
,`cntry_pl` varchar(100)
,`typeid` int(11)
,`fb_url` text
,`photo1` varchar(100)
,`photo2` varchar(100)
,`update_date` timestamp
,`check_date` date
,`sv_heading` float
,`sv_pitch` float
,`sv_zoom` int(11)
,`monday_open` time
,`monday_close` time
,`tuesday_open` time
,`tuesday_close` time
,`wednesday_open` time
,`wednesday_close` time
,`thursday_open` time
,`thursday_close` time
,`friday_open` time
,`friday_close` time
,`saturday_open` time
,`saturday_close` time
,`sunday_open` time
,`sunday_close` time
,`featured` binary(0)
,`rating` binary(0)
,`reviews_number` binary(0)
,`url` binary(0)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `WorkingHours`
--

DROP TABLE IF EXISTS `WorkingHours`;
CREATE TABLE IF NOT EXISTS `WorkingHours` (
  `IDHours` int(11) NOT NULL AUTO_INCREMENT,
  `POIID` int(11) NOT NULL,
  `WeekDay` int(11) NOT NULL,
  `OpenTime` time NOT NULL,
  `CloseTime` time NOT NULL,
  PRIMARY KEY (`IDHours`),
  KEY `POIID` (`POIID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura widoku `view1`
--
DROP TABLE IF EXISTS `view1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`v55582464_mapusr`@`localhost` SQL SECURITY DEFINER VIEW `view1`  AS  select `p`.`IDPOI` AS `id`,`p`.`Name` AS `title`,`p`.`Logo` AS `marker_image`,`c`.`IDCtgry` AS `cat_id`,`c`.`CatName` AS `category_pl`,`p`.`SubcategoryID` AS `subcat_id`,`sc`.`SubcatName` AS `subcat_pl`,`xy`.`Lat` AS `latitude`,`xy`.`Lon` AS `longitude`,`p`.`Phone` AS `phone`,`p`.`Mail` AS `email`,`p`.`WWW` AS `website`,`p`.`ProfPhoto` AS `sidebar_image`,`p`.`Description` AS `description`,`a`.`CityID` AS `cityid`,`a`.`Address` AS `location`,`ct`.`CityName` AS `city_pl`,`cnt`.`IDCountry` AS `cntry_id`,`cnt`.`CountryName` AS `cntry_pl`,`p`.`TypeID` AS `typeid`,`l`.`URL` AS `fb_url`,`ph`.`PhotoPath` AS `photo1`,`ph2`.`PhotoPath` AS `photo2`,`p`.`UpdateDate` AS `update_date`,`p`.`CheckDate` AS `check_date`,`sv`.`Heading` AS `sv_heading`,`sv`.`Pitch` AS `sv_pitch`,`sv`.`Zoom` AS `sv_zoom`,`h1`.`OpenTime` AS `monday_open`,`h1`.`CloseTime` AS `monday_close`,`h2`.`OpenTime` AS `tuesday_open`,`h2`.`CloseTime` AS `tuesday_close`,`h3`.`OpenTime` AS `wednesday_open`,`h3`.`CloseTime` AS `wednesday_close`,`h4`.`OpenTime` AS `thursday_open`,`h4`.`CloseTime` AS `thursday_close`,`h5`.`OpenTime` AS `friday_open`,`h5`.`CloseTime` AS `friday_close`,`h6`.`OpenTime` AS `saturday_open`,`h6`.`CloseTime` AS `saturday_close`,`h7`.`OpenTime` AS `sunday_open`,`h7`.`CloseTime` AS `sunday_close`,NULL AS `featured`,NULL AS `rating`,NULL AS `reviews_number`,NULL AS `url` from (((((((((((((((((`POIs` `p` join `Categories` `c` on((`p`.`CategoryID` = `c`.`IDCtgry`))) join `Subcategories` `sc` on((`p`.`SubcategoryID` = `sc`.`IDSubctgry`))) join `Coordinates` `xy` on((`p`.`CoordinatesID` = `xy`.`IDCoords`))) join `Addresses` `a` on((`xy`.`AddressID` = `a`.`IDAddress`))) join `Cities` `ct` on((`a`.`CityID` = `ct`.`IDCity`))) join `Countries` `cnt` on((`ct`.`CountryID` = `cnt`.`IDCountry`))) left join `Links` `l` on(((`p`.`IDPOI` = `l`.`POIID`) and (`l`.`LinkCatID` = 1)))) left join `Photos` `ph` on((`ph`.`IDPhotos` = (select `ph`.`IDPhotos` from `Photos` `ph` where (`ph`.`POIID` = `p`.`IDPOI`) order by `ph`.`IDPhotos` limit 1,1)))) left join `Photos` `ph2` on((`ph2`.`IDPhotos` = (select `ph2`.`IDPhotos` from `Photos` `ph2` where (`ph2`.`POIID` = `p`.`IDPOI`) order by `ph2`.`IDPhotos` limit 2,1)))) left join `StreetView` `sv` on((`p`.`IDPOI` = `sv`.`POIID`))) left join `WorkingHours` `h1` on((`h1`.`IDHours` = (select `h1`.`IDHours` from `WorkingHours` `h1` where ((`h1`.`POIID` = `p`.`IDPOI`) and (`h1`.`WeekDay` = 1)))))) left join `WorkingHours` `h2` on((`h2`.`IDHours` = (select `h2`.`IDHours` from `WorkingHours` `h2` where ((`h2`.`POIID` = `p`.`IDPOI`) and (`h2`.`WeekDay` = 2)))))) left join `WorkingHours` `h3` on((`h3`.`IDHours` = (select `h3`.`IDHours` from `WorkingHours` `h3` where ((`h3`.`POIID` = `p`.`IDPOI`) and (`h3`.`WeekDay` = 3)))))) left join `WorkingHours` `h4` on((`h4`.`IDHours` = (select `h4`.`IDHours` from `WorkingHours` `h4` where ((`h4`.`POIID` = `p`.`IDPOI`) and (`h4`.`WeekDay` = 4)))))) left join `WorkingHours` `h5` on((`h5`.`IDHours` = (select `h5`.`IDHours` from `WorkingHours` `h5` where ((`h5`.`POIID` = `p`.`IDPOI`) and (`h5`.`WeekDay` = 5)))))) left join `WorkingHours` `h6` on((`h6`.`IDHours` = (select `h6`.`IDHours` from `WorkingHours` `h6` where ((`h6`.`POIID` = `p`.`IDPOI`) and (`h6`.`WeekDay` = 6)))))) left join `WorkingHours` `h7` on((`h7`.`IDHours` = (select `h7`.`IDHours` from `WorkingHours` `h7` where ((`h7`.`POIID` = `p`.`IDPOI`) and (`h7`.`WeekDay` = 7)))))) ;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `Addresses`
--
ALTER TABLE `Addresses`
  ADD CONSTRAINT `Addresses_ibfk_1` FOREIGN KEY (`CityID`) REFERENCES `Cities` (`IDCity`);

--
-- Ograniczenia dla tabeli `Cities`
--
ALTER TABLE `Cities`
  ADD CONSTRAINT `Cities_ibfk_1` FOREIGN KEY (`CountryID`) REFERENCES `Countries` (`IDCountry`);

--
-- Ograniczenia dla tabeli `Coordinates`
--
ALTER TABLE `Coordinates`
  ADD CONSTRAINT `Coordinates_ibfk_1` FOREIGN KEY (`AddressID`) REFERENCES `Addresses` (`IDAddress`);

--
-- Ograniczenia dla tabeli `Links`
--
ALTER TABLE `Links`
  ADD CONSTRAINT `Links_ibfk_1` FOREIGN KEY (`LinkCatID`) REFERENCES `LinkCategories` (`IDLinkCat`);

--
-- Ograniczenia dla tabeli `Photos`
--
ALTER TABLE `Photos`
  ADD CONSTRAINT `Photos_ibfk_1` FOREIGN KEY (`POIID`) REFERENCES `POIs` (`IDPOI`);

--
-- Ograniczenia dla tabeli `POIs`
--
ALTER TABLE `POIs`
  ADD CONSTRAINT `POIs_ibfk_1` FOREIGN KEY (`CategoryID`) REFERENCES `Categories` (`IDCtgry`),
  ADD CONSTRAINT `POIs_ibfk_2` FOREIGN KEY (`SubcategoryID`) REFERENCES `Subcategories` (`IDSubctgry`),
  ADD CONSTRAINT `POIs_ibfk_3` FOREIGN KEY (`CoordinatesID`) REFERENCES `Coordinates` (`IDCoords`),
  ADD CONSTRAINT `POIs_ibfk_4` FOREIGN KEY (`TypeID`) REFERENCES `Types` (`IDType`),
  ADD CONSTRAINT `POIs_ibfk_5` FOREIGN KEY (`FidelityID`) REFERENCES `Fidelities` (`IDFidelity`),
  ADD CONSTRAINT `POIs_ibfk_6` FOREIGN KEY (`UpdaterID`) REFERENCES `Updaters` (`IDUpdater`),
  ADD CONSTRAINT `POIs_ibfk_7` FOREIGN KEY (`POIStatusID`) REFERENCES `POIStatuses` (`IDPOIStatus`),
  ADD CONSTRAINT `POIs_ibfk_8` FOREIGN KEY (`TagsID`) REFERENCES `Tags` (`IDTag`);

--
-- Ograniczenia dla tabeli `StreetView`
--
ALTER TABLE `StreetView`
  ADD CONSTRAINT `StreetView_ibfk_1` FOREIGN KEY (`POIID`) REFERENCES `POIs` (`IDPOI`);

--
-- Ograniczenia dla tabeli `Subcategories`
--
ALTER TABLE `Subcategories`
  ADD CONSTRAINT `Subcategories_ibfk_1` FOREIGN KEY (`CategoryID`) REFERENCES `Categories` (`IDCtgry`);

--
-- Ograniczenia dla tabeli `WorkingHours`
--
ALTER TABLE `WorkingHours`
  ADD CONSTRAINT `WorkingHours_ibfk_1` FOREIGN KEY (`POIID`) REFERENCES `POIs` (`IDPOI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
