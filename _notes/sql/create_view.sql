DROP VIEW IF EXISTS view1;
CREATE VIEW view1 AS SELECT 
	p.IDPOI as 'id', 
	p.Name as 'title', 
	p.Logo as 'marker_image',
	c.IDCtgry as 'cat_id',
	c.CatName as 'category_pl',
	p.SubcategoryID as 'subcat_id',
	sc.SubcatName as 'subcat_pl',
	xy.Lat as 'latitude',
	xy.Lon as 'longitude',
	p.Phone as 'phone',
	p.Mail as 'email',
	p.WWW as 'website',
	p.ProfPhoto as 'sidebar_image',
	p.Description as 'description',
	a.CityID as 'cityid',
	a.Address as 'location',
	ct.CityName as 'city_pl',
	cnt.IDCountry as 'cntry_id',
	cnt.CountryName as 'cntry_pl',
	p.TypeID as 'typeid',
	l.URL as 'fb_url',
	ph.PhotoPath as 'photo1',
	ph2.PhotoPath as 'photo2',
	p.UpdateDate as 'update_date',
	p.CheckDate as 'check_date',
	sv.heading as 'sv_heading',
	sv.pitch as 'sv_pitch',
	sv.zoom as 'sv_zoom',
	h1.OpenTime as 'monday_open',
	h1.CloseTime as 'monday_close',
	h2.OpenTime as 'tuesday_open',
	h2.CloseTime as 'tuesday_close',
	h3.OpenTime as 'wednesday_open',
	h3.CloseTime as 'wednesday_close',
	h4.OpenTime as 'thursday_open',
	h4.CloseTime as 'thursday_close',
	h5.OpenTime as 'friday_open',
	h5.CloseTime as 'friday_close',
	h6.OpenTime as 'saturday_open',
	h6.CloseTime as 'saturday_close',
	h7.OpenTime as 'sunday_open',
	h7.CloseTime as 'sunday_close',
	Null as 'featured',
	Null as 'rating',
	Null as 'reviews_number',
	Null as 'url'
FROM POIs as p 
JOIN Categories as c ON (p.CategoryID=c.IDCtgry)
JOIN Subcategories as sc ON (p.SubcategoryID=sc.IDSubctgry)
JOIN Coordinates as xy ON (p.CoordinatesID=xy.IDCoords)
JOIN Addresses as a ON (xy.AddressID=a.IDAddress)
JOIN Cities as ct ON (a.CityID=ct.IDCity)
JOIN Countries as cnt ON (ct.CountryID=cnt.IDCountry)
LEFT JOIN Links as l ON (p.IDPOI=l.POIID) AND (l.LinkCatID=1)
LEFT JOIN Photos as ph on ph.IDPhotos = (SELECT IDPhotos from Photos ph WHERE ph.POIID = p.IDPOI ORDER BY IDPhotos asc LIMIT 1 OFFSET 1)
LEFT JOIN Photos as ph2 on ph2.IDPhotos = (SELECT IDPhotos from Photos ph2 WHERE ph2.POIID = p.IDPOI ORDER BY IDPhotos asc LIMIT 1 OFFSET 2)
LEFT JOIN StreetView as sv ON (p.IDPOI=sv.POIID)
LEFT JOIN WorkingHours as h1 on h1.IDHours = (SELECT IDHours from WorkingHours h1 WHERE h1.POIID = p.IDPOI AND h1.WeekDay = 1)
LEFT JOIN WorkingHours as h2 on h2.IDHours = (SELECT IDHours from WorkingHours h2 WHERE h2.POIID = p.IDPOI AND h2.WeekDay = 2)
LEFT JOIN WorkingHours as h3 on h3.IDHours = (SELECT IDHours from WorkingHours h3 WHERE h3.POIID = p.IDPOI AND h3.WeekDay = 3)
LEFT JOIN WorkingHours as h4 on h4.IDHours = (SELECT IDHours from WorkingHours h4 WHERE h4.POIID = p.IDPOI AND h4.WeekDay = 4)
LEFT JOIN WorkingHours as h5 on h5.IDHours = (SELECT IDHours from WorkingHours h5 WHERE h5.POIID = p.IDPOI AND h5.WeekDay = 5)
LEFT JOIN WorkingHours as h6 on h6.IDHours = (SELECT IDHours from WorkingHours h6 WHERE h6.POIID = p.IDPOI AND h6.WeekDay = 6)
LEFT JOIN WorkingHours as h7 on h7.IDHours = (SELECT IDHours from WorkingHours h7 WHERE h7.POIID = p.IDPOI AND h7.WeekDay = 7)
;

SELECT * from view1;