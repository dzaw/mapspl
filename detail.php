<?php

require('assets/external/db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$postid = htmlspecialchars($_POST["id"]);
$getid = htmlspecialchars($_GET["id"]);

$idinfo = preg_replace("/[^0-9,.]/", "", $getid ); //cut all non-numeric symbols from query string

//echo $_SERVER['PHP_SELF'] . "?id=" . $idinfo;
//header('Location: http:\/\/155.133.43.155/' . $_SERVER['PHP_SELF'] . '?id=' . $idinfo);

$query = "SELECT * FROM view1 WHERE id = " . $idinfo . ";";

$sth = $DBcon->prepare($query);
$sth->execute();

if ($sth->rowCount() > 0) {
//   echo 'ok';
//   echo gettype(intval($getid));
}
else {
   echo 'don\'t mess with url get parameters..';
}


while ($data = $sth->fetchAll(PDO::FETCH_ASSOC)) {
//var_dump($result);

        foreach ($data as $data_poi => $data_a){
                //echo $val[0]; // echoes cities in all countries on pos 0
                //echo $result['inne'][0]; // echoes just city in one cntry on pos 0
                //echo $key; // echoes cntries
                //echo 'ok';
        //}
//}



echo '
<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ThemeStarz">

    <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href="https:\/\/fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jquery.nouislider.min.css" type="text/css">

    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">

    <title>Details</title>

</head>

<body class="subpage-detail">
<div class="page-wrapper">
    <header id="page-header">
        <nav>
            <div class="left">
                <a href="index.html" class="brand">
                <i class="fa fa-globe fa-2x"></i>
                </a>
            </div>
	    <div class="left">
            <h4>Polski ATLAS 2017</h4>
	    </div>
            <!--end left-->
            <div class="right">
                <div class="primary-nav has-mega-menu">
                    <ul class="navigation">
                        <li class="active has-child"><a href="index.html#nav-homepages">Na początek</a>
                            <div class="wrapper">
                                <div id="nav-homepages" class="nav-wrapper">
                                    <ul>
                                        <li><a href="#">O projekcie</a></li>
                                        <li><a href="#">Perspektywy</a></li>
                                        <li><a href="#">Autorzy</a></li>
                                        <li><a href="#">Współpraca</a></li>
                                        <li><a href="#">Zaproszenie</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="has-child"><a href="index.html#nav-listing">Dane</a>
                            <div class="wrapper">
                                <div id="nav-listing" class="nav-wrapper">
                                    <ul>
                                        <li class="has-child"><a href="index.html#nav-grid-listing">Grid Listing</a>
                                            <div id="nav-grid-listing" class="nav-wrapper">
                                                <ul>
                                                    <li><a href="listing-grid-right-sidebar.html">With Right Sidebar</a></li>
                                                    <li><a href="listing-grid-left-sidebar.html">With Left Sidebar</a></li>
                                                    <li><a href="listing-grid-full-width.html">Full Width</a></li>
                                                    <li><a href="listing-grid-different-widths.html">Different Widths</a></li>
                                                    <li><a href="listing-grid-3-items.html">3 Items in Row</a></li>
                                                    <li><a href="listing-grid-4-items.html">4 Items in Row</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="has-child"><a href="index.html#nav-row-listing">Row Listing</a>
                                            <div id="nav-row-listing" class="nav-wrapper">
                                                <ul>
                                                    <li><a href="listing-row-right-sidebar.html">Row Right Sidebar</a></li>
                                                    <li><a href="listing-row-left-sidebar.html">Row Left Sidebar</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!--<li><a href="blog.html">Blog</a></li>-->
                        <li><a href="contact.html">Kontakt</a></li>
                    </ul>
                    <!--end navigation-->
                </div>
                <!--end primary-nav-->
				<!--
                <div class="secondary-nav">
                    <a href="index.html#" data-modal-external-file="modal_sign_in" data-target="modal-sign-in">Logowanie</a>
                    <a href="index.html#" class="promoted" data-modal-external-file="modal_register" data-target="modal-register">Rejestracja</a>
                </div>
				-->
                <!--end secondary-nav-->
                <a href="submit.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-plus"></i><span>Dodaj</span></a>
                <div class="nav-btn">
                    <i></i>
                    <i></i>
                    <i></i>
                </div>
                <!--end nav-btn-->
            </div>
            <!--end right-->
        </nav>
        <!--end nav-->
    </header>
    <!--end page-header-->
	
	
	

    <div id="page-content">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="detail.html#">Home</a></li>
                <li><a href="detail.html#">Pages</a></li>
                <li class="active">Contact</li>
            </ol>
            <section class="page-title pull-left">
                <h1>'. $data_a['title'] .'</h1>
                <h3>'. $data_a['location'] .'</h3>
                <div class="rating-passive" data-rating="'. $data_a['rating'] .'">
                    <span class="stars"></span>
                    <span class="reviews">'.$data_a['reviews_number'].'</span>
                </div>
            </section>
            <!--end page-title-->
            <a href="detail.html#write-a-review" class="btn btn-primary btn-framed btn-rounded btn-light-frame icon scroll pull-right"><i class="fa fa-star"></i>Dodaj komentarz</a>
        </div>
        <!--end container-->
        <section>
            <div class="gallery detail">
                <div class="owl-carousel" data-owl-items="3" data-owl-loop="1" data-owl-auto-width="1" data-owl-nav="1" data-owl-dots="0" data-owl-margin="2" data-owl-nav-container="#gallery-nav">
                    <div class="image">
		    	<div class="bg-transfer"><img src="'. $data_a['sidebar_image'] .'" alt=""></div>
		    </div>
		    <div class="image">
                        <div class="bg-transfer"><img src="assets/img/items/1.jpg" alt=""></div>
                    </div>
                    <div class="image">
                        <div class="bg-transfer"><img src="assets/img/items/30.jpg" alt=""></div>
                    </div>
                    <div class="image">
                        <div class="bg-transfer"><img src="assets/img/items/31.jpg" alt=""></div>
                    </div>
                    <div class="image">
                        <div class="bg-transfer"><img src="assets/img/items/21.jpg" alt=""></div>
                    </div>
                    <div class="image">
                        <div class="bg-transfer"><img src="assets/img/items/23.jpg" alt=""></div>
                    </div>
                </div>
                <!--end owl-carousel-->
            </div>
            <!--end gallery-->
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <div id="gallery-nav"></div>
                    <section>
                        <h2>O obiekcie</h2>
			<p>
				'.$data_a['description'].'
			</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur tristique enim, ac tincidunt
                            massa pulvinar non. Donec scelerisque libero eu tincidunt cursus. Phasellus vel commodo nunc, nec suscipit
                            enim. Integer suscipit, mauris consectetur pharetra ultrices, neque sem malesuada mauris, id tristique
                            ante leo vel magna. Phasellus ac risus vel erat elementum fringilla et non massa. Pellentesque habitant
                            morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                        </p>
                        <p>
                            In ut varius magna. Integer ullamcorper tincidunt molestie. Morbi consequat sem non nulla laoreet,
                            non commodo tellus elementum. Sed tincidunt, lorem vitae rhoncus pharetra, diam ex pharetra erat, eu
                            lacinia mi libero vitae lectus. Nullam cursus bibendum magna ut elementum. Fusce eget mauris in erat
                            gravida pretium sed eget massa.
                            gravida pretium sed eget massa.
                        </p>
                    </section>
                    <section>
                        <h2>Cechy</h2>
                        <ul class="tags">
                            <li>Wi-Fi</li>
                            <li>Parking</li>
                            <li>TV</li>
                            <li>Alcohol</li>
                            <li>Vegetarian</li>
                            <li>Take-out</li>
                            <li>Balcony</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Opinie</h2>
                        <div class="review">
                            <div class="image">
                                <div class="bg-transfer"><img src="assets/img/person-02.jpg" alt=""></div>
                            </div>
                            <div class="description">
                                <figure>
                                    <div class="rating-passive" data-rating="4">
                                        <span class="stars"></span>
                                        <span class="reviews">6</span>
                                    </div>
                                    <span class="date">09.05.2016</span>
                                </figure>
                                <p>Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis</p>
                            </div>
                        </div>
                        <!--end review-->
                        <div class="review">
                            <div class="image">
                                <div class="bg-transfer"><img src="assets/img/person-01.jpg" alt=""></div>
                            </div>
                            <div class="description">
                                <figure>
                                    <div class="rating-passive" data-rating="5">
                                        <span class="stars"></span>
                                        <span class="reviews">6</span>
                                    </div>
                                    <span class="date">09.05.2016</span>
                                </figure>
                                <p>Vestibulum vel est massa. Integer pellentesque non augue et accumsan. Maecenas molestie elit nibh,
                                    vel vestibulum leo condimentum quis. Duis ac orci a magna auctor vehicula.
                                </p>
                            </div>
                        </div>
                        <!--end review-->
                    </section>
                    <section id="write-a-review">
                        <h2>Napisz komentarz</h2>
                        <form class="clearfix form inputs-underline">
                            <div class="box">
                                <div class="comment">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="comment-title">
                                                <h4>Oceń swoje doświadczenia</h4>
                                            </div>
                                            <!--end title-->
                                            <div class="form-group">
                                                <label for="name">Temat opinii<em>*</em></label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Świetne miejsce!" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="message">Twoja wiadomość<em>*</em></label>
                                                <textarea class="form-control" id="message" rows="8" name="message" required="" placeholder="Opisz swoje doświadczenia"></textarea>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-8-->
                                        <div class="col-md-4">
                                            <div class="comment-title">
                                                <h4>Rating</h4>
                                            </div>
                                            <!--end title-->
                                            <dl class="visitor-rating">
                                                <dt>Jakość</dt>
                                                <dd class="star-rating active" data-name="comfort"></dd>
                                                <dt>Lokalizacja</dt>
                                                <dd class="star-rating active" data-name="location"></dd>
                                                <dt>Udogodnienia</dt>
                                                <dd class="star-rating active" data-name="facilities"></dd>
                                                <dt>Obsługa</dt>
                                                <dd class="star-rating active" data-name="staff"></dd>
                                                <dt>Koszt</dt>
                                                <dd class="star-rating active" data-name="value"></dd>
                                            </dl>
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-rounded">Wyślij</button>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <!--end comment-->
                            </div>
                            <!--end review-->
                        </form>
                        <!--end form-->
                    </section>
                </div>
                <!--end col-md-7-->
                <div class="col-md-5 col-sm-5">
                    <div class="detail-sidebar">
                        <section class="shadow">
                            <div class="map height-250px" id="map-detail"></div>
                            <!--end map-->
                            <div class="content">
                                <div class="vertical-aligned-elements">
                                    <div class="element"><img src="assets/img/logo-2.png" alt=""></div>
                                    <div class="element text-align-right"><a href="detail.html#" class="btn btn-primary btn-rounded btn-xs">Claim</a></div>
                                </div>
                                <hr>
                                <address>
                                    <figure><i class="fa fa-map-marker"></i>'. $data_a['location'] .' </figure>
                                    <figure><i class="fa fa-envelope"></i><a href="detail.html#">'.$data_a['email'].'</a></figure>
                                    <figure><i class="fa fa-phone"></i>'. $data_a['phone'] .'</figure>
                                    <figure><i class="fa fa-globe"></i><a href="detail.html#">'.$data_a['website'].'</a></figure>
                                </address>
                            </div>
                        </section>
                        <section>
                            <h2>Godziny otwarcia</h2>
                            <dl>
                                <dt>Monday</dt>
                                <dd>08:00am - 11:00pm</dd>
                                <dt>Tuesday</dt>
                                <dd>08:00am - 11:00pm</dd>
                                <dt>Wednesday</dt>
                                <dd>12:00am - 11:00pm</dd>
                                <dt>Thursday</dt>
                                <dd>08:00am - 11:00pm</dd>
                                <dt>Friday</dt>
                                <dd>03:00pm - 02:00am</dd>
                                <dt>Saturday</dt>
                                <dd>03:00pm - 02:00am</dd>
                                <dt>Sunday</dt>
                                <dd>Closed</dd>
                            </dl>
                        </section>
                        <section>
                            <h2>Udostępnij</h2>
                            <div class="social-share"></div>
                        </section>
                    </div>
                    <!--end detail-sidebar-->
                </div>
                <!--end col-md-5-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </div>
    <!--end page-content-->

    <footer id="page-footer">
        <div class="footer-wrapper">
            <div class="block">
                <div class="container">
                    <div class="vertical-aligned-elements">
                        <div class="element width-50">
                            <p data-toggle="modal" data-target="#myModal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam at neque sit amet vestibulum. <a href="detail.html#">Terms of Use</a> and <a href="detail.html#">Privacy Policy</a>.</p>
                        </div>
                        <div class="element width-50 text-align-right">
                            <a href="detail.html#" class="circle-icon"><i class="social_twitter"></i></a>
                            <a href="detail.html#" class="circle-icon"><i class="social_facebook"></i></a>
                            <a href="detail.html#" class="circle-icon"><i class="social_youtube"></i></a>
                        </div>
                    </div>
                    <div class="background-wrapper">
                        <div class="bg-transfer opacity-50">
                            <img src="assets/img/footer-bg.png" alt="">
                        </div>
                    </div>
                    <!--end background-wrapper-->
                </div>
            </div>
            <div class="footer-navigation">
                <div class="container">
                    <div class="vertical-aligned-elements">
                        <div class="element width-50">(C) 2017 MapsPL, All right reserved</div>
                        <div class="element width-50 text-align-right">
                            <a href="index.html">Home</a>
                            <a href="listing-grid-right-sidebar.html">Listy</a>
                            <a href="submit.html">Dodaj obiekt</a>
                            <a href="contact.html">Kontakt</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--end page-footer-->
</div>
<!--end page-wrapper-->
<a href="detail.html#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>
<a href="detail.html#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<!--
<div class="message-popup bottom-left" data-show-after-time="2000" data-close-after-time="5000">
    <div class="close"><i class="fa fa-times"></i></div>
    <p>15 people are watching this...</p>
</div>

<div class="message-popup bottom-left featured" data-show-after-time="4000" data-close-after-time="5000">
    <div class="close"><i class="fa fa-times"></i></div>
    <div class="title">Just Booked!</div>
    <p>Hurry up!!</p>
</div>

<div class="message-popup bottom-left" data-show-after-time="5000" data-close-after-time="5000">
    <div class="close"><i class="fa fa-times"></i></div>
    <p>Last booking was from <strong>France</strong></p>
</div>
-->

<script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="http:\/\/maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
<script type="text/javascript" src="assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/maps.js"></script>

<script>
    rating(".visitor-rating");
    var _latitude = '.$data_a['latitude'].';
    var _longitude = '.$data_a['longitude'].';
    var element = "map-detail";
    simpleMap(_latitude,_longitude, element);
</script>

<script>
  (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,"script","https://www.google-analytics.com/analytics.js","ga");

  ga("create", "UA-52524246-7", "auto");
  ga("send", "pageview");

</script>

</body>
';
}
}
?>
