var appElements = {
	streetViewInit : {}
};

var myMapsPL = { 
        testReady: function(){ 
        	console.log("ready ok");
        },
	checkClick: function(){
		$('.promo-item').click(function() {
  			console.log("clicked");
		});
	},
	itemPostReq: function() {
		var url = window.location.href;
		var to = url.lastIndexOf('/');
		to = to == -1 ? url.length : to + 1;
		url = url.substring(0, to);
		var url = "/detail.php";
		var id = $(".promo-item").data("id"); 
		$.post(url,{id:id},        
			function(data) {
			//console.log(data);
			//response is ok (gives page code)
		});
	},
	itemGetReq: function() {
	  $('.promo-item>a>.image').click(function() {
		var url = window.location.href;
		var to = url.lastIndexOf('/');
		to = to == -1 ? url.length : to + 1;
		url = url.substring(0, to);	
		var url = url + "/detail.php?id=";
		var id = ($(this).parent().parent()).data("id"); 
		window.location.href = url+id;
	  });
	},
	animateSearchArrow: function() {
	  $('.search-form .section-title').click(function(){
		$('.arrows').toggleClass("arrow-up arrow-down");
	  });
	},
	//below - not used now
	streetViewInit : false,
	panorama : null,
	initializeStreetView: function(x=37.869260,y=-122.254811) {
            myMapsPL.streetViewInit = true;
	    myMapsPL.panorama = new google.maps.StreetViewPanorama(
              document.getElementById('street-view'),
              {
                position: {lat: x, lng: y},
                pov: {heading: 165, pitch: 0},
                zoom: 1
              }
	    );
	    return myMapsPL.panorama, myMapsPL.streetViewInit;
	},
	updateStreetView: function(x=52.2199932,y=21.0108979) {
	    myMapsPL.panorama.setPosition({lat: x, lng: y});
            myMapsPL.panorama.setPov( ({
                heading: 330,
                pitch: 0
            }));
	},
	searchFieldOptions: function() {
		$("select#category").on("change", function(){
	//$("#subcattr").show();
	$("#subcategory option").hide();
	$("#subcategory").val("");
	  	if ($("#category :selected").val() == "1"){
	 			$("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'1\']").show();
				$("#subcategory option[value=\'2\']").show();
				$("#subcategory option[value=\'3\']").show();
				$("#subcategory option[value=\'4\']").show();
		}
		if ($("#category :selected").val() == "2"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'5\']").show();
				$("#subcategory option[value=\'6\']").show();
                $("#subcategory option[value=\'7\']").show();
                $("#subcategory option[value=\'8\']").show();
        }
		if ($("#category :selected").val() == "3"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'9\']").show();
                $("#subcategory option[value=\'10\']").show();
                $("#subcategory option[value=\'11\']").show();
                $("#subcategory option[value=\'12\']").show();
                $("#subcategory option[value=\'13\']").show();
        }
		if ($("#category :selected").val() == "4"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'14\']").show();
        }
    	if ($("#category :selected").val() == "5"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'15\']").show();
                $("#subcategory option[value=\'16\']").show();
                $("#subcategory option[value=\'17\']").show();
                $("#subcategory option[value=\'18\']").show();
                $("#subcategory option[value=\'19\']").show();
                $("#subcategory option[value=\'20\']").show();
        }
    	if ($("#category :selected").val() == "6"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'21\']").show();
                $("#subcategory option[value=\'22\']").show();
                $("#subcategory option[value=\'23\']").show();
                $("#subcategory option[value=\'24\']").show();
                $("#subcategory option[value=\'25\']").show();
                $("#subcategory option[value=\'26\']").show();
        }
    	if ($("#category :selected").val() == "7"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'27\']").show();
                $("#subcategory option[value=\'28\']").show();
                $("#subcategory option[value=\'29\']").show();
                $("#subcategory option[value=\'30\']").show();
                $("#subcategory option[value=\'31\']").show();
        }
		if ($("#category :selected").val() == "8"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'32\']").show();
                $("#subcategory option[value=\'33\']").show();
                $("#subcategory option[value=\'34\']").show();
                $("#subcategory option[value=\'35\']").show();
        }
		if ($("#category :selected").val() == "9"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'36\']").show();
                $("#subcategory option[value=\'37\']").show();
                $("#subcategory option[value=\'38\']").show();
                $("#subcategory option[value=\'39\']").show();
        }
		if ($("#category :selected").val() == "10"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'40\']").show();
                $("#subcategory option[value=\'41\']").show();
                $("#subcategory option[value=\'42\']").show();
                $("#subcategory option[value=\'43\']").show();
                $("#subcategory option[value=\'44\']").show();
                $("#subcategory option[value=\'45\']").show();
        }
		if ($("#category :selected").val() == "11"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'46\']").show();
                $("#subcategory option[value=\'47\']").show();
                $("#subcategory option[value=\'48\']").show();
                $("#subcategory option[value=\'49\']").show();
                $("#subcategory option[value=\'50\']").show();
        }
		if ($("#category :selected").val() == "12"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'51\']").show();
                $("#subcategory option[value=\'52\']").show();
                $("#subcategory option[value=\'53\']").show();
                $("#subcategory option[value=\'54\']").show();
        }
		if ($("#category :selected").val() == "13"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'55\']").show();
                $("#subcategory option[value=\'56\']").show();
                $("#subcategory option[value=\'57\']").show();
                $("#subcategory option[value=\'58\']").show();
                $("#subcategory option[value=\'59\']").show();
        }
		});
	},
	searchFieldOptions2: function() { // ???
		$("select#country").on("change", function(){
			$("#city option").hide();
			$("#city").val("");
			$("#city option[data-country='"+$("select#country").val()+"']").show();
		});
	},
	indexSearchBtn: function() {
		$('#index_search_btn').on("click", function(e){
			e.preventDefault();
			console.log("search");
		});
	}
};

$(function() {
	//myMapsPL.testReady();
	//myMapsPL.checkClick();
	myMapsPL.itemGetReq();
	myMapsPL.animateSearchArrow();
	myMapsPL.searchFieldOptions();
	myMapsPL.searchFieldOptions2();
	myMapsPL.indexSearchBtn();
});

