<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

// test query
$query = "SELECT * FROM view1;";

$sth = $DBcon->prepare($query);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($result);
echo json_encode($result);


?>

