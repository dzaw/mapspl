<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query = "SELECT DISTINCT city_pl, cntry_pl FROM view1 ORDER BY cntry_pl";

$sth = $DBcon->prepare($query);
$sth->execute();


while ($result = $sth->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_COLUMN,1)) {
//var_dump($result);

	foreach ($result as $key => $val){
		//echo $val[0]; // echoes cities in all countries on pos 0
        	//echo $result['inne'][0]; // echoes just city in one cntry on pos 0
		//echo $key; // echoes cntries
		echo '<li class="has-child"><a href="index.html#nav-locations-' . $key . '">' . $key . '</a>
        		<div class="nav-wrapper" id="nav-locations-' . $key . '">
                	<ul>';
		for ($i=0;$i<count($val);$i++) {
                	echo '<li><a href="index.html#nav-' . $val[$i] . '">' . $val[$i] . '</a></li>';
		}
		echo '</ul>
        		</div>
</li>
';
	}
}

?>
