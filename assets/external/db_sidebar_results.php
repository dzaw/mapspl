<?php
// fetching all data to sidebar
//$json=file_get_contents("http://dagmarazawada.pl/mapsPL/assets/external/db_data.php");
//$data =  json_decode($json, true);

// fetching data from post request

$arr = array();
foreach ($_POST as $value => $key) {
        array_push($arr, $_POST[$value]);
}

$list = array();
foreach ($arr as $key => $val) {
        for ($i=0; $i<count($val); $i++){
                $list[] = $val[$i];
        }
}

$search_val =  "( " . implode(", ", $list) . " ) ";
//echo $search_val;

if (empty($list)) {
	echo 'Move map around or zoom out to get some results here!';
	die();
}

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query_join = "SELECT * FROM view1 WHERE id IN " . $search_val . ";";

$sth = $DBcon->prepare($query_join);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);

$json = json_encode($result);
$data = json_decode($json, true);


foreach ($data as $data_poi => $data_a) {
	echo '<div class="result-item" data-id="'.$data_a['id'].'">';
		echo '<a href=""><h3>'.$data_a['title'].'</h3>';
			echo '<div class="result-item-detail">';
				if ($data_a['sidebar_image'] != null || $data_a['sidebar_image'] != '') {
					echo '<div class="image" style="background-image: url('.$data_a['sidebar_image'].')"></div>
					<div class="description"><h5>';
				}
				else {
					echo '<div class="description" style="padding-left: 0;"><h5>';	
				}
				echo '
					<i class="fa fa-map-marker"></i>'.$data_a['location'].'</h5>
	            <div class="label label-default" style="background-color:#be0c0c; color:#fff;">'.$data_a['subcat_pl'].'</div></div>
			</div>
		</a>
		<div class="controls-more">
			<ul>
				<li><a href="#">Dodaj do ulubionych</a></li>
                <li><a href="#">Byłem, widziałem - potwierdź</a></li>
				<li><a href="#">Zgłoś, że miejsce nie istnieje</a></li>
				<li><a href="#">Edytuj</a></li>
				<li><a href="#">Dodaj zdjęcie</a></li>
				<li><a target="_blank" href="';
			echo 'http://maps.google.com?saddr=Current+Location&daddr='.$data_a['latitude'].','.$data_a['longitude'].'"';
			echo '">Wyznacz trasę</a></li>
			</ul>
		</div>
	</div>';
}
?>



