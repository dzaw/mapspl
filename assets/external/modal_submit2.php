<div class="modal-dialog width-800px" role="document" data-latitude="40.7344458" data-longitude="-73.86704922" data-marker-drag="true">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>Dodaj Wpis</h2>
            </div>
        </div>
        <div class="modal-body">
            <form class="form inputs-underline">
                <section>
                    <h3>Opis</h3>
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <div class="form-group">
                                <label for="title">Tytuł</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Dodaj nazwę">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-9-->
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="category">Kategoria</label>
                                <select class="form-control selectpicker" name="category" id="category">
                                    <option value="">Kategoria</option>
                                    <option value="1">Restauracja</option>
                                    <option value="2">Wydarzenie</option>
                                    <option value="3">Adrenalina</option>
                                    <option value="4">Sport</option>
                                    <option value="5">Wellness</option>
                                </select>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--col-md-3-->
                    </div>
                    <!--end row-->
                    <div class="form-group">
                        <label for="description">Opis</label>
                        <textarea class="form-control" id="description" rows="4" name="description" placeholder="Dodaj opis"></textarea>
                    </div>
                    <!--end form-group-->
                    <div class="form-group">
                        <label for="tags">Tagi</label>
                        <input type="text" class="form-control" name="tags" id="tags" placeholder="+ Dodaj tag">
                    </div>
                    <!--end form-group-->
                </section>
                <section>
                    <h3>Kontakt</h3>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="address-autocomplete">Adres</label>
                                <input type="text" class="form-control" name="address" id="address-autocomplete" placeholder="Adres">
                            </div>
                            <!--end form-group-->
                            <div class="map height-200px shadow" id="map-modal"></div>
                            <!--end map-->
                            <div class="form-group hidden">
                                <input type="text" class="form-control" id="latitude" name="latitude" hidden="">
                                <input type="text" class="form-control" id="longitude" name="longitude" hidden="">
                            </div>
                            <p class="note">Wpisz dokładny adres lub przesuń marker</p>
                        </div>
                        <!--end col-md-6-->
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="region">Region</label>
                                <select class="form-control selectpicker" name="region" id="region">
                                    <option value="">Wybierz Region</option>
                                    <option value="1">New York</option>
                                    <option value="2">Washington</option>
                                    <option value="3">London</option>
                                    <option value="4">Paris</option>
                                </select>
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="phone">Telefon</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Numer telefonu">
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="mail@example.com">
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="website">Strona www</label>
                                <input type="text" class="form-control" name="website" id="website" placeholder="http://">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                    </div>
                </section>
                <section>
                    <h3>Galeria</h3>
                    <div class="file-upload-previews"></div>
                    <div class="file-upload">
                        <input type="file" name="files[]" class="file-upload-input with-preview" multiple title="Kliknij żeby dodać obrazek" maxlength="10" accept="gif|jpg|png">
                        <span>Kliknij lub przeciągnij obrazki tutaj</span>
                    </div>
                    <div class="form-group">
                        <label for="video">Video URL</label>
                        <input type="text" class="form-control" name="video" id="video" placeholder="http://">
                    </div>
                    <!--end form-group-->
                </section>
                <section>
                    <h3>Social media</h3>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="facebook">Facebook URL</label>
                                <input type="text" class="form-control" name="facebook" id="facebook" placeholder="http://">
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="youtube">Youtube URL</label>
                                <input type="text" class="form-control" name="youtube" id="youtube" placeholder="http://">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="twitter">Twitter URL</label>
                                <input type="text" class="form-control" name="twitter" id="twitter" placeholder="http://">
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="pinterest">Pinterest URL</label>
                                <input type="text" class="form-control" name="pinterest" id="pinterest" placeholder="http://">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--end row-->
                </section>
                <section>
                    <h3>Godziny otwarcia<span class="note">(opcja)</span></h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="accordion-heading-1">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#accordion-collapse-1" aria-expanded="false" aria-controls="accordion-collapse-1">
                                        <i class="fa fa-clock-o"></i>Dodaj godziny otwarcia
                                    </a>
                                </h4>
                            </div>
                            <!--end panel-heading-->
                            <div id="accordion-collapse-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-heading-1">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Poniedziałek</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Wtorek</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Środa</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Czwartek</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Piątek</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Sobota</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 horizontal-input-title">
                                            <strong>Niedziela</strong>
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="open_hours[]" placeholder="Otwarcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="close_hours[]" placeholder="Zamknięcie">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                </div>
                            </div>
                            <!--end panel-collapse-->
                        </div>
                        <!--end panel-->
                    </div>
                    <!--end panel-group-->
                </section>
                <section>
                    <h3>Menu Restauracji<span class="note">(opcja)</span></h3>
                    <div class="panel-group" id="accordion-2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="accordion-heading-2">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-2" href="#accordion-collapse-2" aria-expanded="false" aria-controls="accordion-collapse-2">
                                        <i class="fa fa-cutlery"></i>Dodaj menu
                                    </a>
                                </h4>
                            </div>
                            <!--end panel-heading-->
                            <div id="accordion-collapse-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-heading-2">
                                <div class="panel-body">
                                    <div class="wrapper">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3"><strong>Tytuł </strong><span class="note">(opcja)</span></div>
                                            <div class="col-md-6 col-sm-6"><strong>Opis</strong></div>
                                            <div class="col-md-3 col-sm-3"><strong>Typy dań</strong></div>
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_title[]" placeholder="Nazwa">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_description[]" placeholder="Opis">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-6-->
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select class="form-control selectpicker" name="menu_type[]">
                                                        <option value="">Wybierz rodzaj dań</option>
                                                        <option value="1">Starter</option>
                                                        <option value="2">Zupa</option>
                                                        <option value="3">Główne</option>
                                                        <option value="4">Deser</option>
                                                    </select>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_title[]" placeholder="Nazwa">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_description" placeholder="Opis">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-6-->
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select class="form-control selectpicker" name="menu_type[]">
                                                        <option value="">Wybierz rodzaj dań</option>
                                                        <option value="1">Starter</option>
                                                        <option value="2">Zupa</option>
                                                        <option value="3">Główne</option>
                                                        <option value="4">Deser</option>
                                                    </select>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                        </div>
                                        <!--end row-->
                                        <div class="row" id="duplicate-meal">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_title[]" placeholder="Nazwa">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="menu_description" placeholder="Opis">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-6-->
                                            <div class="col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <select class="form-control selectpicker" name="menu_type[]">
                                                        <option value="">Wybierz rodzaj dań</option>
                                                        <option value="1">Starter</option>
                                                        <option value="2">Zupa</option>
                                                        <option value="3">Główne</option>
                                                        <option value="4">Deser</option>
                                                    </select>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-sm-3-->
                                        </div>
                                        <!--end row-->
                                    </div>
                                    <!--end wrapper-->
                                    <div class="center"><a href="#duplicate-meal" class="btn btn-rounded btn-primary btn-framed btn-light-frame btn-xs icon duplicate"><i class="fa fa-plus"></i>Dodaj kolejne danie</a></div>
                                </div>
                                <!--end panel-body-->
                            </div>
                            <!--end panel-collapse-->
                        </div>
                        <!--end panel-->
                    </div>
                    <!--end panel-group-->
                </section>
                <section>
                    <h3>Terminarz<span class="note">(opcja)</span></h3>
                    <div class="panel-group" id="accordion-3" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="accordion-heading-3">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-3" href="#accordion-collapse-3" aria-expanded="false" aria-controls="accordion-collapse-3">
                                        <i class="fa fa-calendar"></i>Dodaj terminarz
                                    </a>
                                </h4>
                            </div>
                            <!--end panel-heading-->
                            <div id="accordion-collapse-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-heading-3">
                                <div class="panel-body">
                                    <div class="wrapper">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-3">
                                                <strong>Data</strong>
                                            </div>
                                            <!--end col-md-2-->
                                            <div class="col-md-2 col-sm-3">
                                                <strong>Godzina</strong>
                                            </div>
                                            <!--end col-md-2-->
                                            <div class="col-md-4 col-sm-3">
                                                <strong>Miejsce</strong>
                                            </div>
                                            <!--end col-md-3-->
                                            <div class="col-md-4 col-sm-3">
                                                <strong>Adres</strong>
                                            </div>
                                            <!--end col-md-4-->
                                        </div>
                                        <div class="row" id="duplicate-schedule">
                                            <div class="col-md-2 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="schedule_date[]" placeholder="Data">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-md-2-->
                                            <div class="col-md-2 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="schedule_time[]" placeholder="Godzina">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-md-2-->
                                            <div class="col-md-4 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="schedule_place[]" placeholder="Miejsce">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-md-3-->
                                            <div class="col-md-4 col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="schedule_address[]" placeholder="Adres">
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-md-4-->
                                        </div>
                                        <!--end row-->
                                    </div>
                                    <!--end wrapper-->
                                    <div class="center"><a href="#duplicate-schedule" class="btn btn-rounded btn-primary btn-framed btn-light-frame btn-xs icon duplicate"><i class="fa fa-plus"></i>Dodaj kolejny wpis w terminarzu</a></div>
                                </div>
                                <!--end panel-body-->
                            </div>
                            <!--end panel-collapse-->
                        </div>
                        <!--end panel-->
                    </div>
                    <!--end panel-group-->
                </section>
                <hr>
                <section class="center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-rounded">Podgląd & Zapis</button>
                    </div>
                    <!--end form-group-->
                </section>
            </form>
            <!--end form-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->