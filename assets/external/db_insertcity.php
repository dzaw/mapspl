<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$cityname = htmlspecialchars($_POST['cityname']);
$countryid = htmlspecialchars($_POST['countryid']);

$query_insert = 'INSERT INTO Cities (CityName, CountryID) VALUES ("' . $cityname . '", "' . $countryid . '");';
$query2 = "SELECT * FROM Cities ORDER BY IDCity DESC LIMIT 1";

try{
	$sth = $DBcon->prepare($query_insert);
	$sth->execute();
	$sth2 = $DBcon->prepare($query2);
	$sth2->execute();
	$addedID = $sth2->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    die($e->getMessage());
}



if ($e) {
	echo json_encode(array("value1"=>"ERROR.. miasto nie dodane","value2"=>"0"));
	}
else {
	echo json_encode(array("value1"=>"OK - miasto dodane do bazy","value2"=>$addedID['IDCity']));
}

?>
