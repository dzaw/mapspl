<div class="result-item" data-id="3"><a href="detail.html"><h3>Centauro di Mitoraj</h3><div class="result-item-detail"><div class="image" style="background-image: url(assets/img/items/33.jpg)"></div><div class="description"><h5><i class="fa fa-map-marker"></i>Via Agostino Depretis, 9-25, 20142 Milano</h5><div class="rating-passive"data-rating="3">
                                            <span class="stars"></span>
                                            <span class="reviews">12</span>
                                        </div><div class="label label-default">Sztuka</div><p>Aliquam vel turpis sagittis, semper tellus eget, aliquam turpis. Cras aliquam, arcu</p></div>
                        </div>
                    </a>
                    <div class="controls-more">
                        <ul>
                            <li><a href="#" class="add-to-favorites">Dodaj do ulubionych</a></li>
                            <li><a href="#" class="add-to-watchlist">Dodaj do obserwowanych</a></li>
                        </ul>
                    </div>
                </div>

                <div class="result-item" data-id="1"><a href=""><h3>Ristorante Polacco Bajka</h3><div class="result-item-detail"><div class="image" style="background-image: url(assets/img/items/34.png)"></div><div class="description"><h5><i class="fa fa-map-marker"></i>Largo Andrea Barbazza 32/33, 00168 Rzym</h5><div class="rating-passive"data-rating="5">
                                            <span class="stars"></span>
                                            <span class="reviews">6</span>
                                        </div><div class="label label-default">Restauracja</div><p>Mauris porttitor justo ac pulvinar bibendum. Ut ex orci, hendrerit ut lectus ac, dapibus cursus neque. Pellentesque eleifend quam nulla, ut blandit purus rutrum eget. Maecenas orci dolor, efficitur porta pharetra eu, cursus at ex. Donec mattis ipsum id dui rutrum malesuada</p></div>
                        </div>
                    </a>
                    <div class="controls-more">
                        <ul>
                            <li><a href="#" class="add-to-favorites">Dodaj do ulubionych</a></li>
                            <li><a href="#" class="add-to-watchlist">Dodaj do obserwowanych</a></li>
                        </ul>
                    </div>
                </div>