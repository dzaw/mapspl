[  
   {  
      "id":1,
      "latitude":41.920136,
      "longitude":12.411698,
      "featured":1,
      "title":"Ristorante Polacco Bajka",
      "location":"Largo Andrea Barbazza 32/33, 00168 Rzym",
      "city":1,
      "phone":"+39 328 803 0162",
      "email":"hello@markys.com",
      "website":"http:\/\/bajka.it",
      "category":"Restaurant",
      "rating":"4",
      "reviews_number":"6",
      "marker_image":"assets\/img\/items\/34.png",
      "gallery":[  
         "assets\/img\/items\/34.png",
         "assets\/img\/items\/35.jpg"
      ],
      "tags":[  
         "Wi-Fi",
         "Parking",
         "Kuchnia",
         "Vegetarian"
      ],
      "additional_info":"Average price $30",
      "url":"detail.html",
      "description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis, arcu non hendrerit imperdiet, metus odio scelerisque elit, sed lacinia odio est ac felis. Nam ullamcorper hendrerit ullamcorper. Praesent quis arcu quis leo posuere ornare eu in purus. Nulla ornare rutrum condimentum. Praesent eu pulvinar velit. Quisque non finibus purus, eu auctor ipsum.",
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ],
      "opening_hours":[  
         "08:00am - 11:00pm",
         "08:00am - 11:00pm",
         "12:00am - 11:00pm",
         "08:00am - 11:00pm",
         "03:00pm - 02:00am",
         "03:00pm - 02:00am",
         "Closed"
      ]
   },
   {  
      "id":2,
      "latitude":41.494079,
      "longitude":13.805688,
      "featured":0,
      "title":"Monte Cassino",
      "location":"Cimitero Militare Polacco di Monte Cassino",
      "city":1,
      "contact":"<\/i>925-585-2459",
      "category":"Historia",
      "rating":"5",
      "reviews_number":"10",
      "marker_image":"assets\/img\/items\/32.jpg",
      "gallery":[  
         "assets\/img\/items\/32.jpg",
         "assets\/img\/items\/9.jpg",
         "assets\/img\/items\/11.jpg"
      ],
      "additional_info":"",
      "url":"",
      "description":"Duis nec lobortis massa. Mauris tempus vitae augue eu tempus",
      "schedule":[  
         {  
            "date":"03.04.2017",
            "time":"18:00",
            "location_title":"Msza",
            "location_address":"Opactwo Benedyktynów"
         }
      ],
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ]
   },
   {  
      "id":3,
      "latitude":45.430453,
      "longitude":9.153661,
      "featured":0,
      "title":"Centauro di Mitoraj",
      "location":"Via Agostino Depretis, 9-25, 20142 Milano",
      "city":1,
      "contact":"<\/i>925-585-2459",
      "category":"Sztuka",
      "rating":"5",
      "reviews_number":"10",
      "marker_image":"assets\/img\/items\/33.jpg",
      "gallery":[  
         "assets\/img\/items\/33.jpg",
         "assets\/img\/items\/9.jpg",
         "assets\/img\/items\/11.jpg"
      ],
      "additional_info":"",
      "url":"",
      "description":"Duis nec lobortis massa. Mauris tempus vitae augue eu tempus",
      "schedule":[  
         {  
            "date":"03.04.2017",
            "time":"18:00",
            "location_title":"Odrestaurowanie",
            "location_address":"..."
         }
      ],
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ]
   },
   {  
      "id":4,
      "latitude":41.909734,
      "longitude":12.493389,
      "featured":1,
      "title":"Związek Polaków we Włoszech",
      "location":"Via Piemonte 117, I 00187 Roma",
      "city":1,
      "contact":"<\/i>http:\/\/polonia-wloska.org\/kontakt",
      "category":"Organizacje polonijne",
      "rating":"5",
      "reviews_number":"10",
      "marker_image":"assets\/img\/items\/36.png",
      "gallery":[  
         "assets\/img\/items\/36.png",
         "assets\/img\/items\/9.jpg",
         "assets\/img\/items\/11.jpg"
      ],
      "additional_info":"",
      "url":"",
      "description":"Duis nec lobortis massa. Mauris tempus vitae augue eu tempus",
      "schedule":[  
         {  
            "date":"03.04.2017",
            "time":"18:00",
            "location_title":"Spotkanie polonii",
            "location_address":"..."
         }
      ],
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ]
   },
   {  
      "id":5,
      "latitude":38.054787,
      "longitude":15.678100,
      "featured":1,
      "title":"Zwiazek Polakow w Kalabrii",
      "location":"Via Trapezzoli Sud, 97 - 89134 Reggio Calabria",
      "city":1,
      "phone":"0965-624466",
      "email":"assopolca@yahoo.it",
      "website":"http:\/\/www.associazionepolacchiincalabria.it",
      "category":"Restaurant","category":"Organizacje polonijne",
      "rating":"5",
      "reviews_number":"10",
      "marker_image":"assets\/img\/items\/37.gif",
      "gallery":[  
         "assets\/img\/items\/37.gif",
         "assets\/img\/items\/9.jpg",
         "assets\/img\/items\/11.jpg"
      ],
      "additional_info":"",
      "url":"",
      "description":"Duis nec lobortis massa. Mauris tempus vitae augue eu tempus",
      "schedule":[  
         {  
            "date":"03.04.2017",
            "time":"18:00",
            "location_title":"Spotkanie polonii",
            "location_address":"..."
         }
      ],
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ]
   },
   {  
      "id":6,
      "latitude":45.414438,
      "longitude":11.916485,
      "featured":1,
      "title":"Scuola di Lingua e Cultura Polacca",
      "location":"Via Giovanni Giolitti, 2, 35129 Padova",
      "city":3,
      "phone":"+39 328 803 0162",
      "email":"aipp.szkolapolska@gmail.com",
      "website":"http:\/\/www.polskaszkola-padwa.pl",
      "category":"Szkoła",
      "rating":"5",
      "reviews_number":"6",
      "marker_image":"assets\/img\/items\/38.jpg",
      "gallery":[  
         "assets\/img\/items\/38.jpg",
         "assets\/img\/items\/35.jpg"
      ],
      "tags":[  
         "Wi-Fi",
         "Parking",
         "Szkoła",
         "Język polski"
      ],
      "additional_info":"Average price $30",
      "url":"detail.html",
      "description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis, arcu non hendrerit imperdiet, metus odio scelerisque elit, sed lacinia odio est ac felis. Nam ullamcorper hendrerit ullamcorper. Praesent quis arcu quis leo posuere ornare eu in purus. Nulla ornare rutrum condimentum. Praesent eu pulvinar velit. Quisque non finibus purus, eu auctor ipsum.",
      "reviews":[  
         {  
            "author_name":"Jane Doe",
            "author_image":"assets\/img\/person-01.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
         },
         {  
            "author_name":"Norma Brown",
            "author_image":"assets\/img\/person-02.jpg",
            "date":"09.05.2016",
            "rating":4,
            "review_text":"Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
         }
      ],
      "opening_hours":[  
         "Closed",
         "Closed",
         "Closed",
         "Closed",
         "08:00am - 16:00pm",
         "08:00am - 16:00pm",
         "Closed"
      ]
   }
]