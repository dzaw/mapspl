<?php

// fetching data from get request - tests 

$arr = array();
foreach ($_GET as $value => $key) {
        array_push($arr, $_GET[$value]);
}

$list = array();
foreach ($arr as $key => $val) {
	for ($i=0; $i<count($val); $i++){
		$list[] = $val[$i];
	}
}

$search_val =  "( " . implode(", ", $list) . " ) ";
//echo $search_val;

require('../db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query_join = "SELECT * FROM data JOIN city ON (data.cityid=city.cityid) JOIN category ON (data.category=category.cat_id) WHERE data.id IN " . $search_val . ";";

$sth = $DBcon->prepare($query_join);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($result);
echo json_encode($result);


?>

