<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query = "SELECT * FROM view1 WHERE id = " . htmlspecialchars($_POST["id"]). ";";

$sth = $DBcon->prepare($query);
$sth->execute();


while ($data = $sth->fetchAll(PDO::FETCH_ASSOC)) {
//var_dump($result);

        foreach ($data as $data_poi => $data_a){
                //echo $val[0]; // echoes cities in all countries on pos 0
                //echo $result['inne'][0]; // echoes just city in one cntry on pos 0
                //echo $key; // echoes cntries
                //echo 'ok';

echo '<div class="modal-item-detail modal-dialog" role="document" data-latitude="'.$data_a['latitude'].'" data-longitude="'.$data_a['longitude'].'" data-address="">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>'.$data_a['title'].'</h2>
                <div class="label label-default">'.$data_a['1'].'</div><div class="rating-passive" data-rating="'.$data_a['rating'].'">
                        <span class="stars"></span>
                        <span class="reviews">'.$data_a['reviews_number'].'</span>
                    </div><div class="controls-more">
                    <ul>
                        <li><a href="#">Dodaj do ulubionych</a></li>
                        <li><a href="#">Dodaj do obserwowanych</a></li>
                    </ul>
                </div>
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left"><div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0"><img src="'.$data_a['sidebar_image'].'" alt=""><img src="'.$data_a['sidebar_image'].'" alt=""><img src="'.$data_a['sidebar_image'].'" alt=""></div>
                    <!--end gallery--><div class="map" id="map-modal"></div>
                <!--end map-->
                <section>
                <h3>Adres</h3><h5><i class="fa fa-map-marker"></i>'.$data_a['location'].'</h5><h5><i class="fa fa-phone"></i>'.$data_a['phone'].'</h5><h5><i class="fa fa-envelope"></i>'.$data_a['email'].'</h5></section>
                <section>
                    <h3>Social Share</h3>
                    <div class="social-share"></div>
                </section>
            </div>
            <!--end left -->
            <div class="right">
                <section>
                    <h3>O obiekcie</h3>
                    <div class="read-more"><p>'.$data_a['description'].'</p></div>
                </section>
                <!--end about--><section>
                            <h3>Cechy</h3>
                            <ul class="tags"><li>tag1</li><li>tag2</li><li>tag3</li></ul>
                    </section>
                    <!--end tags--><section>
                        <h3>Opinie</h3><div class="review">
                                <div class="image">
                                    <div class="bg-transfer" style="background-image: url(assets/img/person-01.jpg)"></div>
                                </div>
                                <div class="description">
                                    <figure>
                                        <div class="rating-passive" data-rating="4">
                                            <span class="stars"></span>
                                        </div>
                                        <span class="date">09.05.2016</span>
                                    </figure>
                                    <p>Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros</p>
                                </div>
                            </div>
                            <!--end review--><div class="review">
                                <div class="image">
                                    <div class="bg-transfer" style="background-image: url(assets/img/person-02.jpg)"></div>
                                </div>
                                <div class="description">
                                    <figure>
                                        <div class="rating-passive" data-rating="4">
                                            <span class="stars"></span>
                                        </div>
                                        <span class="date">09.05.2016</span>
                                    </figure>
                                    <p>Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis</p>
                                </div>
                            </div>
                            <!--end review--></section>
                    <!--end reviews--></div>
            <!--end right-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->';
}
}
?>
