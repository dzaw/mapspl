<?php
// fetching all data to sidebar
//$json=file_get_contents("http://dagmarazawada.pl/mapsPL/assets/external/db_data.php");
//$data =  json_decode($json, true);

// fetching data from post request

$arr = array();
foreach ($_POST as $value => $key) {
        array_push($arr, $_POST[$value]);
}

$list = array();
foreach ($arr as $key => $val) {
        for ($i=0; $i<count($val); $i++){
                $list[] = $val[$i];
        }
}

$search_val =  "( " . implode(", ", $list) . " ) ";
//echo $search_val;

if (empty($list)) {
	echo 'Move map around or zoom out to get some results here!';
	die();
}

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query_join = "SELECT * FROM data JOIN city ON (data.cityid=city.cityid) JOIN category ON (data.category=category.cat_id) WHERE data.id IN " . $search_val . ";";

$sth = $DBcon->prepare($query_join);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);

$json = json_encode($result);
$data = json_decode($json, true);


foreach ($data as $data_poi => $data_a) {
	echo '<div class="result-item" data-id="'.$data_a['id'].'">';
		echo '<a href=""><h3>'.$data_a['title'].'</h3>';
			echo '<div class="result-item-detail">
				<div class="image" style="background-image: url('.$data_a['sidebar_image'].')"></div>
				<div class="description"><h5><i class="fa fa-map-marker"></i>'.$data_a['location'].'</h5>
					<div class="rating-passive"data-rating="'.$data_a['rating'].'">
						<span class="stars"></span>
	                    <span class="reviews">'.$data_a['reviews_number'].'</span>
	                </div>
	            <div class="label label-default">'.$data_a['category'].'</div><p>'.$data_a['description'].'</p></div>
			</div>
		</a>
		<div class="controls-more">
			<ul>
				<li><a href="#" class="add-to-favorites">Dodaj do ulubionych</a></li>
				<li><a href="#" class="add-to-watchlist">Dodaj do obserwowanych</a></li>
			</ul>
		</div>
	</div>';
}
?>



