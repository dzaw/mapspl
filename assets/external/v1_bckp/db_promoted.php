<?php
$json=file_get_contents("http://dagmarazawada.pl/mapsPL/assets/external/db_data.php");
$data =  json_decode($json, true);
foreach ($data as $data_poi => $data_a) {
echo '		  	 <div class="col-md-3 col-sm-3">
                              <div class="item promo-item" data-id="'.$data_a['id'].'">
                                  <figure class="circle featured sale">!</figure>
                                  <a> <!-- '.$data_a['url'].' -->
                                      <div class="description">
                                          <figure>'.$data_a['city_pl'].'</figure>
                                          <div class="label label-default">'.$data_a['category_pl'].'</div>
                                          <h3>'.$data_a['title'].'</h3>
                                          <h4>'.$data_a['location'].'</h4>
                                      </div>
                                      <!--end description-->
                                      <div class="image bg-transfer">
                                        <img src="'.$data_a['sidebar_image'].'" alt="">
                                      </div>
                                      <!--end image-->
                                  </a>
                                  <div class="additional-info">
                                      <div class="rating-passive" data-rating="'.$data_a['rating'].'">
                                          <span class="stars"></span>
                                          <span class="reviews">'.$data_a['reviews_number'].'</span>
                                      </div>
                                      <div class="controls-more">
                                          <ul>
                                              <li><a href="index.html#">Dodaj do ulubionych</a></li>
                                              <li><a href="index.html#">Dodaj do obserwowanych</a></li>
                                              <li><a href="index.html#" class="quick-detail">Szybki podgląd</a></li>
                                          </ul>
                                      </div>
                                      <!--end controls-more-->
                                  </div>
                                  <!--end additional-info-->
                              </div>
                              <!--end item-->
                           </div>
';
}

?>
