<?php

require('db_conn_mapspl2.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

// test query
$query = "SELECT * from view1;";

// query concat and group
$query_concat = "SELECT data.id, data.latitude, data.longitude, data.featured, data.title, data.location, city.city_pl, data.phone, data.email, data.website, data.category, data.rating, data.reviews_number, data.marker_image, data.sidebar_image, data.url, data.description, category.category_pl, group_concat(city.cityid) as cityid, group_concat(category.cat_id) as category from data, city, category where data.cityid = city.cityid and data.category = category.cat_id group by data.id";

// query join
$query_join = "SELECT * FROM data JOIN city ON (data.cityid=city.cityid) JOIN category ON (data.category=category.cat_id);";

$sth = $DBcon->prepare($query);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($result);
echo json_encode($result);


?>

