<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query = "SELECT DISTINCT * FROM Cities ORDER BY CityName";

$sth = $DBcon->prepare($query);
$sth->execute();

echo '<option value="">Miasto</option>';
while ($result = $sth->fetchAll(PDO::FETCH_ASSOC)) {

  foreach ($result as $key => $val) {
    echo '<option data-country="' . $val['CountryID'] . '" value="' . $val['IDCity'] . '">' . $val['CityName'] . '</option>';
  }

}

?>


