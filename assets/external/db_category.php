<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$query = "SELECT DISTINCT cat_id, category_pl FROM view1 ORDER BY category_pl";

$sth = $DBcon->prepare($query);
$sth->execute();

while ($result = $sth->fetchAll(PDO::FETCH_ASSOC)) {
//var_dump($result);

  foreach ($result as $key => $val) {
    echo '<option value="' . $val['cat_id'] . '">' . $val['category_pl'] . '</option>';
  }

}


?>


