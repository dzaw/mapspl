<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

$keyword = htmlspecialchars($_POST['keyword']);
$city = htmlspecialchars($_POST['city']);
$country = htmlspecialchars($_POST['country']);
$category = htmlspecialchars($_POST['category']);
$subcategory = htmlspecialchars($_POST['subcategory']);

//echo $keyword, $city, $category, $subcategory; 

// how to search properly ??????

//$query_isset = '%polacco%';
// query search
$query_search1 = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%'";

if ($keyword && $city && $country && $category && $subcategory) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND cityid = '$city' AND country = '$country' AND category_pl = '$category' AND subcat_pl = '$subcategory';";
}
elseif ($keyword && $city && $country && $category) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND cityid = '$city' AND country = '$country' AND category_pl = '$category';";
}
elseif ($keyword && $city && $country) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND cityid = '$city' AND country = '$country';";
}
elseif ($keyword && $city) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND cityid = '$city';";
}
elseif ($keyword) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%';";
}
elseif ($keyword && $category && $subcategory) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND category_pl = '$category' AND subcat_pl = '$subcategory';";
}
elseif ($keyword && $category) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND category_pl = '$category';";
}
elseif ($keyword && $subcategory) {
	$query_search = "SELECT * FROM view1 WHERE title LIKE '%$keyword%' OR description LIKE '%$keyword%' OR location LIKE '%$keyword%' AND subcat_pl = '$subcategory';";
}

elseif ($country && $city) {
	$query_search = "SELECT * FROM view1 WHERE cntry_id = '$country' AND cityid = '$city';";
}
elseif ($category && $subcategory) {
	$query_search = "SELECT * FROM view1 WHERE cat_id = '$category' AND subcat_id = '$subcategory';";
}
elseif ($country && $category) {
	$query_search = "SELECT * FROM view1 WHERE cntry_id = '$country' AND cat_id = '$category';";
}
elseif ($country && $subcategory) {
	$query_search = "SELECT * FROM view1 WHERE cntry_id = '$country' AND subcat_id = '$subcategory';";
}


elseif ($country) {
	$query_search = "SELECT * FROM view1 WHERE cntry_id = '$country';";
}
elseif ($city) {
	$query_search = "SELECT * FROM view1 WHERE cityid = '$city';";
}
elseif ($category) {
	$query_search = "SELECT * FROM view1 WHERE cat_id = '$category';";
}
elseif ($subcategory) {
	$query_search = "SELECT * FROM view1 WHERE subcat_id = '$subcategory';";
}


else {
	$query_search = $query_search1;
}

// TODO:
// query should be like : SELECT * FROM view1 WHERE title LIKE (?) OR description LIKE (?) OR location LIKE (?)
// and execute like: execute (keyword1, keyword2, keyword3)..

$sth = $DBcon->prepare($query_search);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($result);
echo json_encode($result);


?>

