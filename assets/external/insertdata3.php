<?php

require('db_conn.php');

$up_id = uniqid();

$query_city = "SELECT * FROM Cities ORDER BY CityName;";
$query_country = "SELECT * FROM Countries;";
$query_category = "SELECT * FROM Categories;";
$query_subcat = "SELECT * FROM Subcategories;";

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sth1 = $DBcon->prepare($query_city);
$sth1->execute();
$sth2 = $DBcon->prepare($query_country);
$sth2->execute();
$sth3 = $DBcon->prepare($query_category);
$sth3->execute();
$sth4 = $DBcon->prepare($query_subcat);
$sth4->execute();

}
catch(PDOException $ex){
        die($ex->getMessage());
}

while ($result = $sth1->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result as $key => $val) {
    $cityoption[] = '<option value="' . $val['IDCity'] . '">' . $val['CityName'] . '</option>';
  }
}
while ($result2 = $sth2->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result2 as $key2 => $val2) {
    $cntryoption[] = '<option value="' . $val2['IDCountry'] . '">' . $val2['CountryName'] . '</option>';
  }
}
while ($result3 = $sth3->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result3 as $key3 => $val3) {
    $categoryoption[] = '<option value="' . $val3['IDCtgry'] . '">' . $val3['CatName'] . '</option>';
  }
}
while ($result4 = $sth4->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result4 as $key4 => $val4) {
    $subcatoption[] = '<option value="' . $val4['IDSubctgry'] . '">' . $val4['SubcatName'] . '</option>';
  }
}

echo '
<script type="text/javascript" src="http:\/\/maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
<script type="text/javascript" src="http:\/\/ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http:\/\/ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>



<link href="uploadfiles/style-progress.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="\/\/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style>
    label, input, select { display:block; width:100%; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#insert-data-table { width: 350px; margin: 20px 0; }
    div#insert-data-table table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#insert-data-table table td, div#insert-data-table table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
    body { margin:0 10%; color: #333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-weight: 300; }
    h1 { text-align: center; font-weight:normal; }
    h2 { font-weight:normal; width:100%; float:left; margin: 20px 0  0px 0; }
    label { width: 100px; float:left; text-align:right; margin:4px 10px 0 0; }
    label#geo_label { width: auto; float: none; }
    p { float:left; width: 100%; }
    .gist { margin-top: 10px; font-size: 12px; }
    .ui-autocomplete-input, .input input { border: none; font-size: 14px; width: 300px; height: 24px; margin-bottom: 5px; padding-top: 2px; }
    textarea { border-color: lightgray; font-size: 14px; margin-bottom: 5px; padding-top: 2px; width: 100%;}
    .ui-autocomplete-input { border: 1px solid #DDD !important; padding-top: 0px !important; }
    .map-wrapper { float:left; margin: 0 10px 0 10px; }
    #map { border: 1px solid #DDD; width:300px; height: 300px; margin: 10px 0 10px 0; -webkit-box-shadow: #AAA 0px 0px 15px;}
    #legend { font-size: 12px;font-style: italic; }
    .ui-menu .ui-menu-item a { font-size: 12px; }
    .input { float:left; }
    .input-positioned { padding: 35px 0 0 0; }
    .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
    * html .clearfix { height: 1%; }
    .dialog_label { width:100%; text-align: left; }
    button { width:100%; display:inline-block; }
  </style>
  <script src="https:\/\/code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https:\/\/code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script language="JavaScript" type="text/javascript">

// only allow specific extensions
var exts = "jpg|jpeg|gif|png|bmp|tif|tiff|pdf|JPG|JPEG|GIF|PNG|BMP|TIF|TIFF|PDF";

function checkExtension(value)
{
    if(value=="")return true;
    var re = new RegExp("^.+\.("+exts+")$","i");
    if(!re.test(value))
    {
        alert("Niedozwolone rozszerzenie pliku: \n" + value + "\n\nDozwolone rozszerzenia: "+exts.replace(/\|/g,\',\')+" \n\n");
	$("input[name=\'item_file[]\']:last").val(\'\');
        return false;
    }

    return true;
}

$(document).ready(function() {
    $("#subcattr").hide();

//show the progress bar only if a file field was clicked
	var show_bar = 0;
    $("input[type=\'file\']").on("click", function(){
	show_bar=1;
	console.log(show_bar);
	if (show_bar === 1) {
                        $("#progress-frame").show();
                        function set () {
                                $("#progress-frame").attr("src","uploadfiles/progress-frame.php?up_id='; echo $up_id; echo '");
                        }
                        setTimeout(set);
                }

    });


});

var next_id=0;
var max_number =4;

	function _add_more() {
		if (next_id>=max_number)
		{
			alert("Przekroczono dopuszczalną liczbę zdjęć.");
			return;
		}

		next_id=next_id+1;
		var next_div=next_id+1;
		var txt = "<br><input type=\"file\" name=\"item_file[]\" id=\"item_file[]\" onChange=\"checkExtension(this.value)\">";
		txt+=\'<div id="dvFile\'+next_div+\'"></div>\';
		document.getElementById("dvFile" + next_id ).innerHTML = txt;
	}

  </script>

  <script>
  $( function() {
    var dialog, form, dialog2, form2,
      emailRegex = /^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );


$("select#category").on("change", function(){
	$("#subcattr").show();
	$("#subcategory option").hide();
	if ($("#category :selected").val() == "1"){
	 	$("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
		$("#subcategory option[value=\'1\']").show();
		$("#subcategory option[value=\'2\']").show();
		$("#subcategory option[value=\'3\']").show();
		$("#subcategory option[value=\'4\']").show();
		$("#subcategory option[value=\'5\']").show();
		$("#subcategory option[value=\'6\']").show();
	}
	if ($("#category :selected").val() == "2"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'7\']").show();
                $("#subcategory option[value=\'8\']").show();
                $("#subcategory option[value=\'9\']").show();
                $("#subcategory option[value=\'10\']").show();
                $("#subcategory option[value=\'11\']").show();
        }
	if ($("#category :selected").val() == "3"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'12\']").show();
                $("#subcategory option[value=\'13\']").show();
                $("#subcategory option[value=\'14\']").show();
                $("#subcategory option[value=\'15\']").show();
                $("#subcategory option[value=\'16\']").show();
        }
	if ($("#category :selected").val() == "4"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'17\']").show();
                $("#subcategory option[value=\'18\']").show();
                $("#subcategory option[value=\'19\']").show();
        }
        if ($("#category :selected").val() == "5"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'20\']").show();
                $("#subcategory option[value=\'21\']").show();
                $("#subcategory option[value=\'22\']").show();
                $("#subcategory option[value=\'23\']").show();
        }
        if ($("#category :selected").val() == "6"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'24\']").show();
                $("#subcategory option[value=\'25\']").show();
                $("#subcategory option[value=\'26\']").show();
                $("#subcategory option[value=\'27\']").show();
        }
        if ($("#category :selected").val() == "7"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'28\']").show();
                $("#subcategory option[value=\'29\']").show();
                $("#subcategory option[value=\'30\']").show();
                $("#subcategory option[value=\'31\']").show();
        }
        if ($("#category :selected").val() == "8"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'32\']").show();
                $("#subcategory option[value=\'33\']").show();
                $("#subcategory option[value=\'34\']").show();
                $("#subcategory option[value=\'35\']").show();
        }
});






    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }

    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Zawartość " + n + " musi być między " +
          min + " i " + max + "znaków." );
        return false;
      } else {
	o.removeClass("ui-state-error");
        return true;
      }
    }

    function checkTxtLength( o, n, m, min ) {
      if ( o.text().length < min ) {
        m.addClass( "ui-state-error" );
        updateTips( "Długość tekstu " + n + " musi być większa niż " +
          min + "." );
        return false;
      } else {
        m.removeClass("ui-state-error");
        return true;
      }
    }

    function checkCoords( o, n, min, max ) {
      if ( o.val() > max || o.val() < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Coord " + n + "should be in range " + min + " and " + max + "." );
        return false;
      } else {
        o.removeClass("ui-state-error");
        return true;
      }
    }

    function checkPresence( o, n ) {
      if ( o.val() ==  "" ) {
        o.addClass( "ui-state-error" );
        updateTips( "Wybierz opcję w " + n + "." );
        return false;
      } else {
	o.removeClass("ui-state-error");
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addCity() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      if ( valid ) {
	//console.log("valid");

	$.post( "db_insertcity.php", { cityname: $("#CityName").val() , countryid: $("#CountryID").val() }, function( data ) {
  		console.log( data.value1 );
		alert(data.value1);
		$("#cityid").load("db_cities.php");
		//$("#city option:last").before("<option value="...">$("#CityName").val()</option>");
	}, "json");

        dialog.dialog( "close" );
      }
      return valid;
    }

    function addPhotos() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
        if ( valid ) {
//        console.log("valid");



           $("#progress-frame").show();
           function set () {
             $("#progress-frame").attr("src","uploadfiles/progress-frame.php?up_id='; echo $up_id; echo '");
           }
           setTimeout(set);




	var ajaxData = new FormData();
	  $.each($(\'input[type=file]\'), function(i, obj) {
    	  $.each(obj.files,function(j, file){
    	    ajaxData.append(\'item_file[]\', file);
    	  })
	});

	$.ajax({
	  url: "uploadfiles/upload.php",
	  type: "POST",
	  data: ajaxData, // The form with the file inputs.
	  contentType: false,
	  processData: false                        // Using FormData, no need to process data.
	}).done(function(file){
	  console.log("Success: Files sent!" + file);
		filen = file;
		file = file.replace(\'["\', \'\');
		file = file.replace(\'"]\', \'\');
		file = file.replace((/[\\/|\\\\]/g), \'\');
		file = file.replace(\'","\', \', \');
		file = file.replace(/assetsexternaluploadfilesitems/g, \'\');
 	  $("#photos-td").html("<td id=\"photospath\" data-file="+filen+">"+file+"</td>");
	}).fail(function(){
	  console.log("An error occurred, the files couldn\'t be sent!");
	});

	dialog2.dialog("close");
      }
      return valid;
    }

    function addPhoto() {
      for (var i=0; i<4;i++){
	console.log(i);
        var file = document.getElementById("POIphoto"+i).files[0];
        var reader = new FileReader();
      	reader.readAsText(file, "UTF-8");
      	reader.onload = shipOff;
      }
    }
	function shipOff(event) {
    	  var result = event.target.result;
    	  var fileName = document.getElementById("POIphoto1").files[0].name;
          $.post( "fileupload.php", { photo: result, name: fileName }, function( data ) {
                console.log( data.value1 );
                alert(data.value1);
          }, "json");
      }

 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Dodaj": addCity,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addCity();
    });


    dialog2 = $( "#dialog-form-photo" ).dialog({
      autoOpen: false,
      height: 500,
      width: 350,
      modal: true,
      buttons: {
       "Prześlij": addPhotos,
        Cancel: function() {
          dialog2.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form2 = dialog.find( "upload-form" ).on( "submit", function( event ) {
      event.preventDefault();
      addPhotos();
    });

    $( "#cityid" ).on("change", function() {
	if ($(this).val() == "addnewcity"){
		dialog.dialog( "open" );
	}
    });

    $("#add-photo-dialog").button().on("click", function(){
	dialog2.dialog("open");
    });

    $("#POIprofile_button").button().on("click", function(){
        dialog2.dialog("open");
    });



    function validate_all() {
	valid = true;

        var countryvar = $("#countryid");
	var cityvar = $("#cityid");
        var streetvar = $("#addresspicker_map");
	var latvar = $("#lat");
	var lonvar = $("#lng");
	var catvar = $("#category");
	var subcatvar = $("#subcategory");
	var typevar = $("#type");
	var poiname =  $("#POIname");
	var logovar = $("#logoid");
	var statvar = $("#status");
	var fidvar = $("#fidelity");
	var photovar = $("#photospath");
	var updatervar =  $("#updater");

	checkPresence( countryvar, "Kraj");
	checkPresence(cityvar, "Miasto");
	checkLength( streetvar, "Ulica", 3, 80 );
	checkCoords (latvar, "X Latitude", -90, 90);
	checkCoords (lonvar, "Y Longitude", -180, 180);
	checkPresence(catvar, "Kategoria");
	checkPresence(subcatvar, "Podkategoria");
	checkPresence(typevar, "Typ");
      	checkLength( poiname, "Nazwa obiektu", 3, 50 );
	checkPresence(logovar, "Logo");
	checkPresence(statvar, "Status");

// POI MAIL ?????
	//var email = $("#poimail");
      	//valid = valid && (if(email != ""){checkRegexp( email, emailRegex, "eg. ui@jquery.com" )});

	checkPresence(fidvar, "Dokładność");
	checkTxtLength( photovar, "Zdjęcia", $("#add-photo-dialog"), 3);
      	checkLength( updatervar, "Wprowadzający", 3, 40 );

      	//valid = valid && checkRegexp( poiname, /^[a-z]([0-9a-z_\s])+$/i, "Nazwa może zawierać znaki a-z, 0-9, _, spacje i musi zaczynać się od litery." );
      	//valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

        valid = valid && checkPresence( countryvar, "Kraj") && checkPresence(cityvar, "Miasto") && checkLength( streetvar, "Ulica", 3, 80 )  && checkCoords (latvar, "X Latitude", -90, 90) && checkCoords (lonvar, "Y Longitude", -180, 180) && checkPresence(catvar, "Kategoria") && checkPresence(subcatvar, "Podkategoria") && checkPresence(typevar, "Typ") && checkLength( poiname, "Nazwa obiektu", 3, 50 ) && checkPresence(logovar, "Logo") && checkPresence(statvar, "Status") && checkPresence(fidvar, "Dokładność") && checkTxtLength( photovar, "Zdjęcia", $("#add-photo-dialog"), 3) && checkLength( updatervar, "Wprowadzający", 3, 40 );
        return valid;
    }


    $( "#create-POI" ).button().on( "click", function() {

	validate_all();

	if (valid == true) {
	console.log(valid);
	$.post( "db_insertPOI.php", { city_id: $("#cityid").val(), address_street: $("#addresspicker_map").val(), address_code: $("#postal_code").val(), lat: $("#lat").val(), lon: $("#lng").val(), updater_name: $("#updater").val(), poi_name: $("#POIname").val(), cat_id: $("#category").val(), subcat_id: $("#subcategory").val(), type_id: $("#type").val(), logo: $("#logoid").val(), poi_profile: "", tags_id: "", poi_desc: $("#description").val(), poi_www: $("#poiwww").val(), poi_phone: $("#poiphone").val(), poi_mail: $("#poimail").val(), fidelity: $("#fidelity").val(), poi_status: $("#status").val(), check_date: $("#POIchkdate").val(), url_fb: $("#url_fb").val(), url_tt: $("#url_tt").val(), url_lin: $("#url_lin").val(), url_ta: $("#url_ta").val(), photo_path: $("#photospath").attr(\'data-file\') }, function( data ) {
                alert( data.value1 );
                console.log( data.value1 );
		window.location.reload();
        }, "json");
	}
    });

});
  </script>

<script src="http:\/\/xilinus.com/jquery-addresspicker/src/jquery.ui.addresspicker.js"></script>

  <script>
  $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
    });
    var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
      regionBias: "",
      updateCallback: showCallback,
      reverseGeocode: true,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(51.5, 0.0),
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map:      "#map",
        lat:      "#lat",
        lng:      "#lng",
        street_number: "#street_number",
        route: "#route",
        locality: "#locality",
        administrative_area_level_2: "#administrative_area_level_2",
        administrative_area_level_1: "#administrative_area_level_1",
        country:  "#country",
        postal_code: "#postal_code",
        type:    "#type"
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

    $("#reverseGeocode").change(function(){
        $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === "true"));
    });

    function showCallback(geocodeResult, parsedGeocodeResult){
      $("#callback_result").text(JSON.stringify(parsedGeocodeResult, null, 4));
    }
    // Update zoom field
    var map = $("#addresspicker_map").addresspicker("map");
    google.maps.event.addListener(map, "idle", function(){
      $("#zoom").val(map.getZoom());
    });

  });
  </script>


<div class="modal-dialog width-800px" role="document" data-latitude="40.7344458" data-longitude="-73.86704922" data-marker-drag="true">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>Dodaj Wpis</h2>
            </div>
        </div>
        <div class="modal-body">
            <form class="form inputs-underline">
                <section>









<div id="dialog-form" title="Dodaj nowe miasto do bazy">
  <p>Wybierz odpowiedni kraj i wpisz nazwę miasta / miejscowości</p>
 
  <form id="cityform">
    <fieldset>
      <label class="dialog_label" for="CountryID">Kraj</label>
	<select id="CountryID" class="ui-widget-content ui-corner-all">
        <option value="">Wybierz kraj..</option>
';
	for ($i = 0; $i <= count($cntryoption); $i++) {
	echo $cntryoption[$i];
	};
echo '
        </select><br/>
      <label class="dialog_label" for="CityName">Nazwa miasta / miejsca</label>
      <input type="text" spellcheck name="CityName" id="CityName" class="text ui-widget-content ui-corner-all">
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
 

<div id="dialog-form-photo" title="Dodaj zdjęcia obiektu">
  <p>Dodaj pliki z dysku. Dopuszczalne formaty: JPG, PNG, GIF, TIFF. Maksymalny rozmiar jednego pliku to 2MB, maksymalna ilość 5 plików. Dopiero po dodaniu wszystkich zdjęć kliknij "Prześlij".</p>
 

<form enctype="multipart/form-data" action="uploadfiles/upload.php" method="post" name="upload-form" id="upload-form">
        <!--hidden field-->
         <input type="hidden" value="demo" name="';

echo ini_get("session.upload_progress.name");

echo '"/>
	<div id="dvFile0"><input type="file" accept="image/*" name="item_file[]" onChange="checkExtension(this.value)"></div><div id="dvFile1"></div>
<br/>
        <a href="javascript:_add_more(0);"><span style="float: right;"><u>(+) Dodaj więcej</u></span></a>
<br/>
<br/>
<!--	<input id="upload-send" type="submit" value="Upload!"> -->
</form>
	<!--Include the progress bar frame-->



</div>

<!--
<div id="dialog-form-links" title="Dodaj linki obiektu">
  <p>Dodaj linki do obiektu </p>
 
  <form id="linksform">
    <fieldset>
      <label class="dialog_label" for="image">Wybierz rodzaj linku i wpisz url</label>
        <input type="text" id="url_fb" name="POIlink_fb" placeholder="Facebook">
        <br/>
	<input type="text" id="url_tt" name="POIlink_tt" placeholder="Twitter">
        <br/>
	<input type="text" id="url_lin" name="POIlink_lin" placeholder="LinkedIn">
	<br/>
	<input type="text" id="url_ta" name="POIlink_ta" placeholder="TripAdvisor">

      <!-- Allow form submission with keyboard without duplicating the dialog button
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
-->
 
<div id="insert-data-table" class="ui-widget">
  <h1>Add Data</h1>
  <table id="users" class="ui-widget ui-widget-content">
      	<tr class="ui-widget-header "><th>Field</th>
		<td>Value</td>
	</tr>
	<tr><th>Kraj</th>
		<td><select id="countryid">
        <option value="">Wybierz państwo..</option>
';
        for ($i = 0; $i <= count($cntryoption); $i++) {
                echo $cntryoption[$i];
        };
echo '
        </select></td>
        </tr>
	<tr><th>Miasto</th>
		<td><select id="cityid">
        <option value="">Wybierz miasto z bazy..</option>
';
        for ($i = 0; $i <= count($cityoption); $i++) {
                echo $cityoption[$i];
        };
echo '
        <option value="addnewcity">+ Dodaj nowe miasto do bazy punktów</option>
        </select></td>
	</tr>
	<tr><th>Ulica</th>
		<td><input id="addresspicker_map" type="text" placeholder="Zacznij wpisywać adres / wskaż na mapie"></td>
		<td rowspan=2>
<div>
      <label id="geo_label" for="reverseGeocode" style="display: inline;">Uzupełnianie adresu i kodu na podstawie mapy:</label>
      <select id="reverseGeocode" style="display: inline; width: 100px;">
        <option value="true" selected>On</option>
        <option value="false">Off</option>
      </select><br/>
</div>
</td>
	</tr>
	<tr><th>Kod pocztowy</th>
		<td><input id="postal_code" type="text" placeholder="Kod pocztowy"></td>
	</tr>
	<tr><th>X (latitude)</th>
		<td><input id="lat" type="number"/></td>
		<td rowspan="2">
    <div class="map-wrapper">

      <div id="map"></div>
      <div id="legend">drag and drop the marker to the correct location</div>
    </div>

	</td>
	</tr>
	<tr><th>Y (longitude)</th>
		<td><input id="lng" type="number"/></td>
	</tr>
	<tr><th>Kategoria</th>
		<td><select id="category">
        <option value="">Wybierz kategorię z bazy..</option>
';
        for ($i = 0; $i <= count($categoryoption); $i++) {
                echo $categoryoption[$i];
        };
echo '
        </select></td>
	</tr>
	<tr id="subcattr"><th>Podkategoria</th>
		<td><select id="subcategory">
        <option value="">Wybierz podkategorię  z bazy..</option>
';
        for ($i = 0; $i <= count($subcatoption); $i++) {
                echo $subcatoption[$i];
        };
echo '
        </select></td>
	</tr>
	<tr><th>Typ</th>
		<td><select id="type">
	<option value="">Wybierz typ obiektu..</option>
	<option value="2">non-commerce</option>
	<option value="1">commerce</option>
	</select></td>
	</tr>
	<tr><th>Nazwa obiektu</th>
		<td><input id="POIname" type="text" placeholder="Nazwa obiektu" required></td>
	</tr>
	<tr><th>Logo na mapie</th>
		<td><select id="logoid"><option value="">Wybierz rodzaj logo</option>
	<option value="assets\/img\/icons\/art.png">art</option>
	<option value="assets\/img\/icons\/education.png">education</option>
	<option value="assets\/img\/icons\/restaurant.png">restaurant</option>
	<option value="assets\/img\/icons\/organization.png">organization</option>
	<option value="assets\/img\/icons\/memorialsite.png">memorial</option>
	</select></td>
	</tr>
	<tr><th>Opis obiektu</th>
		<td><textarea id="description" spellcheck rows="4" placeholder="Opis obiektu" required></textarea></td>
	</tr>
	<tr><th>WWW obiektu</th>
		<td><input id="poiwww" type="url" placeholder="http://www.example.com"></td>
	</tr>
	<tr><th>E-mail obiektu</th>
		<td><input id="poimail" type="email" x-moz-errormessage="Please specify a valid email address." placeholder="mail@mail.com"></td>
	</tr>
	<tr><th>Telefon obiektu</th>
                <td><input id="poiphone" type="tel" placeholder="(+0123)555-123-456"></td>
        </tr>
	<tr><th>Data sprawdzenia</th>
		<td><input id="POIchkdate" type="date"></td>
	</tr>
	<tr><th>Dokładność oznaczenia (fidelity)</th>
                <td><select id="fidelity">
		<option value="">Wybierz dokładność oznaczenia..</option>
		<option value="1">budynek</option>
		<option value="2">ulica</option>
		<option value="3">miasto</option>
		<option value="4">region</option>
		<option value="5">państwo</option>
	</td>
        </tr>
	<tr><th>Status</th>
                <td><select id="status">
        <option value="">Wybierz status obiektu..</option>
        <option value="1">istniejący</option>
        <option value="2">nie istniejący</option>
        </select></td>
        </tr>
	<tr><th>Linki</th><td>

	<input type="text" id="url_fb" name="POIlink_fb" placeholder="Facebook">
        <br/>
        <input type="text" id="url_gp" name="POIlink_gp" placeholder="GooglePlus">
        <br/>
        <input type="text" id="url_tt" name="POIlink_tt" placeholder="Twitter">
        <br/>
        <input type="text" id="url_ins" name="POIlink_ins" placeholder="Instagram">
        <br/>
        <input type="text" id="url_lin" name="POIlink_lin" placeholder="LinkedIn">
        <br/>
        <input type="text" id="url_ta" name="POIlink_ta" placeholder="TripAdvisor">

	<!--<button id="add-links-dialog">Dodaj linki</button>-->
	</td></tr>
	<tr><th>Foto</th><td id="photos-td"><button id="add-photo-dialog">Dodaj zdjęcia</button>
   	 <iframe style="position: relative; top: 5px; height: 40px;" id="progress-frame" name="progress-frame" border="0" src="" scrollbar="no" frameborder="0" scrolling="no"> </iframe>

	</td></tr>
	<!--<tr><th>Video</th><td><button/></td></tr>-->
	<tr><th>Wprowadzający</th><td><input id="updater" type="text"></td></tr>

  </table>

<div class="clearfix">

  <div style="display:none;" class="input-positioned">
    <label>Callback: </label>
    <textarea id="callback_result" rows="15"></textarea>
  </div>


</div>
<button id="create-POI">Dodaj punkt do bazy</button>
<p class="validateTips"></p>
</body>
</html>
';

?>
