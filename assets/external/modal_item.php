<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

//$query = "SELECT * FROM data WHERE id = " . htmlspecialchars($_POST["id"]). ";";

$query = "SELECT * FROM view1 WHERE id = " . htmlspecialchars($_POST["id"]) . ";";

$sth = $DBcon->prepare($query);
$sth->execute();

// 1 - commerce, 2 - non-commerce

while ($data = $sth->fetchAll(PDO::FETCH_ASSOC)) {
//var_dump($data);

        foreach ($data as $data_poi => $data_a){
                //echo $val[0]; // echoes cities in all countries on pos 0
                //echo $result['inne'][0]; // echoes just city in one cntry on pos 0
                //echo $key; // echoes cntries
                //echo 'ok';
//var_dump($data_a);
if ($data_a['typeid'] == 1) {
echo '<div class="modal-item-detail modal-dialog" role="document" data-latitude="'.$data_a['latitude'].'" data-longitude="'.$data_a['longitude'].'" data-address="">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>'.$data_a['title'].'</h2>
                <img src="'.$data_a['marker_image'].'" height="20px"/> <div class="label label-default">'.$data_a['category_pl'].'</div><div class="rating-passive" data-rating="'.$data_a['rating'].'">
                        <!--<span class="stars"></span>
                        <span class="reviews">'.$data_a['reviews_number'].'</span>-->
                    </div><div class="controls-more">
		    		<ul>
                        <li><a href="#">Dodaj do ulubionych</a></li>
                        <li><a href="#">Byłem, widziałem - potwierdź</a></li>
						<li><a href="#">Zgłoś, że miejsce nie istnieje</a></li>
						<li><a href="#">Edytuj</a></li>
						<li><a href="#">Dodaj zdjęcie</a></li>
						<li><a target="_blank" href="';
					echo 'http://maps.google.com?saddr=Current+Location&daddr='.$data_a['latitude'].','.$data_a['longitude'].'"';
					echo '">Wyznacz trasę</a></li>
					</ul>
                </div>
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left">
                <iframe style="height:300px; width:350px;" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" src="assets/external/modal_streetview.php'.'?x='.$data_a['latitude'].'&y='.$data_a['longitude'].'&pov=1'.'">
 		</iframe>';
	if ($data_a['sidebar_image'] != null || $data_a['sidebar_image'] != "") {
	echo '
		<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">
                        <img src="'.$data_a['sidebar_image'].'" alt="img1">';
		if ($data_a['photo1'] != null || $data_a['photo1'] !="") {
        		echo '<img src="'.$data_a['photo1'].'" alt="img2">';
		}
		if ($data_a['photo2'] != null || $data_a['photo2'] !="") {
        		echo '<img src="'.$data_a['photo2'].'" alt="img3">';
		}
		if ($data_a['photo3'] != null) {
		        echo '<img src="'.$data_a['photo3'].'" alt="img4">';
		}
		if ($data_a['photo4'] != null) {
        		echo '<img src="'.$data_a['photo2'].'" alt="img5">';
		}
	echo '
		</div>
        <!--end gallery-->';
	}
	else {
		echo '<!-- no image -->';
	}
	echo '      
			<iframe src="https:\/\/www.facebook.com/plugins/page.php?href='. $data_a['fb_url'] .'%2F&tabs=timeline&width=350&height=220&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="350px" height="220px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
            <!--end left -->
            <div class="right">
                <section><h3>Kontakt</h3>
                <h5><i class="fa fa-map-marker"></i>'.$data_a['location'].'</h5>
                <h5><i class="fa fa-phone"></i>'.$data_a['phone'].'</h5> <h5><i class="fa fa-globe"></i><a href="'. $data_a['website'] .'" target="_blank"> '. $data_a['website'] .' </a></h5> <h5><i class="fa fa-envelope"></i>'.$data_a['email'].'</h5></section>
                <div class="map" id="map-modal"></div>
                <!--end map-->
                <section>
                        <ul class="tags"><li>tag1</li><li>tag2</li><li>tag3</li></ul>
                </section>
                <!--end tags-->
                <section>
                    <h3>Opis</h3>
                    <div class="read-more"><p>'.$data_a['description'].'</p></div>
 		</section>
                <!--end about-->
		<section>';
		if ($data_a['monday_open'] || $data_a['tuesday_open'] || $data_a['wednesday_open'] || $data_a['thursday_open'] || $data_a['friday_open'] || $data_a['saturday_open'] || $data_a['sunday_open']) {
                    echo '<h3>Godziny pracy</h3>';
		}
		if ($data_a['monday_open'] != null || $data_a['monday_open'] !="") {
                        echo '<p>poniedziałek: '.substr($data_a['monday_open'],0,-3).' - ' .substr($data_a['monday_close'],0,-3).'</p>';
                };
		if ($data_a['tuesday_open'] != null || $data_a['tuesday_open'] !="") {
                        echo '<p>wtorek: '.substr($data_a['tuesday_open'],0,-3).' - ' .substr($data_a['tuesday_close'],0,-3).'</p>';
                };
		if ($data_a['wednesday_open'] != null || $data_a['wednesday_open'] !="") {
                        echo '<p>środa: '.substr($data_a['wednesday_open'],0,-3).' - ' .substr($data_a['wednesday_close'],0,-3).'</p>';
                };
		if ($data_a['thursday_open'] != null || $data_a['thursday_open'] !="") {
                        echo '<p>czwartek: '.substr($data_a['thursday_open'],0,-3).' - ' .substr($data_a['thursday_close'],0,-3).'</p>';
                };
                if ($data_a['friday_open'] != null || $data_a['friday_open'] !="" && $data_a['friday_close'] != null || $data_a['friday_close'] !="") {
                        echo '<p>piątek: '.substr($data_a['friday_open'],0,-3).' - ' .substr($data_a['friday_close'],0,-3).'</p>';
                };
		if ($data_a['saturday_open'] != null || $data_a['saturday_open'] !="") {
                        echo '<p>sobota: '.substr($data_a['saturday_open'],0,-3).' - ' .substr($data_a['saturday_close'],0,-3).'</p>';
                };
		if ($data_a['sunday_open'] != null || $data_a['sunday_open'] !="") {
                        echo '<p>niedziela: '.substr($data_a['sunday_open'],0,-3).' - ' .substr($data_a['sunday_close'],0,-3).'</p>';
                };

echo '          </section>
                <section>
                    <h3>Udostępnij</h3>
                    <div class="social-share"></div>
                </section>
                </div>
            <!--end right-->
        <hr style="border: none; border-bottom: 2px solid red; margin: 0;">
	<section style="font-size: 85%;">
                <h3 style="font-size:95%; display: inline;">Data aktualizacji: </h3><span>'.substr($data_a['update_date'],0,-9).'</span>
                <h3 style="font-size:95%; display: inline;">Ostatnio odwiedzone: </h3><span>'.$data_a['check_date'].'</span></div>
        </section>
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->';

}
else {
echo '<div class="modal-item-detail modal-dialog" role="document" data-latitude="'.$data_a['latitude'].'" data-longitude="'.$data_a['longitude'].'" data-address="">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>'.$data_a['title'].'</h2>
                <img src="'.$data_a['marker_image'].'" height="20px"/> <div class="label label-default">'.$data_a['category_pl'].'</div><div class="rating-passive" data-rating="'.$data_a['rating'].'">
                        <!--<span class="stars"></span>
                        <span class="reviews">'.$data_a['reviews_number'].'</span>-->
                    </div><div class="controls-more">
                    <ul>
                        <li><a href="#">Dodaj do ulubionych</a></li>
                        <li><a href="#">Byłem, widziałem - potwierdź</a></li>
						<li><a href="#">Zgłoś, że miejsce nie istnieje</a></li>
						<li><a href="#">Edytuj</a></li>
						<li><a href="#">Dodaj zdjęcie</a></li>
						<li><a target="_blank" href="';
					echo 'http://maps.google.com?saddr=Current+Location&daddr='.$data_a['latitude'].','.$data_a['longitude'].'"';
					echo '">Wyznacz trasę</a></li>
                    </ul>
                </div>
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left">
		<iframe style="height:300px; width:350px;" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" src="assets/external/modal_streetview.php'.'?x='.$data_a['latitude'].'&y='.$data_a['longitude'].'&pov=1'.'">
		</iframe>';
	if ($data_a['sidebar_image'] != null || $data_a['sidebar_image'] != "") {
	echo '
		<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">
                        <img src="'.$data_a['sidebar_image'].'" alt="img1">';
		if ($data_a['photo1'] != null || $data_a['photo1'] !="") {
        		echo '<img src="'.$data_a['photo1'].'" alt="img2">';
		}
		if ($data_a['photo2'] != null || $data_a['photo2'] !="") {
        		echo '<img src="'.$data_a['photo2'].'" alt="img3">';
		}
		if ($data_a['photo3'] != null) {
		        echo '<img src="'.$data_a['photo3'].'" alt="img4">';
		}
		if ($data_a['photo4'] != null) {
        		echo '<img src="'.$data_a['photo2'].'" alt="img5">';
		}
	echo '
		</div>
        <!--end gallery-->';
	}
	else {
		echo '<!-- no image -->';
	}
	echo '
            </div>
            <!--end left -->
            <div class="right">
		<section><h3>Adres</h3>
                <h5><i class="fa fa-map-marker"></i>'.$data_a['location'].'</h5>
                </section>
                <div class="map" id="map-modal"></div>
                <!--end map-->
		<section>
                        <ul class="tags"><li>tag1</li><li>tag2</li><li>tag3</li></ul>
                </section>
                <!--end tags-->
		<section>
                    <h3>Opis</h3>
                    <div class="read-more"><p>'.$data_a['description'].'</p></div>
                </section>
                <!--end about-->
		<section>
		    <h3>Artykuły, wydarzenia, nawiązania</h3>
			<span><strong>data</strong> ...</span>
		</section>
		<section>
                    <h3>Udostępnij</h3>
                    <div class="social-share"></div>
                </section>
		</div>
            <!--end right-->
	<hr style="border: none; border-bottom: 2px solid red; margin: 0;">
	<section style="font-size: 85%;">
       		<h3 style="font-size:95%; display: inline;">Data aktualizacji: </h3><span>'.substr($data_a['update_date'],0,-9).'</span>
                <h3 style="font-size:95%; display: inline;">Ostatnio odwiedzone: </h3><span>'.$data_a['check_date'].'</span></div>
        </section>
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->';}
}
}
</script>

