<?php
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]". dirname($_SERVER['PHP_SELF']).'/';
$json=file_get_contents($url."assets/external/db_data.php");
$data =  json_decode($json, true);
foreach ($data as $data_poi => $data_a) {

echo '		    <div class="item featured promo-item" data-id="'.$data_a['id'].'">
                        <a>
                            <div class="description">
                                <figure>'.$data_a['city_pl'].'</figure>
                                <div class="label label-default">'.$data_a['category_pl'].'</div>
                                <h3>'.$data_a['title'].'</h3>
                                <h4>'.$data_a['location'].'</h4>
                            </div>
                            <div class="image bg-transfer">
                                <img src="'.$data_a['sidebar_image'].'" alt="">
                            </div>
                        </a>
                        <div class="additional-info">
						<!--
                            <div class="rating-passive" data-rating="'.$data_a['rating'].'">
                                <span class="stars"></span>
                                <span class="reviews">'.$data_a['reviews_number'].'</span>
                            </div>
						-->
                            <div class="controls-more">
                                <ul>
									<li><a href="#">Dodaj do ulubionych</a></li>
                        			<li><a href="#">Byłem, widziałem - potwierdź</a></li>
									<li><a href="#">Zgłoś, że miejsce nie istnieje</a></li>
									<li><a href="#">Edytuj</a></li>
									<li><a href="#">Dodaj zdjęcie</a></li>
									<li><a target="_blank" href="';
							echo 'http://maps.google.com?saddr=Current+Location&daddr='.$data_a['latitude'].','.$data_a['longitude'].'"';
							echo '">Wyznacz trasę</a></li>	
									<li><a href="index.html#" class="quick-detail">Szybki podgląd</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
';

}

?>
