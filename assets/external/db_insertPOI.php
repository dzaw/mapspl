<?php

require('db_conn.php');

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex){
        die($ex->getMessage());
}

// GET ALL DATA FROM insertdata.php
$AddressStreet = htmlspecialchars($_POST['address_street']);
$AddressCode = htmlspecialchars($_POST['address_code']);
$CityID = htmlspecialchars($_POST['city_id']);
$Lat = htmlspecialchars($_POST['lat']);
$Lon = htmlspecialchars($_POST['lon']);
$UpdaterName = htmlspecialchars($_POST['updater_name']);
$POIName = htmlspecialchars($_POST['poi_name']);
$CategoryID = htmlspecialchars($_POST['cat_id']);
$SubCatID = htmlspecialchars($_POST['subcat_id']);
$TypeID = htmlspecialchars($_POST['type_id']);
$Logo = htmlspecialchars($_POST['logo']);

//$POIprofilephoto = htmlspecialchars($_POST['photo_path']);

$TagsID = htmlspecialchars($_POST['tags_id']);
$TagsID = 1;
$POIdescription = htmlspecialchars($_POST['poi_desc']);
$POIwebsite = htmlspecialchars($_POST['poi_www']);
$POIphone = htmlspecialchars($_POST['poi_phone']);
$POImail= htmlspecialchars($_POST['poi_mail']);
$Fidelity = htmlspecialchars($_POST['fidelity']);
$POIStatus = htmlspecialchars($_POST['poi_status']);
$CheckDate = htmlspecialchars($_POST['check_date']);

$URLfb = htmlspecialchars($_POST['url_fb']);
$URLtt = htmlspecialchars($_POST['url_tt']);
$URLlin = htmlspecialchars($_POST['url_lin']);
$URLta = htmlspecialchars($_POST['url_ta']);

$POIphotos = $_POST['photo_path'];
$POIphotos = str_replace('["', '', $POIphotos);
$POIphotos = str_replace('"]', '', $POIphotos);

$photoarray = explode('","', $POIphotos);
$POIprofilephoto = $photoarray[0];

$WH_monday_open = htmlspecialchars($_POST['monday_open']);
$WH_monday_close = htmlspecialchars($_POST['monday_close']);
$WH_tuesday_open = htmlspecialchars($_POST['tuesday_open']);
$WH_tuesday_close = htmlspecialchars($_POST['tuesday_close']);
$WH_wednesday_open = htmlspecialchars($_POST['wednesday_open']);
$WH_wednesday_close = htmlspecialchars($_POST['wednesday_close']);
$WH_thursday_open = htmlspecialchars($_POST['thursday_open']);
$WH_thursday_close = htmlspecialchars($_POST['thursday_close']);
$WH_friday_open = htmlspecialchars($_POST['friday_open']);
$WH_friday_close = htmlspecialchars($_POST['friday_close']);
$WH_saturday_open = htmlspecialchars($_POST['saturday_open']);
$WH_saturday_close = htmlspecialchars($_POST['saturday_close']);
$WH_sunday_open = htmlspecialchars($_POST['sunday_open']);
$WH_sunday_close = htmlspecialchars($_POST['sunday_close']);

$query_insert_all = "START TRANSACTION;
INSERT INTO Addresses VALUES (null, '".$AddressStreet."', '".$AddressCode."', $CityID);
SELECT @newAddressID:=MAX(IDAddress) from Addresses;
INSERT INTO Coordinates VALUES (null, $Lat, $Lon, @newAddressID);
SELECT @newCoordsID:=MAX(IDCoords) from Coordinates;
INSERT IGNORE INTO Updaters VALUES (null, '".$UpdaterName."');
SELECT @updaterID:=IDUpdater FROM Updaters WHERE UpdaterName = '".$UpdaterName."';
SELECT @maxUpdaterID:=MAX(IDUpdater) FROM Updaters;
SELECT @updaterIDfin:=(IF (@updaterID>0, @updaterID, @maxUpdaterID));

INSERT INTO POIs VALUES (null, '".$POIName."', '".$CategoryID."', '".$SubCatID."', @newCoordsID, null, '".$TypeID."', '".$Logo."', '".$POIprofilephoto."', '".$TagsID."', '".$POIdescription."', null, null, '".$POIwebsite."', '".$POIphone."', '".$POImail."', '".$Fidelity."', null, @updaterIDfin, '".$POIStatus."', '".$CheckDate."');
SELECT @newPOIID:=MAX(IDPOI) FROM POIs;";

// POIs ('IDPOI', 'Name', 'CategoryID', 'SubcategoryID', 'CoordinatesID', 'LinkID', 'TypeID', 'Logo', 'ProfPhoto', 'TagsID', 'Description', 'PhotoID', 'VideoID', 'WWW', 'Phone', 'Mail', 'FidelityID', 'UpdateDate', 'UpdaterID', 'POIStatusID', 'CheckDate')

if ($URLfb !== ''){
	$query_insert_all .= "INSERT INTO Links VALUES (null, @newPOIID, '".$URLfb."', 1);";
}
if ($URLtt !== ''){
	$query_insert_all .= "INSERT INTO Links VALUES (null, @newPOIID, '".$URLtt."', 2);";
}
if ($URLlin !== ''){
	$query_insert_all .= "INSERT INTO Links VALUES (null, @newPOIID, '".$URLlin."', 5);";
}
if ($URLta !== ''){
	$query_insert_all .= "INSERT INTO Links VALUES (null, @newPOIID, '".$URLta."', 6);";
}

for ($i=0;$i<count($photoarray);$i++) {
	$query_insert_all .= "INSERT INTO Photos VALUES (null, '".$photoarray[$i]."', @newPOIID);";
}

if ($WH_monday_open !== ''){
        $query_insert_all .= "INSERT INTO WorkingHours VALUES (null, @newPOIID, 1,'".$WH_monday_open."', '".$WH_monday_close."');";
}

$query_insert_all .= "COMMIT;";

//echo $query_insert_all;

try{
        $sth = $DBcon->prepare($query_insert_all);
        $sth->execute();
}
catch(PDOException $e){
        die($e->getMessage());
	echo json_encode(array("value1"=>$e));
}

if ($e) {
        echo json_encode(array("value1"=>"ERROR.."));
        }
else {
        echo json_encode(array("value1"=>"OK, dodane do bazy"));
}


?>
