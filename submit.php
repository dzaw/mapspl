﻿<?php

require('assets/external/db_conn.php');

$up_id = uniqid();

$query_city = "SELECT * FROM Cities ORDER BY CityName;";
$query_country = "SELECT * FROM Countries;";
$query_category = "SELECT * FROM Categories;";
$query_subcat = "SELECT * FROM Subcategories;";

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sth1 = $DBcon->prepare($query_city);
$sth1->execute();
$sth2 = $DBcon->prepare($query_country);
$sth2->execute();
$sth3 = $DBcon->prepare($query_category);
$sth3->execute();
$sth4 = $DBcon->prepare($query_subcat);
$sth4->execute();

}
catch(PDOException $ex){
        die($ex->getMessage());
}

while ($result = $sth1->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result as $key => $val) {
    $cityoption[] = '<option value="' . $val['IDCity'] . '">' . $val['CityName'] . '</option>';
  }
}
while ($result2 = $sth2->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result2 as $key2 => $val2) {
    $cntryoption[] = '<option value="' . $val2['IDCountry'] . '">' . $val2['CountryName'] . '</option>';
  }
}
while ($result3 = $sth3->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result3 as $key3 => $val3) {
    $categoryoption[] = '<option value="' . $val3['IDCtgry'] . '">' . $val3['CatName'] . '</option>';
  }
}
while ($result4 = $sth4->fetchAll(PDO::FETCH_ASSOC)) {
  foreach ($result4 as $key4 => $val4) {
    $subcatoption[] = '<option value="' . $val4['IDSubctgry'] . '">' . $val4['SubcatName'] . '</option>';
  }
}

echo '



<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="mapsPL">


    <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="assets/css/style.css" type="text/css">
    
	<link rel="stylesheet" href="assets/css/trackpad-scroll-emulator.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jquery.nouislider.min.css" type="text/css">
    
	
	<!-- <script type="text/javascript" src="https:\/\/maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script> -->
    <script type="text/javascript" src="https:\/\/maps.googleapis.com/maps/api/js?key=AIzaSyCoXJIZvbJyAvW_hcDXXiaK1sBL1OLTgLU"></script> 


	<script type="text/javascript" src="https:\/\/ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="https:\/\/ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>	
	
	<link href="assets/external/uploadfiles/style-progress.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="\/\/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
	<link rel="stylesheet" href="assets/css/jquery-clockpicker.min.css" type="text/css">
	<script type="text/javascript" src="assets/js/jquery-clockpicker.min.js"></script>
	
	<style>
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
    .gist { margin-top: 10px; font-size: 12px; }
    .map-wrapper { float:left; margin: 0 10px 0 10px; }
    #map-submit { border: 1px solid #DDD; width:100%; height: 300px; margin: 10px 0 10px 0; -webkit-box-shadow: #AAA 0px 0px 15px;}
    #legend { font-size: 12px;font-style: italic; }
    .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
    * html .clearfix { height: 1%; }
    .dialog_label { width:100%; text-align: left; }
	.qtip-content {font-size: 1.2em; line-height: 1em;}
  	</style>
	
	<script src="https:\/\/code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https:\/\/code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!--<script type="text/javascript" src="assets/js/custom3.js"></script>-->
	
	<script src="assets/js/jquery.ui.addresspicker.js"></script>

    <title>Polski ATLAS 2017</title>

</head>

<body class="homepage">
<div class="page-wrapper">
    <header id="page-header">
        <nav>
            <div class="left">
                <a href="index.html" class="brand">
                <i class="fa fa-globe fa-2x"></i>
                </a>
            </div>
	    <div class="left">
            <h4>Polski ATLAS 2017</h4>
	    </div>
            <!--end left-->
            <div class="right">
                <div class="primary-nav has-mega-menu">
                    <ul class="navigation">
                        <li class="active has-child"><a href="index.html#nav-homepages">Na początek</a>
                            <div class="wrapper">
                                <div id="nav-homepages" class="nav-wrapper">
                                    <ul>
                                        <li><a href="#">O projekcie</a></li>
                                        <li><a href="#">Perspektywy</a></li>
                                        <li><a href="#">Autorzy</a></li>
                                        <li><a href="#">Współpraca</a></li>
                                        <li><a href="#">Zaproszenie</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="has-child"><a href="index.html#nav-listing">Dane</a>
                            <div class="wrapper">
                                <div id="nav-listing" class="nav-wrapper">
                                    <ul>
                                        <li class="has-child"><a href="index.html#nav-grid-listing">Grid Listing</a>
                                            <div id="nav-grid-listing" class="nav-wrapper">
                                                <ul>
                                                    <li><a href="listing-grid-right-sidebar.html">With Right Sidebar</a></li>
                                                    <li><a href="listing-grid-left-sidebar.html">With Left Sidebar</a></li>
                                                    <li><a href="listing-grid-full-width.html">Full Width</a></li>
                                                    <li><a href="listing-grid-different-widths.html">Different Widths</a></li>
                                                    <li><a href="listing-grid-3-items.html">3 Items in Row</a></li>
                                                    <li><a href="listing-grid-4-items.html">4 Items in Row</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="has-child"><a href="index.html#nav-row-listing">Row Listing</a>
                                            <div id="nav-row-listing" class="nav-wrapper">
                                                <ul>
                                                    <li><a href="listing-row-right-sidebar.html">Row Right Sidebar</a></li>
                                                    <li><a href="listing-row-left-sidebar.html">Row Left Sidebar</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!--<li><a href="blog.html">Blog</a></li>-->
                        <li><a href="contact.html">Kontakt</a></li>
                    </ul>
                    <!--end navigation-->
                </div>
                <!--end primary-nav-->
				<!--
                <div class="secondary-nav">
                    <a href="index.html#" data-modal-external-file="modal_sign_in" data-target="modal-sign-in">Logowanie</a>
                    <a href="index.html#" class="promoted" data-modal-external-file="modal_register" data-target="modal-register">Rejestracja</a>
                </div>
				-->
                <!--end secondary-nav-->
                <a href="submit.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-plus"></i><span>Dodaj</span></a>
                <div class="nav-btn">
                    <i></i>
                    <i></i>
                    <i></i>
                </div>
                <!--end nav-btn-->
            </div>
            <!--end right-->
        </nav>
        <!--end nav-->
    </header>
    <!--end page-header-->
   
    <div id="page-content">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="submit.php#">Home</a></li>
                <li><a href="submit.php#">Pages</a></li>
                <li class="active">Dodaj</li>
            </ol>
            <!--end breadcrumb-->
            <section class="page-title center" style="color: #be0c0c;">
				<h1 style="opacity: 1;"><i class="fa fa-globe" style="font-size: 1.5em;"></i><br/> Budujemy Polski Świat</h1>
                <h3 style="opacity: 1; line-height: inherit;">Dodaj dowolne miejsce na świecie<br/> związane z Polską, Polonią, Polakami za granicą</h3>
            </section>
            <!--end page-title-->
            <section>
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                        <form class="form inputs-underline" enctype="multipart/form-data">
                            <section>
							<div class="section_nr">1 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Nazwa obiektu ma kluczowe znaczenie i wpływ na to, czy inni użytkiownicy Atlasu łatwo odnajdą i zidentyfikują to miejsce. Postaraj się zatem podać nazwę możliwie precyzyjnie. Nazwa powinna być krótka i zwięzła. Jeżeli obiekt nie ma oficjalnej nazwy podaj możliwie krótki opis, składający się maksymalnie z trzech, czterech słów. Przykłady nazw znajdziesz w FAQ."></i></div>  
                                <h3>Podaj nazwę miejsca</h3>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="title">Nazwa</label>
                                            <input type="text" class="form-control" name="title" id="POIname" placeholder="Nazwa obiektu">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-12-->
                                </div>
                                <!--end row-->
                                								
                            </section>
                            <section>
							<div class="section_nr">2 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Pokaż gdzie znajduje się Twoje miejsce. Jeżeli jest to sklep, punkt usługowy, instytucja lub jakiekolwiek inne miejsce możliwe do precyzyjnego zlokalizowania (np. tablica pamiątkowa na budynku) wprowadź adres, a punkt na mapie zostanie ustawiony automatycznie we właściwym miejscu. Jeżeli jest to większe miejsce, wskaż punkt centralny (np. park) wykorzystując mapę i przenosząc wskaźnik. Jeżeli natomiast nie jest możliwe wskazanie precyzyjne budynku, określ ulicę, dzielnicę lub okolicę. Wówczas oznacz w polu dokładność na ile dokładna jest lokalizacja na mapie."></i></div>
                                <h3>Podaj adres miejsca lub wskaż lokalizację na mapie</h3>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
											<label for="countryid">Kraj</label>
											<select id="countryid"> 
											<option value="">Wybierz państwo..</option>
											';
													for ($i = 0; $i <= count($cntryoption); $i++) {
															echo $cntryoption[$i];
													};
											echo '
											</select>
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="region">Miasto</label>
                                            <select id="cityid">
											<option value="">Wybierz miasto z bazy..</option>
											';
											for ($i = 0; $i <= count($cityoption); $i++) {
													echo $cityoption[$i];
											};
											echo '
											<option value="addnewcity">+ Dodaj nowe miasto do bazy punktów</option>
											</select>
											<label for="postal_code" hidden>Kod pocztowy</label>
											<input id="postal_code" type="text" placeholder="Kod pocztowy" hidden>
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
									<div class="col-md-12 col-sm-12">
										<label for="addresspicker_map">Adres</label>
                                        <input type="text" class="form-control" name="address" id="addresspicker_map" placeholder="Zacznij wpisywać adres i wybierz z podpowiedzi lub wskaż na mapie przeciągając marker">
									</div>
									<!--end col-md-12-->
									
									<div class="col-md-6 col-sm-6">
                                        <div class="map height-200px shadow" id="map-submit"></div>
                                        <!--end map-->
                                        <div class="form-group">
											<label for="lat" hidden>X (lat), Y (lon)</label>
                                            <input type="hidden" class="form-control" id="lat" name="latitude" placeholder="X">
                                            <input type="hidden" class="form-control" id="lng" name="longitude" placeholder="Y">
                                        </div>
                                        <p class="note">Wybierz adres z podpowiedzi lub wskaż na mapie</p>
                                    </div>
									
									<div class="col-md-6 col-sm-6">                                        
										<div class="form-group">
                                            <label for="POIchkdate">Data sprawdzenia</label>
                                            <input id="POIchkdate" type="text" class="form-control" value="yyyy-mm-dd">
											
                                        </div>
                                        <!--end form-group-->
										<div class="form-group">
                                            <label for="fidelity">Dokładność oznaczenia</label>
                                            <select id="fidelity">
											<option value="">Wybierz dokładność oznaczenia..</option>
											<option value="1">budynek</option>
											<option value="2">ulica</option>
											<option value="3">miasto</option>
											<option value="4">region</option>
											<option value="5">państwo</option>
											</select>
                                        </div>
                                        <!--end form-group-->
										<div class="form-group">
                                            <label for="status">Status obiektu</label>
                                            <select id="status">
											<option value="">Wybierz status obiektu..</option>
											<option value="1">istniejący</option>
											<option value="2">nie istniejący</option>
											</select>
                                        </div>
                                        <!--end form-group-->
                                    </div>	
                                </div>
                            </section>
							<section>
							<div class="section_nr">3 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Teraz dokonaj klasyfikacji obiektu. To bardzo ważne, bo dzięki temu inni użytkownicy łatwo znajdą to miejsce. Aby dowiedzieć się więcej o kategoriach oraz typach obiektów zajrzyj na kartę pomocy. Klasyfikacja jest intuicyjna. Jeżeli stwierdzić, że brakuje jakiejś kategorii, napisz do nas."></i>
							</div>
								<h3>Sklasyfikuj miejsce lub obiekt</h3>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
											<label for="category">Kategoria</label>
											<select id="category">
											<option value="0">Wybierz kategorię z bazy..</option>
											';
											for ($i = 0; $i <= count($categoryoption); $i++) {
													echo $categoryoption[$i];
											};
											echo '
											</select>
											<div id="subcattr">
											<label for="subcategory">Podkategoria</label>
											<select id="subcategory">
											<option value="">Wybierz podkategorię z bazy..</option>
											';
											for ($i = 0; $i <= count($subcatoption); $i++) {
													echo $subcatoption[$i];
											};
											echo '
											</select>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<label for="type">Typ</label>
											<select id="comtype">
											<option value="">Wybierz typ obiektu..</option>
											<option value="2">non-commerce</option>
											<option value="1">commerce</option>
											</select>
											
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="logoid">Logo na mapie</label>
                                            <select id="logoid"><option value="">Wybierz rodzaj logo</option>
											<option value="assets\/img\/icons\/art.png">art</option>
											<option value="assets\/img\/icons\/education.png">education</option>
											<option value="assets\/img\/icons\/restaurant.png">restaurant</option>
											<option value="assets\/img\/icons\/organization.png">organization</option>
											<option value="assets\/img\/icons\/memorialsite.png">memorial</option>
											</select>
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--col-md-6-->
									<div class="col-md-12 col-sm-12">
										<div class="form-group">
										<label for="tags">Tagi</label>
										<input type="text" class="form-control" name="tags" id="tags" placeholder="+ Wpisz tagi / to pole jest wyłączone /">
									</div>
									<!--end form-group-->
								
								</div>
								</section>
                            	
								<section>
								<div class="section_nr">4 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Opisz miejsce swoimi słowami. Możesz podać dodatkowe informacje na temat historii miejsca, wartościowe linki do artykułów opisujących daną lokalizację. Jeżeli jest to miejsce turystyczne, możesz także napisać dlaczego warto je odwiedzić."></i></div>
									<h3>Opisz miejsce</h3>
								<div class="row">
								<div class="col-md-12 col-sm-12">
								<div class="form-group">
                                    <label for="description">Opis</label>
                                    <textarea class="form-control" id="description" spellcheck rows="4" name="description" placeholder="Opis obiektu"></textarea>
                                </div>
								</div>
                                <!--end form-group-->
								</div>
								
								<div class="row">
								<div class="col-md-4 col-sm-4">
								<div class="form-group">
                                    <label for="phone">Telefon</label>
                                    <input type="text" class="form-control" id="poiphone" type="tel" placeholder="(+0123)555-123-456">
                                </div>
								</div>
                                <!--end form-group-->
								<div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="poimail" type="email" class="form-control" name="email" placeholder="mail@mail.com">
                                </div>
								</div>
                                <!--end form-group-->
								<div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label for="website">WWW</label>
									<input id="poiwww" type="text" class="form-control" name="website" placeholder="https://www.example.com">
                                </div>
								</div>
                                <!--end form-group-->
								</div>
								
								</section>
								
								<section>
								<div class="section_nr">5 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Każde dodatkowe zdjęcie danego miejsca lub obiektu jest mile widziane. Dodaj przynajmniej jedno, maksymalnie pięć zdjęć."></i></div>
                                	<h3>Dodaj zdjęcia związane z danym miejscem</h3>
                                <div class="file-upload-previews"></div>
                                <div class="file-upload">
                                    <div id="photos-td">
									<button class="file-upload-input" id="add-photo-dialog"><span>Kliknij żeby dodać zdjęcia</span></button>
									
   	 								<iframe style="position: relative; top: 5px; height: 40px;" id="progress-frame" name="progress-frame" border="0" src="" scrollbar="no" frameborder="0" scrolling="no"> </iframe>
									
									</div>
                                </div>
                                
                            </section>
                            
                            <section>
							<div class="section_nr">6 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Naszą ambicją jest gromadzenie kompletnych danych. Jeżeli posiadasz linki do profili na social media dotyczących dodawanego miejsca, wprowadź je tu."></i></div>
                                <h3>Podaj linki do social media</h3>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="url_fb">Facebook</label>
                                            <input type="text" class="form-control" id="url_fb" name="POIlink_fb" placeholder="https://">
                                        </div>
                                        <!--end form-group-->
										<div class="form-group">
                                            <label for="url_ins">Instagram</label>
                                            <input type="text" class="form-control" id="url_ins" name="POIlink_ins" placeholder="https://">
                                        </div>
                                        <!--end form-group-->
										<div class="form-group">
                                            <label for="url_wiki">Wikipedia</label>
                                            <input type="text" class="form-control" id="url_wiki" name="POIlink_wiki" placeholder="/ to pole jest wyłączone /">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="url_tt">Twitter</label>
                                            <input type="text" class="form-control" id="url_tt" name="POIlink_tt" placeholder="https://">
                                        </div>
                                        <!--end form-group-->
										<div class="form-group">
                                            <label for="url_ta">TripAdvisor</label>
                                            <input type="text" class="form-control" id="url_ta" name="POIlink_ta" placeholder="https://">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                </div>
                                <!--end row-->
                            </section>
                            <section>
							<div class="section_nr">7 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="W przypadku punktów usługowych, sklepów, istotne znaczenie mają godziny otwarcia. Wprowadź je tu. Pole można wykorzystać np. w przypadku nabożeństw, podając w jakie dni tygodnia odprawiana jest msza święta w języku polskim w danym kościele."></i></div>
                                <h3>Podaj godziny kiedy obiekt jest dostępny<span class="note">(opcjonalnie)</span></h3>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="accordion-heading-1">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-collapse-1" id="openworkinghours">
                                                    <i class="fa fa-clock-o"></i>Dodaj godziny pracy
                                                </a>
                                            </h4>
                                        </div>
                                        <!--end panel-heading-->
                                        <div id="accordion-collapse-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-heading-1">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Poniedziałek</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" id="monday_open" name="monday_open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" id="monday_close" name="monday_close" placeholder="Close"> 
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Wtorek</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="tuesday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="tuesday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Środa</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="wednesday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="wednesday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Czwartek</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="thursday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="thursday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Piątek</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="friday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="friday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Sobota</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="saturday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="saturday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 horizontal-input-title">
                                                        <strong>Niedziela</strong>
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="sunday_open" placeholder="Open">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <input type="time" class="form-control clockpicker" name="sunday_close" placeholder="Close">
                                                        </div>
                                                        <!--end form-group-->
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                        <!--end panel-collapse-->
                                    </div>
                                    <!--end panel-->
                                </div>
                                <!--end panel-group-->
                            </section>
                    <!--
                            <section>
                                <h3>Kalendarz<span class="note">(optional)</span></h3>
                                <div class="panel-group" id="accordion-3" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="accordion-heading-3">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion-3" href="submit.php#accordion-collapse-3" aria-expanded="false" aria-controls="accordion-collapse-3">
                                                    <i class="fa fa-calendar"></i>Dodaj wydarzenia
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="accordion-collapse-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-heading-3">
                                            <div class="panel-body">
                                                <div class="wrapper">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-3">
                                                            <strong>Date</strong>
                                                        </div>
                                                        <div class="col-md-2 col-sm-3">
                                                            <strong>Time</strong>
                                                        </div>
                                                        <div class="col-md-4 col-sm-3">
                                                            <strong>Place</strong>
                                                        </div>
                                                        <div class="col-md-4 col-sm-3">
                                                            <strong>Address</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="duplicate-schedule">
                                                        <div class="col-md-2 col-sm-3">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="schedule_date[]" placeholder="Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-3">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="schedule_time[]" placeholder="Time">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-3">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="schedule_place[]" placeholder="Place">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-3">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="schedule_address[]" placeholder="Address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="center"><a href="submit.php#duplicate-schedule" class="btn btn-rounded btn-primary btn-framed btn-light-frame btn-xs icon duplicate"><i class="fa fa-plus"></i>Add another schedule item</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
			-->
							<section>
							<br>
							<hr>
							<div class="form-group">
                                <label for="updater">Wprowadzający do bazy</label>
                                <input id="updater" type="text" placeholder="Imię lub nick">
                            </div>
                            <!--end form-group-->
							</section>
                            <hr>
                            <section class="center">
                                <div class="form-group">
                                    <button id="create-POI" class="btn btn-primary btn-rounded">ZAPISZ I WYŚLIJ DO AUTORYZACJI</button>
									<p class="validateTips"></p>
                                </div>
                                <!--end form-group-->
								<div class="clearfix">
								  <div style="display:none;" class="input-positioned">
									<label>Callback: </label>
									<textarea id="callback_result" rows="15"></textarea>
								  </div>
								</div>
                            </section>
                        </form>
                        <!--end form-->
                    </div>
                    <!--end col-md-6-->
                </div>
                <!--end row-->
            </section>
        </div>
        <!--end container-->
    </div>
    <!--end page-content-->

    <footer id="page-footer">
        <div class="footer-wrapper">
            <div class="block">
                <div class="container">
                    <div class="vertical-aligned-elements">
                        <div class="element width-50">
                            <p data-toggle="modal" data-target="#myModal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam at neque sit amet vestibulum. <a href="submit.php#">Terms of Use</a> and <a href="submit.php#">Privacy Policy</a>.</p>
                        </div>
                        <div class="element width-50 text-align-right">
                            <a href="submit.php#" class="circle-icon"><i class="social_twitter"></i></a>
                            <a href="submit.php#" class="circle-icon"><i class="social_facebook"></i></a>
                            <a href="submit.php#" class="circle-icon"><i class="social_youtube"></i></a>
                        </div>
                    </div>
                    <div class="background-wrapper">
                        <div class="bg-transfer opacity-50">
                            <img src="assets/img/footer-bg.png" alt="">
                        </div>
                    </div>
                    <!--end background-wrapper-->
                </div>
            </div>
            <div class="footer-navigation">
                <div class="container">
                    <div class="vertical-aligned-elements">
                        <div class="element width-50">(C) 2016 Your Company, All right reserved</div>
                        <div class="element width-50 text-align-right">
                            <a href="index.html">Home</a>
                            <a href="listing-grid-right-sidebar.html">Listings</a>
                            <a href="submit.php">Submit Item</a>
                            <a href="contact.html">Contact</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--end page-footer-->
</div>


<div id="dialog-form" title="Dodaj nowe miasto do bazy">
  <p>Wybierz odpowiedni kraj i wpisz nazwę miasta / miejscowości</p>
 
  <form id="cityform">
    <fieldset>
      <label class="dialog_label" for="CountryID">Kraj</label>
	<select id="CountryID" class="ui-widget-content ui-corner-all">
        <option value="">Wybierz kraj..</option>
';
	for ($i = 0; $i <= count($cntryoption); $i++) {
	echo $cntryoption[$i];
	};
echo '
        </select><br/>
      <label class="dialog_label" for="CityName">Nazwa miasta / miejsca</label>
      <input type="text" spellcheck name="CityName" id="CityName" class="text ui-widget-content ui-corner-all">
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>


<div id="dialog-form-photo" title="Dodaj zdjęcia obiektu">
  <p>Dodaj pliki z dysku. Dopuszczalne formaty: JPG, PNG, GIF, TIFF. Maksymalny rozmiar jednego pliku to 2MB, maksymalna ilość 5 plików. Dopiero po dodaniu wszystkich zdjęć kliknij "Prześlij".</p>
 

<form enctype="multipart/form-data" action="https://www.v55582464.wirt29.bhlink.pl/assets/external/uploadfiles/upload.php" method="post" name="upload-form" id="upload-form">
        <!--hidden field-->
         <input type="hidden" value="demo" name="';echo ini_get("session.upload_progress.name");echo '"/>
	<div id="dvFile0"><input type="file" accept="image/*" name="item_file[]" onChange="submitFN.checkExtension(this.value)"></div><div id="dvFile1"></div>
<br/>
        <a href="javascript:submitFN._add_more(0);"><span style="float: right;"><u>(+) Dodaj więcej</u></span></a>
<br/>
<br/>
<!--	<input id="upload-send" type="submit" value="Upload!"> -->
</form>
	<!--Include the progress bar frame-->

</div>

<!--end page-wrapper-->
<a href="submit.php#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>


<script src="assets/js/jquery-clockpicker.min.js"></script>
<link rel="stylesheet" href="assets/css/jquery-clockpicker.min.css" type="text/css">

<link rel="stylesheet" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" type="text/css">
<script type="text/javascript" src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/locales/bootstrap-datepicker.pl.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css" type="text/css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>

<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
        $(".clockpicker").clockpicker({
		placement: "bottom",
    	align: "left",
    	autoclose: true,
    	"default": "now"
		});
	$(\'[title!=""]\').qtip({ position: {my: \'center right\',at: \'center left\' }, style: {classes: \'qtip-light qtip-shadow\', width: 250 } });
	//$(\'[data-toggle="tooltip"]\').tooltip( {position: { my: "left+5 center", at: "left center" } });
	$("#POIchkdate").datepicker("setDate", new Date());
});

$("#POIchkdate").datepicker({
	format: "yyyy-mm-dd",
    todayBtn: "linked",
    clearBtn: true,
    language: "pl",
    orientation: "bottom left",
    autoclose: true, 
	defaultDate: new Date()
});

// this is dirty workaround for not working bootstrap..
$(window).on("load resize", function(){
      var win = $(this);
      if (win.width() < 768) { 
	  	$("body").addClass("nav-btn-only");
		$("#page-header").css("position", "relative");
		$(".nav-btn").on("click", function(){
			console.log("aaa");
			$(".primary-nav").toggleClass("show");
			$(".navigation>li>a").attr("data-toggle", "collapse");
		});
		$(".navigation>li>.wrapper>.nav-wrapper").addClass("collapse");
		$(".navigation>li>.wrapper>.nav-wrapper").on("click", function(){
			$(".navigation>li>.wrapper>.nav-wrapper").toggleClass("collapse");
		});
      }
	  else {
	  	$("body").removeClass("nav-btn-only");
	  }
});

</script>


';



echo '
<script>

var submitFN = {
    exts : "jpg|jpeg|gif|png|bmp|tif|tiff|pdf|JPG|JPEG|GIF|PNG|BMP|TIF|TIFF|PDF",
    init : function(){
        console.log("init submitFN");
    },
    checkExtension : function(value){
        if(value=="")return true;
        var re = new RegExp("^.+\.("+submitFN.exts+")$","i");
        if(!re.test(value)) {
            //console.log( value.split("\\") );
            alert("Niedozwolone rozszerzenie pliku: \n" + value + "\n\nDozwolone rozszerzenia: "+submitFN.exts.replace(/\|/g,\', \')+" \n\n"); 
            $("input[name=\"item_file[]\"]:last").val("");
            return false;
        }
        return true;
    },
    
    next_id : 0,
    max_number : 4,
	_add_more : function() {
		if (submitFN.next_id>=submitFN.max_number) {
			alert("Przekroczono dopuszczalną liczbę zdjęć.");
			return;
		}
		submitFN.next_id=submitFN.next_id+1;
		var next_div=submitFN.next_id+1;
		var txt = "<br><input type=\"file\" name=\"item_file[]\" id=\"item_file[]\" onChange=\"submitFN.checkExtension(this.value)\">";
		txt+=\'<div id="dvFile\'+next_div+\'"></div>\';
		document.getElementById("dvFile" + submitFN.next_id ).innerHTML = txt;
	}
    
}

$(function() {

$(document).ready(function() {

    //$("#subcattr").hide();
	$("#subcategory option").hide();
    $("#workhourstr").hide();

    //show the progress bar only if a file field was clicked
	var show_bar = 0;
    $("input[type=\"file\"]").on("click", function(){
        show_bar=1;
        if (show_bar === 1) {
            $("#progress-frame").show();
            function set () {
                $("#progress-frame").attr("src","https://www.v55582464.wirt29.bhlink.pl/assets/external/progress-frame.php?up_id='; echo $up_id; echo '");
            }
            setTimeout(set);
        }
    });

});






  $( function() {
    var dialog, form, dialog2, form2,
      emailRegex = /^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );



$("select#category").on("change", function(){
	//$("#subcattr").show();
	$("#subcategory option").hide();
	$("#subcategory").val("");
	if ($("#category :selected").val() == "1"){
	 	$("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
		$("#subcategory option[value=\'1\']").show();
		$("#subcategory option[value=\'2\']").show();
		$("#subcategory option[value=\'3\']").show();
		$("#subcategory option[value=\'4\']").show();
	}
	if ($("#category :selected").val() == "2"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'5\']").show();
				$("#subcategory option[value=\'6\']").show();
                $("#subcategory option[value=\'7\']").show();
                $("#subcategory option[value=\'8\']").show();
        }
	if ($("#category :selected").val() == "3"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'9\']").show();
                $("#subcategory option[value=\'10\']").show();
                $("#subcategory option[value=\'11\']").show();
                $("#subcategory option[value=\'12\']").show();
                $("#subcategory option[value=\'13\']").show();
        }
	if ($("#category :selected").val() == "4"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'14\']").show();
        }
    if ($("#category :selected").val() == "5"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'15\']").show();
                $("#subcategory option[value=\'16\']").show();
                $("#subcategory option[value=\'17\']").show();
                $("#subcategory option[value=\'18\']").show();
                $("#subcategory option[value=\'19\']").show();
                $("#subcategory option[value=\'20\']").show();
        }
    if ($("#category :selected").val() == "6"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'21\']").show();
                $("#subcategory option[value=\'22\']").show();
                $("#subcategory option[value=\'23\']").show();
                $("#subcategory option[value=\'24\']").show();
                $("#subcategory option[value=\'25\']").show();
                $("#subcategory option[value=\'26\']").show();
        }
    if ($("#category :selected").val() == "7"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
				$("#subcategory option[value=\'27\']").show();
                $("#subcategory option[value=\'28\']").show();
                $("#subcategory option[value=\'29\']").show();
                $("#subcategory option[value=\'30\']").show();
                $("#subcategory option[value=\'31\']").show();
        }
    if ($("#category :selected").val() == "8"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'32\']").show();
                $("#subcategory option[value=\'33\']").show();
                $("#subcategory option[value=\'34\']").show();
                $("#subcategory option[value=\'35\']").show();
        }
	if ($("#category :selected").val() == "9"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'36\']").show();
                $("#subcategory option[value=\'37\']").show();
                $("#subcategory option[value=\'38\']").show();
                $("#subcategory option[value=\'39\']").show();
        }
	if ($("#category :selected").val() == "10"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'40\']").show();
                $("#subcategory option[value=\'41\']").show();
                $("#subcategory option[value=\'42\']").show();
                $("#subcategory option[value=\'43\']").show();
                $("#subcategory option[value=\'44\']").show();
                $("#subcategory option[value=\'45\']").show();
        }
	if ($("#category :selected").val() == "11"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'46\']").show();
                $("#subcategory option[value=\'47\']").show();
                $("#subcategory option[value=\'48\']").show();
                $("#subcategory option[value=\'49\']").show();
                $("#subcategory option[value=\'50\']").show();
        }
	if ($("#category :selected").val() == "12"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'51\']").show();
                $("#subcategory option[value=\'52\']").show();
                $("#subcategory option[value=\'53\']").show();
                $("#subcategory option[value=\'54\']").show();
        }
	if ($("#category :selected").val() == "13"){
                $("#subcategory option").hide();
                $("#subcategory option[value=\'\']").show();
                $("#subcategory option[value=\'55\']").show();
                $("#subcategory option[value=\'56\']").show();
                $("#subcategory option[value=\'57\']").show();
                $("#subcategory option[value=\'58\']").show();
                $("#subcategory option[value=\'59\']").show();
        }
});


$("select#comtype").on("change", function(){ 
	if ($("#comtype :selected").val() == "1"){
		$("#workhourstr").show();
	}
	if ($("#comtype :selected").val() !== "1"){
                $("#workhourstr").hide();
        }
});



    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }

    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Zawartość " + n + " musi być między " +
          min + " i " + max + "znaków." );
        return false;
      } else {
	o.removeClass("ui-state-error");
        return true;
      }
    }

    function checkTxtLength( o, n, m, min ) {
      if ( o.text().length < min ) {
        m.addClass( "ui-state-error" );
        updateTips( "Długość " + n + " musi być większa niż " +
          min + "." );
        return false;
      } else {
        m.removeClass("ui-state-error");
        return true;
      }
    }

    function checkCoords( o, n, min, max ) {
      if ( o.val() > max || o.val() < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Coord " + n + "should be in range " + min + " and " + max + "." );
        return false;
      } else {
        o.removeClass("ui-state-error");
        return true;
      }
    }

    function checkPresence( o, n ) {
      if ( o.val() ==  "" ) {
        o.addClass( "ui-state-error" );
        updateTips( "Wybierz opcję w " + n + "." );
        return false;
      } else {
	o.removeClass("ui-state-error");
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
	
	function findMaxValue(element) {
    var maxValue = undefined;
    $("option", element).each(function() {
        var val = $(this).attr("value");
        val = parseInt(val, 10);
        if (maxValue === undefined || maxValue < val) {
            maxValue = val;
        }
    });
    return maxValue;
	}
    
 
    function addCity() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      if ( valid ) {

	$.post( "https://www.v55582464.wirt29.bhlink.pl/assets/external/db_insertcity.php", { cityname: $("#CityName").val() , countryid: $("#CountryID").val() }, function( data ) {
  		//console.log( data.value1 );
		alert(data.value1);
		console.log( data.value2 );
		$("#cityid").load("https://www.v55582464.wirt29.bhlink.pl/assets/external/db_cities.php");
		setTimeout(function(){
		  $("#cityid").val(data.value2).change();
		}, 200);
		//$("#city option:last").before("<option value="...">$("#CityName").val()</option>");
	}, "json");
        dialog.dialog( "close" );
      }
      return valid;
    }


    function addPhotos() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
        if ( valid ) {
           $("#progress-frame").show();
           function set () {
            console.log("https://www.v55582464.wirt29.bhlink.pl/assets/external/progress-frame.php?up_id='; echo $up_id; echo '");
             $("#progress-frame").attr("src","https://www.v55582464.wirt29.bhlink.pl/assets/external/progress-frame.php?up_id='; echo $up_id; echo '");
           }
           setTimeout(set);

	var ajaxData = new FormData();
	  $.each($(\'input[type=file]\'), function(i, obj) {
    	  $.each(obj.files,function(j, file){
    	    ajaxData.append(\'item_file[]\', file);
    	  })
	});


	$.ajax({
	  url: "https://www.v55582464.wirt29.bhlink.pl/assets/external/upload.php",
	  type: "POST",
	  data: ajaxData, // The form with the file inputs.
	  contentType: false,
	  processData: false                        // Using FormData, no need to process data.
	}).done(function(file){
	  console.log("Success: Files sent!" + file);
		filen = file;
		file = file.replace(\'["\', "");
		file = file.replace(\'"]\', "");
		file = file.replace((/[\\/|\\\\]/g), "");
		file = file.replace(\'","\', ', ');
		file = file.replace(/assetsexternaluploadfilesitems/g, "");
 	  $("#photos-td").html("<td id=\"photospath\" data-file="+filen+">"+file+"</td>");
	}).fail(function(){
	  console.log("An error occurred, the files couldn\'t be sent!");
	});

	dialog2.dialog("close");
      }
      return valid;
    }

    function addPhoto() {
      for (var i=0; i<4;i++){
	console.log(i);
        var file = document.getElementById("POIphoto"+i).files[0];
        var reader = new FileReader();
      	reader.readAsText(file, "UTF-8");
      	reader.onload = shipOff;
      }
    }
	function shipOff(event) {
    	  var result = event.target.result;
    	  var fileName = document.getElementById("POIphoto1").files[0].name;
          $.post( "https://www.v55582464.wirt29.bhlink.pl/assets/external/fileupload.php", { photo: result, name: fileName }, function( data ) {
                console.log( data.value1 );
                alert(data.value1);
          }, "json");
      }

 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Dodaj": addCity,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        //form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addCity();
    });

    dialog2 = $( "#dialog-form-photo" ).dialog({
      autoOpen: false,
      height: 500,
      width: 350,
      modal: true,
      buttons: {
       "Prześlij": addPhotos,
        Cancel: function() {
          dialog2.dialog( "close" );
        }
      },
      close: function() {
        //form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form2 = dialog.find( "upload-form" ).on( "submit", function( event ) {
      event.preventDefault();
      addPhotos();
    });

    $( "#cityid" ).on("change", function() {
	if ($(this).val() == "addnewcity"){
		dialog.dialog( "open" );
	}
    });

    $("#add-photo-dialog").button().on("click", function( event ){
	event.preventDefault();
	dialog2.dialog("open");
    });

    $("#POIprofile_button").button().on("click", function(){
        dialog2.dialog("open");
    });


    function validate_all() {
	valid = true;

    var countryvar = $("#countryid");
	var cityvar = $("#cityid");
    var streetvar = $("#addresspicker_map");
	var latvar = $("#lat");
	var lonvar = $("#lng");
	var catvar = $("#category");
	var subcatvar = $("#subcategory");
	var typevar = $("#comtype");
	var poiname =  $("#POIname");
	var logovar = $("#logoid");
	var checkdatevar = $("#POIchkdate");
	var statvar = $("#status");
	var fidvar = $("#fidelity");
	var photovar = $("#photospath");
	var updatervar =  $("#updater");

	checkPresence( countryvar, "Kraj");
	checkPresence(cityvar, "Miasto");
	checkLength( streetvar, "Ulica", 3, 80 );
	checkCoords (latvar, "X Latitude", -90, 90);
	checkCoords (lonvar, "Y Longitude", -180, 180);
	checkPresence(catvar, "Kategoria");
	checkPresence(subcatvar, "Podkategoria");
	checkPresence(typevar, "Typ");
    checkLength( poiname, "Nazwa obiektu", 3, 50 );
	checkPresence(logovar, "Logo");
	checkPresence(checkdatevar, "Data sprawdzenia");
	checkPresence(statvar, "Status");

// POI MAIL ?????
	//var email = $("#poimail");
      	//valid = valid && (if(email != ""){checkRegexp( email, emailRegex, "eg. ui@jquery.com" )});

	checkPresence(fidvar, "Dokładność");
	//checkTxtLength( photovar, "Zdjęcia", $("#add-photo-dialog"), 3);
      	checkLength( updatervar, "Wprowadzający", 3, 40 );

      	//valid = valid && checkRegexp( poiname, /^[a-z]([0-9a-z_\s])+$/i, "Nazwa może zawierać znaki a-z, 0-9, _, spacje i musi zaczynać się od litery." );
      	//valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

        valid = valid && checkPresence( countryvar, "Kraj") && checkPresence(cityvar, "Miasto") && checkLength( streetvar, "Ulica", 3, 80 )  && checkCoords (latvar, "X Latitude", -90, 90) && checkCoords (lonvar, "Y Longitude", -180, 180) && checkPresence(catvar, "Kategoria") && checkPresence(subcatvar, "Podkategoria") && checkPresence(typevar, "Typ") && checkLength( poiname, "Nazwa obiektu", 3, 50 ) && checkPresence(logovar, "Logo") && checkPresence(checkdatevar, "Data sprawdzenia") && checkPresence(statvar, "Status") && checkPresence(fidvar, "Dokładność") && checkLength( updatervar, "Wprowadzający", 3, 40 );
        return valid;
    }
    
    
    
    
    

    $( "#create-POI" ).button().on( "click", function(event) {
	event.preventDefault();

	validate_all();

	if ($("#postal_code").val() == "false") {
		$("#postal_code").val(0);
	}

	if (valid == true) {
	//console.log("VALID?", valid);
	$.post( "https://www.v55582464.wirt29.bhlink.pl/assets/external/db_insertPOI.php", { city_id: $("#cityid").val(), address_street: $("#addresspicker_map").val(), address_code: $("#postal_code").val(), lat: $("#lat").val(), lon: $("#lng").val(), updater_name: $("#updater").val(), poi_name: $("#POIname").val(), cat_id: $("#category").val(), subcat_id: $("#subcategory").val(), type_id: $("#comtype").val(), logo: $("#logoid").val(), poi_profile: "", tags_id: "", poi_desc: $("#description").val(), poi_www: $("#poiwww").val(), poi_phone: $("#poiphone").val(), poi_mail: $("#poimail").val(), fidelity: $("#fidelity").val(), poi_status: $("#status").val(), check_date: $("#POIchkdate").val(), url_fb: $("#url_fb").val(), url_tt: $("#url_tt").val(), url_lin: $("#url_lin").val(), url_ta: $("#url_ta").val(), photo_path: $("#photospath").attr(\'data-file\'), monday_open: $("#monday_open").val(), monday_close: $("#monday_close").val() }, function( data ) {
                alert( data.value1 );
                console.log( data.value1 );
		window.location.reload();
        }, "json");
	}
    });

});
	
	$(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
    });
    var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
      regionBias: "",
      updateCallback: showCallback,
      reverseGeocode: true,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(51.5, 0.0),
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map:      "#map-submit",
        lat:      "#lat",
        lng:      "#lng",
        street_number: "#street_number",
        route: "#route",
        locality: "#locality",
        administrative_area_level_2: "#administrative_area_level_2",
        administrative_area_level_1: "#administrative_area_level_1",
        country:  "#country",
        postal_code: "#postal_code",
        type:    "#type"
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

    $("#reverseGeocode").change(function(){
        $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === "true"));
    });

    function showCallback(geocodeResult, parsedGeocodeResult){
      $("#callback_result").text(JSON.stringify(parsedGeocodeResult, null, 4));
    }
    // Update zoom field
    var map = $("#addresspicker_map").addresspicker("map");
    google.maps.event.addListener(map, "idle", function(){
      $("#zoom").val(map.getZoom());
    });
	
	
	$("#openworkinghours").click(function(){
		$("#accordion-collapse-1").toggleClass("in");
	});

  });
	
});

</script>
';


echo '
</body>
';

?>
